## Contributors
* Matteo Biasetton		<matteo.biasetton@studenti.unipd.it>
* Albero Purpura		<alberto.purpura@studenti.unipd.it>
* Andrea Tonon			<andrea.tonon.3@studenti.unipd.it>

### Implementation
This project is developed in Java using IntelliJ IDE version 2016. 
For the implementation we used the [fastutil library](http://fastutil.di.unimi.it/) in addiction to 
the Java Standard Library.

### Execution
We built a jar file containing all the project files. All the needed library are included in it.
To start the program, launch the [RunWindows.bat](RunWindows.bat) or [RunMAC.command](RunMAC.command).
Otherwise you can start the program from command line with: 
-jar -Xmx5g Banks.jar 


