# Keyword Search at University of Padua (KS-UNIPD)


KS-UNIPD contains implementations of several state-of-the-art algorithms for keyword search over relation data, developed by students of the course on databases at the master degree in Computer Engineering of the University of Padua, Italy, academic year 2015-2016.

More info on the [course homepage](http://en.didattica.unipd.it/offerta/2015/IN/IN0521/2009/000ZZ/1131676).

This software consists of student projects and it is not intended for use in production or in any critical context. It may contain bugs and it may not work as expected.

KS-UNIPD is copyright of University of Padua, Italy and it licensed under the BSD 3-Clause license, as also described in the file [license.txt](license.txt).


## References

Please, acknowledge the use of these implementations by citing

   Badan, A., Benvegnù, L., Biasetton, M., Bonato, G., Brighente, A., Cenzato, A., Ceron, P., Cogato, G., Marchesin, S., Minetto, A., Pellegrina, L., Purpura, A., Simionato, R., Soleti, N., Tessarotto, M., Tonon, A., Vendramin, F., and Ferro, N. (2017). Towards open-source shared implementations of keyword-based access systems to relational data. In *Proc. 1st International Workshop on Keyword-Based Access and Ranking at Scale (KARS 2017) - Proc. of the Workshops of the EDBT/ICDT 2017 Joint Conference (EDBT/ICDT 2017)*. CEUR Workshop Proceedings (CEUR-WS.org), ISSN 1613-0073, [http://ceur-ws.org/Vol-1810/KARS_paper_01.pdf](http://ceur-ws.org/Vol-1810/KARS_paper_01.pdf).


## Algorithms

### Schema-based algorithms

* **DISCOVER-I**  
   Hristidis, V. and Papakonstantinou, Y. (2002). DISCOVER: Keyword Search in Relational Databases. In Bernstein, P. A., Ioannidis, Y. E., Ramakrishnan, R., and Papadias, D., editors, *Proc. 28th International Conference on Very Large Data Bases (VLDB 2002)*, pages 670-681. Morgan Kaufmann Publisher, Inc., San Francisco, CA, USA.

* **DISCOVER-II**  
   Hristidis, V., Gravano, L., and Papakonstantinou, Y. (2003). Efficient IR-Style Keyword Search over Relational Databases. In Freytag, J. C., Lockemann, P. C., Abiteboul, S., Carey, M. J., Selinger, P. G., and Heuer, A., editors, *Proc. 29th International
  Conference on Very Large Data Bases (VLDB 2003)*, pages 850-861. Morgan
  Kaufmann Publisher, Inc., San Francisco, CA, USA.


### Graph-based algorithms

* **BANKS-I**  
   Bhalotia, G., Hulgeri, A., Nakhe, C., Chakrabarti, S., and Sudarshan, S. (2002). Keyword Searching and Browsing in Databases using BANKS. In Agrawal, R., Dittrich, K., and Ngu, A. H. H., editors, *Proc. 18th International Conference on Data Engineering (ICDE 2002)*, pages 431-440. IEEE Computer Society, Los Alamitos, CA, USA.
  
* **BANKS-II**  
   Kacholia, V., Pandit, S., Chakrabarti, S., Sudarshan, S., Desai, R., and Karambelkar, H. (2004). Bidirectional Expansion For Keyword Search on Graph Databases. In Böhm, K., Jensen, C. S., Haas, L. M., Kersten, M. L., Larson, P.-A., and Chin Ooi, B., editors, *Proc. 31st International Conference on Very Large Data Bases (VLDB 2005)*, pages 505-516. ACM Press, New York, USA. 

* **DPBF**  
   Ding, B., Xu Yu, J., Wang, S., Qin, L., Zhang, X., and Lin, X. (2007). Finding Top-k Min-Cost Connected Trees in Databases. In Chirkova, R., Dogac, A., Özsu, M. T., and Sellis, T., editors, *Proc. 23rd International Conference on Data Engineering (ICDE 2007)*, pages 836-845. IEEE Computer Society, Los Alamitos, CA, USA.

* **STAR**  
   Kasneci, G., Ramanath, M., Sozio, M., Suchanek, F. M., and Weikum, G. (2009). STAR: Steiner-Tree Approximation in Relationship Graphs. In Li, Z., Yu, P. S., Ioannidis, Y. E., Lee, D., and Ng, R., editors, *Proc. 25th International Conference on Data Engineering (ICDE 2009)*, pages 868-879. IEEE Computer Society, Los Alamitos, CA, USA.


## Contributors

* Alex Badan <[alex.badan@studenti.unipd.it](mailto:alex.badan@studenti.unipd.it)>
* Luca Benvegnù <[luca.benvegnu.2@studenti.unipd.it](mailto:luca.benvegnu.2@studenti.unipd.it)>
* Matteo Biasetton <[matteo.biasetton@studenti.unipd.it](mailto:matteo.biasetton@studenti.unipd.it)>
* Giovanni Bonato <[giovanni.bonato@studenti.unipd.it](mailto:giovanni.bonato@studenti.unipd.it)>
* Alessandro Brighente <[alessandro.brighente@studenti.unipd.it](mailto:alessandro.brighente@studenti.unipd.it)>
* Alberto Cenzato <[alberto.cenzato@studenti.unipd.it](mailto:alberto.cenzato@studenti.unipd.it)>
* Piergiorgio Ceron <[piergiorgio.ceron@studenti.unipd.it](mailto:piergiorgio.ceron@studenti.unipd.it)>
* Giovanni Cogato <[giovanni.cogato.1@studenti.unipd.it](mailto:giovanni.cogato.1@studenti.unipd.it)>
* Stefano Marchesin <[stefano.marchesin@studenti.unipd.it](mailto:stefano.marchesin@studenti.unipd.it)>
* Alberto Minetto <[alberto.minetto@studenti.unipd.it](mailto:alberto.minetto@studenti.unipd.it)>
* Leonardo Pellegrina <[leonardo.pellegrina@studenti.unipd.it](mailto:leonardo.pellegrina@studenti.unipd.it)>
* Alberto Purpura <[alberto.purpura@studenti.unipd.it](mailto:alberto.purpura@studenti.unipd.it)>
* Riccardo Simionato <[riccardo.simionato.1@studenti.unipd.it](mailto:riccardo.simionato.1@studenti.unipd.it)>
* Nicolò Soleti <[nicolo.soleti@studenti.unipd.it](mailto:nicolo.soleti@studenti.unipd.it)>
* Matteo Tessarotto <[matteo.tessarotto.1@studenti.unipd.it](mailto:matteo.tessarotto.1@studenti.unipd.it)>
* Andrea Tonon <[andrea.tonon.3@studenti.unipd.it](mailto:andrea.tonon.3@studenti.unipd.it)>
* Federico Vendramin <[federico.vendramin@studenti.unipd.it](mailto:federico.vendramin@studenti.unipd.it)>


## Main Contact

* Nicola Ferro <[ferro@dei.unipd.it](mailto:ferro@dei.unipd.it)>


## License

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.