package keyword;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

public class Exe_Alg {

	public ArrayList<Result> Sparse(ArrayList<ArrayList<GraphNode>> CN, LinkedHashMap map, int top_k, LinkedHashMap map_ID_SCORE, Connection con) {

		PreparedStatement ps = null;
		ResultSet rs = null;

		// create the comparator
		Comparator<Result> comparator = Collections.reverseOrder();

		// set the ArrayList that contains all the results for the algorithm

		ArrayList<Result> results = new ArrayList<Result>();

		// first CN in the CN list (one node only)
		ArrayList<GraphNode> single_node_CN = CN.get(0);
		// get the tuple set from the CN
		ArrayList<Tuple> tuple_set = single_node_CN.get(0).get_TupleSet();

		// declaration of the inputs for the result object
		ArrayList<Integer> input_ids = new ArrayList<Integer>();
		ArrayList<int[]> input_keys = new ArrayList<int[]>();
		double score = 0;

		// cycle on the first top_k elements of the ordered tuple set
		for (int i = 0; i < top_k; i++) {

			// Verify that tuple_set has at least five rows
			if (i < tuple_set.size()) {
				
				boolean found = false;
				for (int j = 0; j<results.size();j++){
					if (results.get(j).get_Ids().get(0) == tuple_set.get(i).RowId()) 
					       found = true;								
				}
							
				
				if (!found){							
				// insertion of the list of tuple ids
				input_ids = new ArrayList<Integer>();
				input_ids.add(Integer.valueOf(tuple_set.get(i).RowId()));
				// insertion of the list of tuple connections
				input_keys = new ArrayList<int[]>();
				// insertion of the tree of tuples score, obtained using the
				// function combine
				score = tuple_set.get(i).Weight();

				// the input is an array which contains the list of search_id of
				// the tuples in the CN the list of the connections, i.e.
				// (PK,FK) relations, between
				// those tuples the score of the entire Tree of tuples
				Result input = new Result(input_ids, input_keys, score);

				// insertion of the object in the final list
				results.add(input);
				}
			} else
				break;

		}

		// cycle on all the other CNs that have one single node
		int dim = 1;
		// set the MPS equal to 0
		double MPS = 0;

		while ( (dim < CN.size()) && CN.get(dim).size() < 2) {
			// get the CN from the list
			single_node_CN = CN.get(dim);
			// get the tuple set from the CN
			tuple_set = single_node_CN.get(0).get_TupleSet();
			// find the MPS for the CN
			MPS = tuple_set.get(0).Weight();
			// verify if the CN can be competitive with the previous one
			if (MPS > results.get(results.size() - 1).get_Score()) {
				// then start to find the possible solutions
				for (int i = 0; i < top_k; i++) {

					// Verify that tuple_set has at least five rows
					if (i < tuple_set.size()) {

						double tuple_score = tuple_set.get(i).Weight();
						if (tuple_score > results.get(results.size() - 1).get_Score()) {

							if (results.size() == top_k) {
								// remove the element with the lowest score from
								// the
								// list
								results.remove(top_k - 1);
							}
							boolean found = false;
							for (int j = 0; j<results.size();j++){
								if (results.get(j).get_Ids().get(0) == tuple_set.get(i).RowId()) 
								       found = true;								
							}
							
							if (!found){
							// insertion of the list of tuple ids
							input_ids = new ArrayList<Integer>();
							input_ids.add(Integer.valueOf(tuple_set.get(i).RowId()));
							// insertion of the list of tuple connections
							input_keys = new ArrayList<int[]>();
							// insertion of the tree of tuples score, obtained
							// using
							// the function combine
							score = tuple_set.get(i).Weight();
							// create the input result
							Result input = new Result(input_ids, input_keys, score);
							// insertion of the object in the final list
							results.add(input);
							// sort the result list based on the score keeping
							// the
							// highest score in the first position (0)
							Collections.sort(results, comparator);
							}
					
						} else
							break;

					} else
						break;

				}
			}
			// increase in order to move on the next single node CN
			dim++;
		}

		// now consider the CNs with more than one node
		while (dim < CN.size()) {
			// set the MPS equal to 0 for the CN we are selecting
			MPS = 0;
			for (int i = 0; i < CN.get(dim).size(); i++) {
				// get the tuple set from the CN
				MPS = MPS + CN.get(dim).get(i).get_TupleSet().get(0).Weight();
			}
			// verify if the CN can be competitive with the previous one
			if (MPS > results.get(results.size() - 1).get_Score()) {

				// extract the CN
				ArrayList<GraphNode> current_CN = CN.get(dim);
				// Initialize the query's parts
				String WHERE_clause = ""; // set the WHERE clause for the query

				String query = "SELECT * FROM "; // set the query string
				String query_nested = ""; // set the nested query string

				ArrayList<Tuple> tuple = new ArrayList<Tuple>();// set the
																// ArrayList
																// for the
																// tuples
				ArrayList<Integer> row_ids = new ArrayList<Integer>();// set the
																		// ArrayList
																		// for
																		// the
																		// RowIds
				ArrayList<String> attribute_name = new ArrayList<String>();// set
																			// the
				// ArrayList for theAttribute names

				// find all the connections between the nodes of the CN
				// considered get the dimension of the CN
				int current_CN_size = current_CN.size();

				int counter_nodes = 0;
				// set the flag to take note of which node has the PK (flag = PK
				// -> the first node has the PK)
				String flag = "";
				String[] flags_results = new String[current_CN.size() - 1];
				// create the array for the table names

				String[] table_names = new String[current_CN.size()];
				// create the array for the tuple sets, i.e. _Q and _FREE
				String[] tuple_set_names = new String[current_CN.size()];

				for (int i = 0; i < current_CN_size - 1; i++) {
					// simply get all the informations contained in the
					// Tuple
					// Set considered as a single CN

					try {

						// for each node
						// get the name of the i and i+1 nodes in the CN
						table_names[i] = current_CN.get(i).getLabel();
						tuple_set_names[i] = table_names[i];
						String next = current_CN.get(i + 1).getLabel();
						String next_complete = current_CN.get(i + 1).getLabel();

						// delete the Q or FREE on the table names
						if (table_names[i].contains("_Q"))
							table_names[i] = table_names[i].substring(0, table_names[i].length() - 2);

						else
							table_names[i] = table_names[i].substring(0, table_names[i].length() - 5);

						if (next.contains("_Q"))
							next = next.substring(0, next.length() - 2);

						else
							next = next.substring(0, next.length() - 5);

						// check if there is a couple (FK,PK) or viceversa
						if (current_CN.get(i).get_OutEdge().contains(map.get(next_complete))) { // case
							// (FK,PK)

							ps = con.prepareStatement("SELECT foreign_key_col, referenced_PK_col FROM foreign_keys_view " + "WHERE referencing_table_name = '" + table_names[i] + "' AND referenced_table_name = '" + next + "';", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
							rs = ps.executeQuery();

							flag = "FK";
							// insert in the array the correspondent flag for
							// the node
							flags_results[i] = flag;

						} else { // case (PK,FK)

							ps = con.prepareStatement("SELECT foreign_key_col, referenced_PK_col FROM foreign_keys_view " + "WHERE referencing_table_name = '" + next + "' AND referenced_table_name = '" + table_names[i] + "';", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
							rs = ps.executeQuery();

							flag = "PK";
							// insert in the array the correspondent flag for
							// the node
							flags_results[i] = flag;
						}

						int rows = 0;
						if (rs.last()) {
							rows = rs.getRow();
							// Move to beginning
							rs.beforeFirst();
						}
						// set an array that contains all the couples
						// (PK,FK) or
						// (FK,PK) for
						// each node in the CN
						String[] FKPK_couples = new String[2 * rows]; // [2*k-2]
						counter_nodes = 0;
						// get the column names from rs
						while (rs.next()) {

							// set the foreign and primary keys
							String foreign = rs.getString("foreign_key_col");
							String primary = rs.getString("referenced_PK_col");

							// add to the vector that contains the
							// connections between the nodes all the flags, primary and foreign keys
							// related to that CN  FKPK_couples[counter_nodes++] = flag;
							FKPK_couples[counter_nodes++] = primary;
							FKPK_couples[counter_nodes++] = foreign;
						}

						// get the tuples from the internal nodes
						tuple = current_CN.get(i).get_TupleSet();
						for (int h = 0; h < tuple.size(); h++) {

							// get the row ids
							row_ids.add(Integer.valueOf(tuple.get(h).RowId()));
							// get the attribute names
							attribute_name.add(tuple.get(h).Attribute());
						}
						// for each row id prepare the query parts

						query_nested = query_nested + tuple_set_names[i] + " AS A" + i + ",";

						// last node condition: at k-2 set the name for the last using k-1
						if (i == current_CN_size - 2) {
							table_names[i + 1] = current_CN.get(i + 1).getLabel();
							tuple_set_names[i + 1] = table_names[i + 1];
							if (table_names[i + 1].contains("_Q"))
								table_names[i + 1] = table_names[i + 1].substring(0, table_names[i + 1].length() - 2);

							else
								table_names[i + 1] = table_names[i + 1].substring(0, table_names[i + 1].length() - 5);

							// extract all the tuples
							tuple = current_CN.get(i + 1).get_TupleSet();
							for (int h = 0; h < tuple.size(); h++) {

								// get the row ids from the tuples
								row_ids.add(Integer.valueOf(tuple.get(h).RowId()));
								// get the attribute names from the tuples
								attribute_name.add(tuple.get(h).Attribute());
							}

							// WHERE clause for the last but one node
							if (flag.equals("PK")) {
								for (int j = 0; j < FKPK_couples.length - 2; j = j + 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j] + " = " + "A" + (i + 1) + "." + FKPK_couples[j + 1] + " AND";
								}
								WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[FKPK_couples.length - 2] + " = " + "A" + (i + 1) + "." + FKPK_couples[FKPK_couples.length - 1];

							} else {
								for (int j = 0; j < FKPK_couples.length - 2; j = j + 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j + 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[j] + " AND";
								}
								WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[FKPK_couples.length - 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[FKPK_couples.length - 2];

							}

						} // end if last node
						else {
							if (flag.equals("PK")) {
								for (int j = 0; j < FKPK_couples.length; j += 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j] + " = " + "A" + (i + 1) + "." + FKPK_couples[j + 1] + " AND ";
								}

							} else {
								for (int j = 0; j < FKPK_couples.length; j += 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j + 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[j] + " AND ";
								}

							}
						}

					} catch (SQLException e) {
						System.out.printf("Database access error:%n");
						while (e != null) {
							System.out.printf("] Message: %s%n", e.getMessage());
							System.out.printf("] SQL status code: %s%n", e.getSQLState());
							System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
							System.out.printf("%n");
							e = e.getNextException();
						}

					}

				} // end for cycle for the current CN

				// completion of the nested query, including the last node
				query_nested = query_nested + tuple_set_names[current_CN.size() - 1] + " AS A" + (current_CN.size() - 1);
				// query string creation
				query = query + query_nested + " WHERE " + WHERE_clause + ";";

				try {
					ps = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
					rs = ps.executeQuery();

					// set the variable for the total number of rows
					int row_count = 0;

					if (rs.last()) {
						row_count = rs.getRow();
						// Move to beginning
						rs.beforeFirst();
					}

					// verify if the result set has rows
					if (row_count != 0) {

						// obtain the metadata, useful to know the name of all
						// columns
						ResultSetMetaData meta_clmns = rs.getMetaData();

						// indexes of the columns where a __search_id is present
						ArrayList<Integer> indexes = new ArrayList<Integer>();

						// Obtain the indexes for the __search_id that are the
						// same for
						// all the rows in the result set
						for (int m = 1; m <= meta_clmns.getColumnCount(); m++) {
							if (meta_clmns.getColumnLabel(m).contains("__search_id"))
								indexes.add(Integer.valueOf(m));
						}

						// Counter to insert the Result of each row in a
						// different position
						int cnt_row = 0;
						input_ids = new ArrayList<Integer>();
						
						// insertion of the list of tuple connections
						input_keys = new ArrayList<int[]>();
						// set the array containing the candidate tuple trees
						Result[] candidate_trees = new Result[row_count];
						while (rs.next()) {
							input_ids = new ArrayList<Integer>();
							input_keys = new ArrayList<int[]>();
							double row_score = 0;
							// Sum oof the score in the same row
							for (int x = 0; x < indexes.size(); x++) {
								if (map_ID_SCORE.get(Integer.valueOf(rs.getInt(indexes.get(x).intValue()))) != null)
									row_score = row_score + (double)map_ID_SCORE.get(rs.getInt(indexes.get(x)));
								// put in the input object the __search_id for
								// each row joined in the table
								input_ids.add(Integer.valueOf(rs.getInt(indexes.get(x).intValue())));
								
								// set the array that contains the (FK,PK) or (PK,FK)
								// couples
								
								// put the __search_ids related to the FK,PK
								// couples
								int[] couples = new int[2];
								
								if (x < indexes.size() - 1) {
									// if the first node has the FK then put its
									// __search_id as first
									if (flags_results[x].equals("FK")) {
										couples[0] = rs.getInt(indexes.get(x).intValue());
										couples[1] = rs.getInt(indexes.get(x + 1).intValue());
									}
									// else the first node has the PK and it's
									// put as second
									else {
										couples[0] = rs.getInt(indexes.get(x + 1).intValue());
										couples[1] = rs.getInt(indexes.get(x).intValue());
									}
									input_keys.add(couples);
								}

							}
							// compute the combine for each row
							row_score = row_score / indexes.size();
							score = row_score;
							// put everything inside the result object
							Result input = new Result(input_ids, input_keys, score);
							candidate_trees[cnt_row] = input;
							cnt_row++;
						}

						// sort the candidate_trees array in decreasing order
						Arrays.sort(candidate_trees, comparator);
						// search for candidate trees
						for (int i = 0; i < top_k; i++) {

							if (i < candidate_trees.length) {
								if (candidate_trees[i].get_Score() > results.get(results.size() - 1).get_Score()) {

									if (results.size() == top_k) {
										// remove the element with the lowest
										// score from the list
										results.remove(top_k - 1);
									}
									results.add(candidate_trees[i]);
									// sort the result list based on the score
									// keeping the
									// highest score in the first position (0)
									Collections.sort(results, comparator);
								} else
									break;

							} else
								break;
						}
					}

				} catch (SQLException e) {
					System.out.printf("Database access error:%n");
					while (e != null) {
						System.out.printf("] Message: %s%n", e.getMessage());
						System.out.printf("] SQL status code: %s%n", e.getSQLState());
						System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
						System.out.printf("%n");
						e = e.getNextException();
					}
				}

			}

			dim++;
		}
		// end cycle for the CNs container

		return results;
	}
}
