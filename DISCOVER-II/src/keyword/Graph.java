package keyword;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;


public class Graph {
 
 
 public Object[] createGraph (ArrayList<ArrayList<Tuple>> tuple_sets, Connection con) {
  
  ResultSet rs = null;
  //Node's creation
  // Numbers of tables,
     int counter = tuple_sets.size();
  
  // Create a hash map  
     LinkedHashMap<String, Integer> map = new LinkedHashMap<String, Integer>();
  
   //create the graph list, which contains all the nodes along with their adj lists 
   ArrayList<GraphNode> graph = new ArrayList<GraphNode>();
   //Fill the nodes with the given values
  for (int i = 0; i < counter; i++) {
   String label = tuple_sets.get(i).get(0).Attribute();
   //map the table names in a index
   map.put(label, Integer.valueOf(i));
   tuple_sets.get(i).remove(0);
   graph.add(new GraphNode(label, tuple_sets.get(i)));
  }
  
  //Adjacency list creation
   ////retrieve the number of tables that have foreign key relations
  
   //set an index
  int tabs_FK = 0;
   //set a list with table names for tables that have FK
  ArrayList<String> tabnames_FK = new ArrayList<String>(tabs_FK);
 
  try{
   PreparedStatement ps = con.prepareStatement("SELECT COUNT( DISTINCT tc.table_name) AS foreign_table_name "
         + "FROM information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu "
         + "ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu "
         + "ON ccu.constraint_name = tc.constraint_name WHERE constraint_type = 'FOREIGN KEY';");
      rs = ps.executeQuery();
       
   rs.next();   
   tabs_FK = rs.getInt(1);
       
      //retrieve the table names of those tables that have foreign keys relations 
      ps = con.prepareStatement("SELECT DISTINCT tc.table_name AS foreign_table_name FROM "
         + "information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu "
         + "ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu"
         + " ON ccu.constraint_name = tc.constraint_name"
         + " WHERE constraint_type = 'FOREIGN KEY';");
   rs = ps.executeQuery();
   
   while(rs.next()){
    tabnames_FK.add(rs.getString(1));
   } 
   
   for (int i = 0; i < tabs_FK ; i++){
    //retrieve the table and column names of both the relation and the referenced relation 
    ps = con.prepareStatement("SELECT DISTINCT tc.table_name, kcu.column_name, "
           + "ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name "
           + "FROM information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu "
           + "ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu "
           + "ON ccu.constraint_name = tc.constraint_name "
           + "WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name = '" + tabnames_FK.get(i) +"';");
    rs = ps.executeQuery();
    //int Q_index = ((Integer)map.get(tabnames_FK.get(i)+"_Q")).intValue();
    //int FREE_index = ((Integer)map.get(tabnames_FK.get(i)+"_FREE")).intValue();
    while (rs.next()){
     //fill the outedges lists
     if  (map.get(rs.getString(3)+"_Q") != null)  {
      int Q_ = ((Integer)map.get(rs.getString(3)+"_Q")).intValue();
      
      if (map.get(tabnames_FK.get(i)+"_Q") != null) {
       int Q_index = ((Integer)map.get(tabnames_FK.get(i)+"_Q")).intValue();
       // fill the tuple set_Q out list
       graph.get(Q_index).add_OutEdge(Q_);
       //fill the inedges list
       //fill the tuple set_Q in list
       graph.get(Q_).add_InEdge(Q_index);
      }
      
      if (map.get(tabnames_FK.get(i)+"_FREE") != null) {
       int FREE_index = ((Integer)map.get(tabnames_FK.get(i)+"_FREE")).intValue();
       // fill the tuple set_FREE out list
       graph.get(FREE_index).add_OutEdge(Q_);
       //fill the inedges list
       //fill the tuple set_Q in list
       graph.get(Q_).add_InEdge(FREE_index);
        
      }
      
      
    
     }
     if (map.get(rs.getString(3)+"_FREE") != null){
      int FREE_ = ((Integer)map.get(rs.getString(3)+"_FREE")).intValue();
          
      if (map.get(tabnames_FK.get(i)+"_Q") != null) {
       int Q_index = ((Integer)map.get(tabnames_FK.get(i)+"_Q")).intValue();
       // fill the tuple set_Q out list
       graph.get(Q_index).add_OutEdge(FREE_);
       //fill the inedges list
       //fill the tuple set_FREE in list
       graph.get(FREE_).add_InEdge(Q_index);
      }
      
      if (map.get(tabnames_FK.get(i)+"_FREE") != null) {
       int FREE_index = ((Integer)map.get(tabnames_FK.get(i)+"_FREE")).intValue();
       // fill the tuple set_FREE out list
       graph.get(FREE_index).add_OutEdge(FREE_); 
       //fill the inedges list
       //fill the tuple set_FREE in list
       graph.get(FREE_).add_InEdge(FREE_index);
        
      }  
     }
     
     }
    }
   
  } catch (SQLException e) {
   System.out.printf("Database access error:%n");
   while (e != null) {
   System.out.printf("] Message: %s%n", e.getMessage());
   System.out.printf("] SQL status code: %s%n", e.getSQLState());
   System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
   System.out.printf("%n");
   e = e.getNextException();
    }
  }
  
  
  Object[] output = new Object[2];
  output[0] = map;
  output[1] = graph;
  return output;
 }
 
 
}