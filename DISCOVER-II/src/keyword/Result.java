package keyword;

import java.util.ArrayList;

public class Result implements Comparable<Result>{
	
	ArrayList<Integer> Ids;
	ArrayList<int[]> Keys;
	double Score;
	
	public Result(ArrayList<Integer> ids, ArrayList<int[]> keys , double score) {
		Ids = ids;
		Keys = keys;
		Score = score;	
	}
	
	public ArrayList<Integer> get_Ids (){
		return Ids;
	}
	
	public ArrayList<int[]> get_Keys (){
		return Keys;
	}
	
	public double get_Score (){
		return Score;
	}
	
	
	
	/*override the compareTo method in order to make the class comparable*/
	public int compareTo(Result anotherResult) {
		    if (this.get_Score() < anotherResult.get_Score()) {
		        return -1;
		    }
		    else if(this.get_Score() > anotherResult.get_Score()){
		        return 1;
		    }

		    return 0;
	} 

}
