package keyword;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import javax.sql.*;

public class auto_test {

	private static final String DRIVER = "org.postgresql.Driver";
	private static final String DATABASE = "jdbc:postgresql://localhost/postgres";
	private static final String USER = "postgres";
	private static final String PASSWORD = "INSERTDBPASSWORD";
	public static String line;
    public static ArrayList<String> IDs = new ArrayList<String>();
    public static ArrayList<String[]> IDs_local = new ArrayList<String[]>();
    public static int count;
    public static int number_of_results;
    public static int dimension;

	public static void main(String[] args) {

		Connection con = null;
		long start;
		long end;
		String line = "";
	     ArrayList<String> IDs = new ArrayList<String>();
	    ArrayList<String[]> IDs_local = new ArrayList<String[]>();
	    int count = 0;
	    int number_of_results = 0;
	    int dimension = 0;

		try {
			Class.forName(DRIVER);
			//System.out.printf("Driver %s successfully registered.%n", DRIVER);

		} catch (ClassNotFoundException e) {
			System.out.printf("Driver %s not found: %s.%n", DRIVER, e.getMessage());
			System.exit(-1);
		}

		try {

			start = System.currentTimeMillis();
			con = DriverManager.getConnection(DATABASE, USER, PASSWORD);
			end = System.currentTimeMillis();

			//System.out.printf("Connection to database %s successfully established in %,d milliseconds.%n", DATABASE, Long.valueOf(end - start));

			long start1;
			long end1;
			long start2;
			long end2;
			Charset charset = Charset.forName("US-ASCII");
			String name = "";
			String pathSt = "";
			Object[] out = new Object[4];
			ArrayList<Object> Results = new ArrayList<Object>();
			Long[] ExecTime = new Long[50];
			double[] precision = new double[50];
			double[] avgPrecision = new double[50];

			start1 = System.currentTimeMillis();

			// ********** PARAMETERS **********//
			for (int doc_name =48; doc_name < 49 ; doc_name++){
				System.out.println("documnt " + doc_name);
				if(doc_name<10)
					 pathSt = "C:/Users/Piergiorgio/Desktop/imdb/00";
				else
					pathSt = "C:/Users/Piergiorgio/Desktop/imdb/0";
				
			Path logFile = Paths.get(pathSt+doc_name+".txt");
			try (BufferedReader reader = Files.newBufferedReader(logFile, charset)) {
			    line = null;
			    IDs = new ArrayList<String>();
			    count = 0;
			    number_of_results = 0;
			    dimension = 0;
			    
			    while ((line = reader.readLine()) != null) {
			    	//System.out.println("entro nel reader");
			    	if (count ==2){
			        name = line.substring(2,line.length());
			    	}
			    	if (count >= 3){
			    		if (line.charAt(0) != '#')
			    		number_of_results++;
			    		dimension = 0;
			    		for (int c = 1; c <line.length(); c++){
			    		
			    		if(line.charAt(c) == '[' && line.charAt(c-1) == '(')
			    			dimension++;
			    		if (line.charAt(c) == ',')
			    			dimension++;
			    		if (line.charAt(c) == ']'){
			    			IDs.add(line.substring(2,c));
			    			break;
			    		}
			    		
			    		}
			    	}
			    	
			    	count++;
			    }
			} catch (IOException x) {
			    System.err.format("IOException: %s%n", x);
			}
			
			String delims = "[ ]+";    
			String[] keyword = name.split(delims);	
			System.out.println(IDs.size());
			for (int ind_ids = 0; ind_ids <IDs.size(); ind_ids++){
				String delims_ids = ",[ ]";    
		        String[] idsOfTxt = IDs.get(ind_ids).split(delims_ids);
		        IDs_local.add(idsOfTxt);
			}
			
			int topK = 10;
			int treeMaximumDepth = 3;

			// ********** PARAMETERS **********//

			for (int i = 0; i < keyword.length; i++) {
				keyword[i] = keyword[i].toLowerCase();
			}
			
			long start20;
			long end20;
			start20 = System.currentTimeMillis();

			TupleSet_QF afo = new TupleSet_QF();

			start = System.currentTimeMillis();

			Object[] Oexample = afo.CreateTS(keyword, con);
			end = System.currentTimeMillis();

			//System.out.printf("Tuple Set execution time in %,d milliseconds.%n", Long.valueOf(end - start));


			LinkedHashMap<Integer, Double> map_ID_SCORE = (LinkedHashMap) Oexample[0];
			ArrayList<ArrayList<Tuple>> example = (ArrayList<ArrayList<Tuple>>) Oexample[1];

			Graph G = new Graph();

			start = System.currentTimeMillis();
			Object[] TSgraph = G.createGraph(example, con);
			end = System.currentTimeMillis();
			//System.out.printf("Graph created in %,d milliseconds.%n", Long.valueOf(end - start));

			Candidate_Network Example = new Candidate_Network();

			start = System.currentTimeMillis();
			ArrayList<ArrayList<GraphNode>> CN = Example.CN(TSgraph, keyword.length, treeMaximumDepth);
			end = System.currentTimeMillis();

			//System.out.printf("CN generated in %,d milliseconds.%n", Long.valueOf(end - start));

			LinkedHashMap<String, Integer> map = (LinkedHashMap) TSgraph[0];

			if (CN.size() == 0) {
				System.out.println("Le parole cercate non compaiono nel database");
			} else {
				
				long end3;
				long start3;
				//System.out.println("Esecuzione Sparse algorithm");
				Exe_Alg Alg2 = new Exe_Alg();
				start3 = System.currentTimeMillis();
				ArrayList<Result> Sparse_results = Alg2.Sparse(CN, map, topK, map_ID_SCORE, con);
				end3 = System.currentTimeMillis();
				System.out.printf("Sparse done in %,d milliseconds.%n", Long.valueOf(end3 - start3));
				
				ExecTime[doc_name-1]= Long.valueOf(end3 - start3);
				
				end20 = System.currentTimeMillis();
				System.out.printf("Total EXEC time: %,d milliseconds.%n", Long.valueOf(end20 - start20));
				
				
				//evaluation of precision and mean precision for document number doc_name

				int count_number_of_matches = 0;
				double sumOfPrecisions = 0.0;
				//iteration on optimal values (text file)
				for (int p2 = 0; p2 < number_of_results; p2++){
				//iteration on the results of the sparse algorithm: navigate lines of results
				for (int p = 0; p < Sparse_results.size() ; p++){
				
					String[] ids_alg = new String[Sparse_results.get(p).Ids.size()];
					//iteration on the number of IDs of result p: converting all elements of the arrayLis in string
					for (int p1 = 0; p1< Sparse_results.get(p).Ids.size(); p1++){
						//converting IDs to string in order to compare
						ids_alg[p1] = String.valueOf(Sparse_results.get(p).Ids.get(p1));
						//System.out.println("ids algo" + ids_alg[p1]);
					}
					boolean[] found = new boolean[dimension];
					boolean all_in = true;
					//cycle on the columns of txt IDs
					for (int index4colTxt = 0;index4colTxt<dimension; index4colTxt++ ){
						//cycle on the columns of Alg IDs
						for (int index4colAlg = 0; index4colAlg < Sparse_results.get(p).Ids.size(); index4colAlg++)
						if(IDs_local.get(p2)[index4colTxt].equals(ids_alg[index4colAlg])){
						found[index4colTxt] = true;
						}else {found[index4colTxt] = false;}
						}
					for (int check_flag = 0; check_flag<found.length; check_flag++){
						if(!found[check_flag]) 
							all_in = false;
						}
					if (all_in){
						count_number_of_matches++;
					    sumOfPrecisions = sumOfPrecisions + (double)1/(p+1);
					}
					}
					}
					precision[doc_name-1] = (double)count_number_of_matches/topK;
					avgPrecision[doc_name-1] = (double)sumOfPrecisions/number_of_results;
					System.out.println("precision= "+ precision[doc_name-1]+" for " + name);
					System.out.println("avgprecision= "+ avgPrecision[doc_name-1]+" for " + name);
					//System.out.println("Exec time %,d milliseconds.%n "+ Long.valueOf(end3 - start3));
					System.out.println(" for " + name);
					System.out.println("-----------------------------------------------------------------");
				}
				
			}
			
				
				 // Sparse Algorithm
				/*long start3;
				long end3;
				System.out.println("Esecuzione Sparse algorithm");
				Exe_Alg Alg2 = new Exe_Alg();
				start3 = System.currentTimeMillis();
				ArrayList<Result> Sparse_results = Alg2.Sparse(CN, map, topK, map_ID_SCORE, con);
				end3 = System.currentTimeMillis();
				System.out.printf("Sparse done in %,d milliseconds.%n", Long.valueOf(end3 - start3));

				for (int i = 0; i < Sparse_results.size(); i++) {
					System.out.println("I search id: " + Sparse_results.get(i).get_Ids());
					if (Sparse_results.get(i).get_Keys().size() > 0) {
						System.out.println("Le chiavi sono: " + Sparse_results.get(i).get_Keys().get(0)); //
					}
					System.out.println("Lo score �: " + Sparse_results.get(i).Score);
				}*/

			

			// End of the program
			//end1 = System.currentTimeMillis();
			//System.out.printf("Total time %,d milliseconds.%n", Long.valueOf(end1 - start1));

		} catch (SQLException e) {
			System.out.printf("Database access error:%n");

			while (e != null) {
				System.out.printf("- Message: %s%n", e.getMessage());
				System.out.printf("- SQL status code: %s%n", e.getSQLState());
				System.out.printf("- SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
				System.out.printf("%n");
				e = e.getNextException();

			}
		} finally {
			try {

				/*
				 * if (rs != null) { start = System.currentTimeMillis();
				 * rs.close(); end = System.currentTimeMillis();
				 * 
				 * System.out.
				 * printf("Result set successfully closed in %,d millseconds.%n"
				 * , end-start); }
				 * 
				 * if (stmt != null) { start = System.currentTimeMillis();
				 * stmt.close(); end = System.currentTimeMillis();
				 * 
				 * System.out.
				 * printf("Statement successfully closed in %,d millseconds.%n",
				 * end-start); }
				 */

				if (con != null) {
					start = System.currentTimeMillis();
					con.close();
					end = System.currentTimeMillis();

					System.out.printf("Connection successfully closed in %,d millseconds.%n", Long.valueOf(end - start));
				}

				System.out.printf("Resources successfully released.%n");
			} catch (SQLException e) {

				System.out.printf("Error while releasing resources:%n");

				while (e != null) {
					System.out.printf("- Message: $s%n", e.getMessage());
					System.out.printf("- SQL status code: $s%n", e.getSQLState());
					System.out.printf("- SQL error code: $s%n", Integer.valueOf(e.getErrorCode()));
					System.out.printf("%n");
					e = e.getNextException();

				}
			} finally {

				// rs = null;
				// stmt = null;
				con = null;

				System.out.printf("Resources released to the garbage collector.%n");
			}

		}
	}
}
