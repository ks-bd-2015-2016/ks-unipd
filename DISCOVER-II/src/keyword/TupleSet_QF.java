package keyword;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

public class TupleSet_QF {

	public Object[] CreateTS(String[] keyword, Connection con) {

		ResultSet rs = null;
		Statement stmt = null;
		LinkedHashMap<Integer, Double> map_ID_SCORE = new LinkedHashMap<Integer, Double>();

		// find the number of tables
		int rows = 0;

		try {
			// find all the table names
			PreparedStatement ps = con.prepareStatement("SELECT table_name FROM information_schema.tables WHERE " + "table_schema='public' AND table_type='BASE TABLE' ;", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			rs = ps.executeQuery();

			if (rs.last()) {
				rows = rs.getRow();
				// Move to beginning
				rs.beforeFirst();
			}

		} catch (SQLException e) {
			System.out.printf("Database'access'error:%n");

			while (e != null) {
				System.out.printf("] Message: %s%n", e.getMessage());
				System.out.printf("] SQL status'code: %s%n", e.getSQLState());
				System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
				System.out.printf("%n");
				e = e.getNextException();
			}

		}

		// create a list of table names and put into the names
		String[] Table_Names = new String[rows];

		System.out.println("Creazione tuple con score...");
		try {

			while (rs.next()) {
				Table_Names[rs.getRow() - 1] = rs.getString("table_name");
			}

		} catch (SQLException e) {
			System.out.printf("Database'access'error:%n");
			while (e != null) {
				System.out.printf("] Message: %s%n", e.getMessage());
				System.out.printf("] SQL status'code: %s%n", e.getSQLState());
				System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
				System.out.printf("%n");
				e = e.getNextException();
			}

		}

		// create the Arrays for the average attribute value size and the number
		// of rows in the table
		double AVDL[] = new double[Table_Names.length];
		int N[] = new int[Table_Names.length];

		// create a map between table names and their position in the array
		// (used for the score function)
		LinkedHashMap<String, Integer> map_score = new LinkedHashMap<String, Integer>();

		// the DF_tables ArrayList utilizes the map created before to find
		// efficiently the position of the ArrayList tables
		Object[][] DF_tables = new Object[Table_Names.length][keyword.length];

		try {

			for (int i = 0; i < Table_Names.length; i++) {

				String label = Table_Names[i];
				// map the table names in a index
				map_score.put(label, Integer.valueOf(i));

				try (PreparedStatement ps = con.prepareStatement("SELECT AVG(LENGTH(cast(__search_id as varchar))) FROM " + Table_Names[i] + ";"); ResultSet rs1 = ps.executeQuery()) {
					while (rs1.next()) {
						AVDL[i] = rs1.getDouble(1);
					}
				}

				try (PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) FROM " + Table_Names[i] + ";"); ResultSet rs1 = ps.executeQuery()) {
					while (rs1.next()) {
						N[i] = rs1.getInt(1);
					}
				}
			}

			// LinkedHashMap map_keycol = new LinkedHashMap();

			for (int i = 0; i < Table_Names.length; i++) {

				for (int j = 0; j < keyword.length; j++) {

					PreparedStatement ps = con.prepareStatement("SELECT columnname, COUNT(*) " + "FROM search_columns('" + keyword[j] + "','{" + Table_Names[i] + "}') " + "GROUP BY columnname;", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
					rs = ps.executeQuery();

					// create the ArrayList containing the columns and thei
					// counts
					ArrayList<Object> keys = new ArrayList<Object>();
					DF_tables[i][j] = keys;

					while (rs.next()) {
						keys.add(rs.getString("columnname"));
						keys.add(Double.valueOf(rs.getDouble("count")));
					}

				}

			}

		} catch (SQLException e) {

			System.out.printf("Database'access'error:%n");
			while (e != null) {
				System.out.printf("] Message: %s%n", e.getMessage());
				System.out.printf("] SQL status'code: %s%n", e.getSQLState());
				System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
				System.out.printf("%n");
				e = e.getNextException();
			}

		}


		// create the object to be returned with initial size equal to
		// Table_Names size
		// the total size cannot be larger than the size of Table_Names
		ArrayList<ArrayList<Tuple>> SetOf_TS = new ArrayList<ArrayList<Tuple>>();
		// retrieve all the rows of jth table and find the two tuple sets
		// (NON_FREE and FREE)
		int counter = 0;

		for (int j = 0; j < Table_Names.length; j++) {
			String retrieval_NON_FREE = "";
			String retrieval_FREE = "";
			// given the keywords cycle on them in order to create the Tuple
			// Sets
			for (int i = 0; i < keyword.length; i++) {

				// combine the strings to create searched query
				retrieval_NON_FREE = retrieval_NON_FREE + "SELECT * FROM search_columns('" + keyword[i] + "','{" + Table_Names[j] + "}')";
				retrieval_FREE = retrieval_FREE + "SELECT __search_id FROM search_columns('" + keyword[i] + "','{" + Table_Names[j] + "}')";

				if (i < keyword.length - 1) {
					retrieval_NON_FREE = retrieval_NON_FREE + " UNION ";
					retrieval_FREE = retrieval_FREE + " UNION ";

				}

			}

			Rank r = new Rank();
			
			// String Drop = "";
			try {
				// execute the searched query
				PreparedStatement ps = con.prepareStatement(retrieval_NON_FREE + ";", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = ps.executeQuery();
				
				// SCOMMENTA SE NON HAI LA VISTA NEL DB
				
				  if (rs.next()) {
				  
				  // create the associated view which gives the related view // caveat:  we  put retrieval_FREE in order not to create useless  columns
				  String View_Q = "CREATE OR REPLACE VIEW " + Table_Names[j] +
				  "_Q AS SELECT * FROM " + Table_Names[j] +" AS A1 NATURAL JOIN (" + retrieval_FREE + ") AS A2;";
				  
				  // create in the database the view 
				  stmt = con.createStatement(); 
				  stmt.executeUpdate(View_Q);
				  
				  rs.beforeFirst(); }
				 

				if (rs.next()) {
					rs.beforeFirst();
					// add the NON_FREE tuple set to the resultset
					SetOf_TS.add(new ArrayList<Tuple>());
					// set the first tuple of the tuple set as the tablename tuple
					SetOf_TS.get(counter).add(new Tuple(0, Table_Names[j] + "_Q", "", 0));
					while (rs.next()) {
						// insert the current values for the tuple
                       
					   double score_tupla = r.Score(rs.getInt("__search_id"), rs.getString("columnname"), rs.getString("tablename"), DF_tables[map_score.get(rs.getString("tablename")).intValue()], AVDL[map_score.get(rs.getString("tablename")).intValue()], N[map_score.get(rs.getString("tablename")).intValue()], keyword, con);
						
					   //Combine in a row
					   if ( map_ID_SCORE.get(Integer.valueOf(rs.getInt("__search_id")))!= null ){
						     score_tupla = score_tupla + map_ID_SCORE.get(Integer.valueOf(rs.getInt("__search_id")));
						}
						map_ID_SCORE.put(Integer.valueOf(rs.getInt("__search_id")), Double.valueOf(score_tupla));
						// insert the current values for the tuple
						 
						 
						 
						Tuple current = new Tuple(rs.getInt("__search_id"), rs.getString("columnname"), rs.getString("tablename"), score_tupla);

						// add in the correspondent tupleset the right tuple
						SetOf_TS.get(counter).add(current);
						// Collections.sort(SetOf_TS.get(index));
					}
					counter++;
				}

			} catch (SQLException e) {

				System.out.printf("Database'access'error:%n");
				while (e != null) {
					
					System.out.printf("] Message: %s%n", e.getMessage());
					System.out.printf("] SQL status'code: %s%n", e.getSQLState());
					System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
					System.out.printf("%n");
					e = e.getNextException();
				}

			}

			// find the RowIds of those records that don't contain any keyword
			retrieval_FREE = "SELECT __search_id FROM " + Table_Names[j] + " EXCEPT (" + retrieval_FREE + ")";

			try {

				// execute the searched query
				PreparedStatement ps = con.prepareStatement(retrieval_FREE + ";", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = ps.executeQuery();

				// SCOMMENTA SE NON HAI LA VISTA NEL DB
				
				  if (rs.next()) { // create the associated view which give the related view 
				  String View_FREE = "CREATE OR REPLACE VIEW  " +
				  Table_Names[j] + "_FREE AS SELECT * FROM " + Table_Names[j] +
				  " AS A1 NATURAL JOIN (" + retrieval_FREE + ") AS A2;";
				  
				  // create in the database the view 
				  stmt = con.createStatement(); 
				  stmt.executeUpdate(View_FREE);
				  
				  rs.beforeFirst(); }
				 
				if (rs.next()) {
					rs.beforeFirst();
					// add the FREE tuple set to the resultset
					SetOf_TS.add(new ArrayList<Tuple>());
					// set the first tuple of the tuple set as the tablename
					// tuple
					SetOf_TS.get(counter).add(new Tuple(0, Table_Names[j] + "_FREE", "", 0));

					while (rs.next()) {
						// insert the current values for the tuple
						Tuple current = new Tuple(rs.getInt("__search_id"), "", "", 0);
						// add in the correspondent tupleset the right tuple
						SetOf_TS.get(counter).add(current);
						// Collections.sort(SetOf_TS.get(index));
					}
					counter++;
				}

			} catch (SQLException e) {
				System.out.printf("Database'access'error:%n");
				while (e != null) {

					System.out.printf("] Message: %s%n", e.getMessage());
					System.out.printf("] SQL status'code: %s%n", e.getSQLState());
					System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
					System.out.printf("%n");
					e = e.getNextException();
				}
			}

		}

		Comparator<Tuple> comparator = Collections.reverseOrder();
		// sort the tuple sets using their weights as sort parameter
		for (int i = 0; i < SetOf_TS.size(); i++) {
			Collections.sort(SetOf_TS.get(i).subList(1, SetOf_TS.get(i).size()), comparator);
		}

		System.out.println("Fine creazione tuple con score...");
		Object[] output = new Object[2];
		output[0] = map_ID_SCORE;
		output[1] = SetOf_TS;

		return output;

	}
}
