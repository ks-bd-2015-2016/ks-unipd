package keyword;

public class Tuple implements Comparable<Tuple> {
	int RowId;
	String Attribute;
	double Weight;
	String Table;

	public Tuple(int rowId, String attribute,String table, double weight) {
		RowId = rowId;
		Attribute = attribute;
		Weight = weight;
		Table = table;
		
	}
	
	//returns the row identifier of the tuple
	public int RowId (){
		return RowId;
	}
	
	//returns the name attribute of the tuple
	public String Attribute (){
		return Attribute;
	}
	
	//returns the table name
	public String Table (){
		return Table;
	}
	
	//returns the weight of the attribute
	public double Weight (){
		return Weight;
	}

	/*override the compareTo method in order to make the class comparable*/
	public int compareTo(Tuple anotherTuple) {
		    if (this.Weight() < anotherTuple.Weight()) {
		        return -1;
		    }
		    else if(this.Weight() > anotherTuple.Weight()){
		        return 1;
		    }

		    return 0;
	} 
}
