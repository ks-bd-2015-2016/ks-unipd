package keyword;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.sql.*;

public class IR_Index {

	private static final String DRIVER = "org.postgresql.Driver";
	private static final String DATABASE = "jdbc:postgresql://localhost/postgres";
	private static final String USER = "INSERT_USERDB";
	private static final String PASSWORD = "INSERTPASSWORD";

	public static void main(String[] args) {

		Connection con = null;
		long start;
		long end;

		try {
			Class.forName(DRIVER);
			System.out.printf("Driver %s successfully registered.%n", DRIVER);

		} catch (ClassNotFoundException e) {
			System.out.printf("Driver %s not found: %s.%n", DRIVER, e.getMessage());
			System.exit(-1);
		}

		try {

			start = System.currentTimeMillis();
			con = DriverManager.getConnection(DATABASE, USER, PASSWORD);
			end = System.currentTimeMillis();

			System.out.printf("Connection to database %s successfully established in %,d milliseconds.%n", DATABASE, Long.valueOf(end - start));

			long start1;
			long end1;
			long start2;
			long end2;

			start1 = System.currentTimeMillis();

			// ********** PARAMETERS **********//
			int Numkeyword = 5;
			int topK = 10;
			int treeMaximumDepth = 3;

			String[] keyword = new String[Numkeyword];
			 keyword[0] = "crocodile";
			 keyword[1] = "australia";
			 keyword[2] = "dundee";
			 keyword[3] = "II";
			 keyword[4] = "Cornell";
			 //keyword[3] = "kabul";
			// keyword[4] = "afg";
			// keyword[0] = "Dawn";
			// keyword[1] = "which";
			//keyword[0] = "tom";
			//keyword[1] = "hanks";
			// ********** PARAMETERS **********//

			for (int i = 0; i < keyword.length; i++) {
				keyword[i] = keyword[i].toLowerCase();
			}

			TupleSet_QF afo = new TupleSet_QF();

			start = System.currentTimeMillis();

			Object[] Oexample = afo.CreateTS(keyword, con);
			end = System.currentTimeMillis();

			System.out.printf("Tuple Set execution time in %,d milliseconds.%n", Long.valueOf(end - start));

			/*
			 * TupleSet_P afo = new TupleSet_P();
			 * 
			 * start = System.currentTimeMillis();
			 * 
			 * Object[] Oexample = afo.CreateTS(keyword, con); end =
			 * System.currentTimeMillis();
			 * 
			 * System.out.
			 * printf("TUPLE_SET execution time in %,d milliseconds.%n",
			 * Long.valueOf(end - start));
			 */

			LinkedHashMap<Integer, Double> map_ID_SCORE = (LinkedHashMap) Oexample[0];
			ArrayList<ArrayList<Tuple>> example = (ArrayList<ArrayList<Tuple>>) Oexample[1];

			Graph G = new Graph();

			start = System.currentTimeMillis();
			Object[] TSgraph = G.createGraph(example, con);
			end = System.currentTimeMillis();
			System.out.printf("Graph created in %,d milliseconds.%n", Long.valueOf(end - start));

			Candidate_Network Example = new Candidate_Network();

			start = System.currentTimeMillis();
			ArrayList<ArrayList<GraphNode>> CN = Example.CN(TSgraph, keyword.length, treeMaximumDepth);
			end = System.currentTimeMillis();

			System.out.printf("CN generated in %,d milliseconds.%n", Long.valueOf(end - start));

			LinkedHashMap<String, Integer> map = (LinkedHashMap) TSgraph[0];

			if (CN.size() == 0) {
				System.out.println("Le parole cercate non compaiono nel database");
			} else {

				
				/*
				// Naive Algorithm
				Naive_Final Alg1 = new Naive_Final();
				start2 = System.currentTimeMillis();
				System.out.println("Esecuzione Naive....");
				ArrayList<Result> resultsNaive = Alg1.NaiveAlg(CN, map, topK, map_ID_SCORE, con);
				end2 = System.currentTimeMillis();
				System.out.printf("Naive done in %,d milliseconds.%n", Long.valueOf(end2 - start2));
				for (int i = 0; i < resultsNaive.size(); i++) {
					System.out.println("I risultati top-" + topK + " sono: " + resultsNaive.get(i).Score);
				}

                 */
				
				 // Sparse Algoritm
				long start3;
				long end3;
				System.out.println("Esecuzione Sparse algorithm");
				Exe_Alg Alg2 = new Exe_Alg();
				start3 = System.currentTimeMillis();
				ArrayList<Result> Sparse_results = Alg2.Sparse(CN, map, topK, map_ID_SCORE, con);
				end3 = System.currentTimeMillis();
				System.out.printf("Sparse done in %,d milliseconds.%n", Long.valueOf(end3 - start3));
				
				end1 = System.currentTimeMillis();
				System.out.printf("Total time %,d milliseconds.%n", Long.valueOf(end1 - start1));

				for (int i = 0; i < Sparse_results.size(); i++) {
					System.out.println("I search id: " + Sparse_results.get(i).get_Ids());
					if (Sparse_results.get(i).get_Keys().size() > 0) {
						System.out.println("Le chiavi sono: " + Sparse_results.get(i).get_Keys().get(0)); //
					}
					System.out.println("Lo score �: " + Sparse_results.get(i).Score);
				}

			}

			// End of the program
			

		} catch (SQLException e) {
			System.out.printf("Database access error:%n");

			while (e != null) {
				System.out.printf("- Message: %s%n", e.getMessage());
				System.out.printf("- SQL status code: %s%n", e.getSQLState());
				System.out.printf("- SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
				System.out.printf("%n");
				e = e.getNextException();

			}
		} finally {
			try {

				/*
				 * if (rs != null) { start = System.currentTimeMillis();
				 * rs.close(); end = System.currentTimeMillis();
				 * 
				 * System.out.
				 * printf("Result set successfully closed in %,d millseconds.%n"
				 * , end-start); }
				 * 
				 * if (stmt != null) { start = System.currentTimeMillis();
				 * stmt.close(); end = System.currentTimeMillis();
				 * 
				 * System.out.
				 * printf("Statement successfully closed in %,d millseconds.%n",
				 * end-start); }
				 */

				if (con != null) {
					start = System.currentTimeMillis();
					con.close();
					end = System.currentTimeMillis();

					System.out.printf("Connection successfully closed in %,d millseconds.%n", Long.valueOf(end - start));
				}

				System.out.printf("Resources successfully released.%n");
			} catch (SQLException e) {

				System.out.printf("Error while releasing resources:%n");

				while (e != null) {
					System.out.printf("- Message: $s%n", e.getMessage());
					System.out.printf("- SQL status code: $s%n", e.getSQLState());
					System.out.printf("- SQL error code: $s%n", Integer.valueOf(e.getErrorCode()));
					System.out.printf("%n");
					e = e.getNextException();

				}
			} finally {

				// rs = null;
				// stmt = null;
				con = null;

				System.out.printf("Resources released to the garbage collector.%n");
			}

		}
	}
}