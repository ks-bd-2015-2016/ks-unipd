package keyword;

import java.util.ArrayList;

public class Comparator_CN {

	public boolean Compare_CN(ArrayList<ArrayList<GraphNode>> CN, ArrayList<GraphNode> current) {
		// cycle on the list of CNs already in output in order to find out if
		// current is present in reverse order
		// set the equivalence variable to true
		boolean equivalence = false;
		for (int i = 0; i < CN.size(); i++) {
			// search in the CN list all those CNs that have the same size of
			// the current one
			if (CN.get(i).size() == current.size()) {
				// set the condition that verify that the current CN is in the
				// reverse order wrt another one already present in CN

				for (int index2check = 0; index2check < CN.get(i).size(); index2check++) {
					if ((CN.get(i).get(index2check).getLabel().equals(current.get(current.size() - 1 - index2check).getLabel()))) {
						equivalence = true;

					} 
					else{
						equivalence = false;
						break;
					}
				}
				if (equivalence)
					break;
			}

		}
		return equivalence;
	}
}
