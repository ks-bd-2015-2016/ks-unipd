package keyword;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

public class Naive_Final {

	public ArrayList<Result> NaiveAlg(ArrayList<ArrayList<GraphNode>> CN, LinkedHashMap map, int top_k, LinkedHashMap map_ID_SCORE, Connection con) {

		String[] FKPK_couples;
		ArrayList<String> set_FKPK = new ArrayList<String>();
		ArrayList<ArrayList<String>> FKsPKs = new ArrayList<ArrayList<String>>();
		ArrayList<Integer> Indexes4Result = new ArrayList<Integer>();
		ArrayList<Result> Res_tot = new ArrayList<Result>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		ResultSet[] MTJNT = new ResultSet[CN.size()]; // creo Array per le query
														// di ogni CN

		String[] Queries = new String[CN.size()]; // set the array which
													// contains the queries for
													// all the CNs

		// Scorro le CN
		for (int a = 0; a < CN.size(); a++) {
			ArrayList<GraphNode> current_CN = CN.get(a);// get the CN
			// Initialize the query's parts
			String WHERE_clause = ""; // set the WHERE clause for the query

			String query_naive = "SELECT * FROM "; // set the query string
			String query_nested = ""; // set the nested query string
			// set the ArrayList for the tuples set the ArrayList for the RowIds
			// set the ArrayList for
			ArrayList<Tuple> tuple = new ArrayList<Tuple>();
			ArrayList<Integer> row_ids = new ArrayList<Integer>();
			ArrayList<String> attribute_name = new ArrayList<String>();
			// the Attribute names find all the connections between the nodes of
			// the CN considered
			// get the dimension of the CN
			int current_CN_size = current_CN.size();

			int counter_nodes = 0;
			// set the flag to take note of which node has the PK (flag = PK ->
			// the first node has the PK)
			String flag = "";

			// create the array for the table names create the array for the
			// tuple sets, i.e. _Q and _FREE
			String[] table_names = new String[current_CN.size()];

			String[] tuple_set_names = new String[current_CN.size()];

			// first, consider the CNs with size equal to 1
			if (current_CN_size == 1) {

				// temp.get(0) it's due to the fact that there is only one node
				// in the CN
				tuple = current_CN.get(0).get_TupleSet();

				// find the table name of the TupleSet
				table_names[current_CN_size - 1] = current_CN.get(0).getLabel().substring(0, current_CN.get(0).getLabel().length() - 2);
				tuple_set_names[current_CN_size - 1] = current_CN.get(0).getLabel();
				// cycle on the elements of TupleSet
				for (int i = 0; i < tuple.size(); i++) {
					// extract RowIds from tuples
					row_ids.add(Integer.valueOf(tuple.get(i).RowId()));
					// extract attribute names from tuples
					attribute_name.add(tuple.get(i).Attribute());
				}

				// set the final query to be returned
				query_naive = query_naive + tuple_set_names[current_CN_size - 1] + ";";
				// put in the final array the result query to be executed
				Queries[a] = query_naive;

			} else {

				// then consider all the other CNs
				for (int i = 0; i < current_CN_size - 1; i++) {
					// simply get all the informations contained in the Tuple
					// Set considered as a single CN

					try {

						// for each node
						// get the name of the i and i+1 nodes in the CN
						table_names[i] = current_CN.get(i).getLabel();
						tuple_set_names[i] = table_names[i];
						String next = current_CN.get(i + 1).getLabel();
						String next_complete = current_CN.get(i + 1).getLabel();

						// delete the Q or FREE on the table names
						if (table_names[i].contains("_Q"))
							table_names[i] = table_names[i].substring(0, table_names[i].length() - 2);

						else
							table_names[i] = table_names[i].substring(0, table_names[i].length() - 5);

						if (next.contains("_Q"))
							next = next.substring(0, next.length() - 2);

						else
							next = next.substring(0, next.length() - 5);

						// check if there is a couple (FK,PK) or viceversa
						if (current_CN.get(i).get_OutEdge().contains(map.get(next_complete))) { // case
																								// (FK,PK)

							ps = con.prepareStatement("SELECT foreign_key_col, referenced_PK_col FROM foreign_keys_view " + "WHERE referencing_table_name = '" + table_names[i] + "' AND referenced_table_name = '" + next + "';", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
							rs = ps.executeQuery();

							flag = "FK";

						} else { // case (PK,FK)

							ps = con.prepareStatement("SELECT foreign_key_col, referenced_PK_col FROM foreign_keys_view " + "WHERE referencing_table_name = '" + next + "' AND referenced_table_name = '" + table_names[i] + "';", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
							rs = ps.executeQuery();

							flag = "PK";
						}

						int rows = 0;
						if (rs.last()) {
							rows = rs.getRow();
							// Move to beginning
							rs.beforeFirst();
						}
						// set an array that contains all the couples (PK,FK) or
						// (FK,PK) for each node in the CN
						FKPK_couples = new String[2 * rows]; // [2*k-2]
						counter_nodes = 0;
						// get the column names from rs
						while (rs.next()) {

							// set the foreign and primary keys
							String foreign = rs.getString("foreign_key_col");
							String primary = rs.getString("referenced_PK_col");

							// add to the vector that contains the connections
							// between the nodes
							// all the flags, primary and foreign keys related
							// to that CN
							// FKPK_couples[counter_nodes++] = flag;
							FKPK_couples[counter_nodes++] = primary;
							FKPK_couples[counter_nodes++] = foreign;
							set_FKPK.add(flag);
							set_FKPK.add(primary);
							set_FKPK.add(foreign);
						}

						// get the tuples from the internal nodes
						tuple = current_CN.get(i).get_TupleSet();
						for (int h = 0; h < tuple.size(); h++) {

							// get the row ids
							row_ids.add(Integer.valueOf(tuple.get(h).RowId()));
							// get the attribute names
							attribute_name.add(tuple.get(h).Attribute());
						}
						// for each row id prepare the query parts

						query_nested = query_nested + tuple_set_names[i] + " AS A" + i + ",";

						// last node condition:
						// at k-2 set the name for the last
						// using k-1
						if (i == current_CN_size - 2) {
							table_names[i + 1] = current_CN.get(i + 1).getLabel();
							tuple_set_names[i + 1] = table_names[i + 1];
							if (table_names[i + 1].contains("_Q"))
								table_names[i + 1] = table_names[i + 1].substring(0, table_names[i + 1].length() - 2);

							else
								table_names[i + 1] = table_names[i + 1].substring(0, table_names[i + 1].length() - 5);

							// extract all the tuples
							tuple = current_CN.get(i + 1).get_TupleSet();
							for (int h = 0; h < tuple.size(); h++) {

								// get the row ids from the tuples
								row_ids.add(Integer.valueOf(tuple.get(h).RowId()));
								// get the attribute names from the tuples
								attribute_name.add(tuple.get(h).Attribute());
							}

							// WHERE clause for the last but one node
							if (flag.equals("PK")) {
								for (int j = 0; j < FKPK_couples.length - 2; j = j + 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j] + " = " + "A" + (i + 1) + "." + FKPK_couples[j + 1] + " AND";
								}
								WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[FKPK_couples.length - 2] + " = " + "A" + (i + 1) + "." + FKPK_couples[FKPK_couples.length - 1];

							} else {
								for (int j = 0; j < FKPK_couples.length - 2; j = j + 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j + 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[j] + " AND";
								}
								WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[FKPK_couples.length - 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[FKPK_couples.length - 2];

							}

						} // end if last node
						else {
							if (flag.equals("PK")) {
								for (int j = 0; j < FKPK_couples.length; j += 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j] + " = " + "A" + (i + 1) + "." + FKPK_couples[j + 1] + " AND ";
								}

							} else {
								for (int j = 0; j < FKPK_couples.length; j += 2) {
									WHERE_clause = WHERE_clause + " A" + i + "." + FKPK_couples[j + 1] + " = " + "A" + (i + 1) + "." + FKPK_couples[j] + " AND ";
								}

							}
						}

					} catch (SQLException e) {
						System.out.printf("Database access error:%n");
						while (e != null) {
							System.out.printf("] Message: %s%n", e.getMessage());
							System.out.printf("] SQL status code: %s%n", e.getSQLState());
							System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
							System.out.printf("%n");
							e = e.getNextException();
						}

					}

				} // end for cycle for the current CN

				// completion of the nested query, including the last node
				query_nested = query_nested + tuple_set_names[current_CN.size() - 1] + " AS A" + (current_CN.size() - 1);
				// terminazione della query
				query_naive = query_naive + query_nested + " WHERE " + WHERE_clause + ";";
				// Memorizzo tutte le query
				Queries[a] = query_naive;
			}
		FKsPKs.add(set_FKPK);
		} // end cycle for the CNs container

		
		for (int g = 0; g < Queries.length; g++) {
			
			try {
				// System.out.println("Ciao mike sto facendo la query numero: "
				// + g);
				// System.out.println("Ciao mike sono lunga: " +
				// Queries[g].length());
				ps = con.prepareStatement(Queries[g], ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

				MTJNT[g] = ps.executeQuery();
				rs = MTJNT[g];

				// Count the numbers of row in a Result Set
				int row_count = 0;
				if (rs.last()) {

					row_count = rs.getRow();
					// Move to beginning
					rs.beforeFirst();
				}

				// System.out.println("Il numero di righe della query e': " +
				// countR);

				// Ottengo i metadata, utile per conoscere il nome di tutte le
				// colonne
				ResultSetMetaData R = rs.getMetaData();

				// Indici di __search_id
				ArrayList<Integer> indici = new ArrayList<Integer>();

				// Ottengo gli indici di __search_id che saranno uguali per
				// tutte le righe del Result set
				for (int m = 1; m <= R.getColumnCount(); m++) {
					if (R.getColumnLabel(m).contains("__search_id"))
						indici.add(Integer.valueOf(m));
				}

				// Se la CN ha solo un nodo qui non entri, per� devi riordinarla
				// lo stesso perch� stranamente non sono riordinati
			
				if (row_count != 0) {

					// Contiene il combine di ogni riga
					double combines = 0.0;
					int cnt = 0;
					while (rs.next()) {
						double count = 0;
						
						// Sommo gli score di una stessa riga											
						for (int x = 0; x < indici.size(); x++) {
							if (map_ID_SCORE.get(Integer.valueOf(rs.getInt(indici.get(x).intValue()))) != null){
								count = count + (double) map_ID_SCORE.get(rs.getInt(indici.get(x)));
								Indexes4Result.add(Integer.valueOf(rs.getInt(indici.get(x).intValue())));
							}
						}
						// compute the combine for each row
						combines = count / indici.size();
						//if (combines > 9)
							//System.out.println("Il valore di combine per la riga" + cnt + "e' " + combines);

						if (CN.get(g).size() > 1) {
							ArrayList<int[]> keys = new ArrayList<int[]>();
							for (int l = 0; l < CN.get(g).size() - 1; l++) {
								if (FKsPKs.get(g).get(3 * l).equals("FK")) {
									int[] couple = { Indexes4Result.get(l).intValue(), Indexes4Result.get(l + 1).intValue() };
									keys.add(couple);
								}
								if (FKsPKs.get(g).get(3 * l).equals("PK")) {
									int[] couple = { Indexes4Result.get(l + 1).intValue(), Indexes4Result.get(l).intValue() };
									keys.add(couple);
								}
							}

							// create results and add them into an ArrayList
							Result res = new Result(Indexes4Result, keys, combines);
							Res_tot.add(res);
						} else {
							Result res = new Result(Indexes4Result, null, combines);
							Res_tot.add(res);
						}
					}
					Comparator<Result> comparator = Collections.reverseOrder();
					// sort result using combine as sort parameter
					for (int i = 0; i < Res_tot.size(); i++) {
						Collections.sort(Res_tot.subList(0, Res_tot.size()), comparator);
					}

				}
			} catch (SQLException e) {
				System.out.printf("Database access error:%n");

				while (e != null) {
					System.out.printf("- Message: %s%n", e.getMessage());
					System.out.printf("- SQL status code: %s%n", e.getSQLState());
					System.out.printf("- SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
					System.out.printf("%n");
					e = e.getNextException();
				}
			}

		}
		ArrayList<Result> results = new ArrayList<Result>(Res_tot.subList(0, top_k));
		return results;
	}
}
