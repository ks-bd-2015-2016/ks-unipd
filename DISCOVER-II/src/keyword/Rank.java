package keyword;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.lang.Math;
import java.util.LinkedHashMap;

public class Rank {
    
  public double Score (int RowId, String Attributo, String table, Object[] table_k, double AVDL, int N, String[] k, Connection con){
    ResultSet rs;
    String attributo = "";
    try{ 
      // Gives the row of all the tuple 
      PreparedStatement  ps = con.prepareStatement("SELECT * FROM "+table+" WHERE __search_id = "+RowId+";");
         rs = ps.executeQuery();
      while (rs.next())
      attributo = rs.getString(Attributo).toLowerCase();
      
    } catch (SQLException e) {
      System.out.printf("Database access error:%n");
      while (e != null) {
        System.out.printf("] Message: %s%n", e.getMessage());
        System.out.printf("] SQL status code: %s%n", e.getSQLState());
        System.out.printf("] SQL error code: %s%n", Integer.valueOf(e.getErrorCode()));
        System.out.printf("%n");
        e = e.getNextException();
      }
      }
   
   
    LinkedHashMap<String, Object> map_keycol = new LinkedHashMap<String, Object>();
    for (int l = 0; l<k.length;l++){
     map_keycol.put(k[l],table_k[l]);
    }
   
   
   
   
    // Vediamo le keyword che compaiono nell'attributo
    ArrayList<String> w = new ArrayList<String>();
    for ( int i = 0; i<k.length;i++){
     if (attributo.contains(k[i].toLowerCase())){
      w.add(k[i]);
     }
    }
     
    //tf: frequency of w in a_i using an object [w, frequency]
    Object[] w_f = new Object[w.size()*2];
   
    double c = 0;
    int counter = 0;
    for (int i = 0; i < w.size(); i++) {
     c = 0;
     StringTokenizer st = new StringTokenizer(attributo); 
     while (st.hasMoreElements()){
      String temp = st.nextToken();
      if (temp.contains(w.get(i)))
       c++;
     }
     w_f[counter]= w.get(i);
     counter++;
     w_f[counter]=Double.valueOf(c);                        
     counter++;
    }
    double m = 0;  //Initialize score
  
      
    for(int i = 0; i < w.size(); i++){          
       // set the df value which is the number of tuples in ai's relation with word w in it
			double df = 0;
			ArrayList<Object> key = (ArrayList<Object>) map_keycol.get(w.get(i));
			df = ((Double) key.get(key.indexOf(Attributo) + 1)).doubleValue();

			// Score formula
			double s = 0.2;
			double Num = (1 + Math.log(1 + Math.log(((Double) w_f[2 * i + 1]).doubleValue())));
			double Den = (1 - s) + s * (attributo.replace(" ", "").length() / AVDL);
			double dx = Math.log(((N + 1) / df));
			m = m + (Num / Den) * dx;
			// System.out.println("Il valore di Mike �: "+m);
		}

    return m;
   }
  
  public double Combine (double[] weights){
   double sum = 0;
   for (int i = 0; i < weights.length; i++)
	   sum = sum + weights[i];
   double combine = (sum)/weights.length;
   
   return combine;   
  }
  
  
   
 }