package keyword;

import java.util.ArrayList;

public class GraphNode {
    /*Global Variables*/
	protected String label;
    protected ArrayList<Integer> outedges;
    protected ArrayList<Integer> inedges;
    protected ArrayList<Tuple> tuple_set;
    /*Global Variables*/

    public GraphNode(String label, ArrayList<Tuple> tuple_set) {
    	this.label = label;
        this.outedges= new ArrayList<Integer>();
        this.inedges= new ArrayList<Integer>();
        this.tuple_set = tuple_set;
    }

    public String getLabel() {
    	return this.label;
    }
 
    public void add_OutEdge(int outedge) {
    	this.outedges.add(Integer.valueOf(outedge));
    };
    
    public ArrayList<Integer> get_OutEdge() {
    	return this.outedges;
    }
    
    public void add_InEdge(int inedge) {
    	this.inedges.add(Integer.valueOf(inedge));
    };
    
    public ArrayList<Integer> get_InEdge() {
    	return this.inedges;
    }
    
    public ArrayList<Tuple> get_TupleSet() {
        return this.tuple_set;
       }

}
