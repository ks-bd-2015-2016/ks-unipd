package keyword;
import java.util.ArrayList;

import java.util.LinkedHashMap;

public class Candidate_Network {
   
  public ArrayList<ArrayList<GraphNode>> CN (Object[] Map_Graph, int numKeyword, int max_bound){
   
   ArrayList<GraphNode> G = (ArrayList<GraphNode>)Map_Graph[1];
   LinkedHashMap map = (LinkedHashMap)Map_Graph[0];
  
   ArrayList<ArrayList<GraphNode>>  Q = new ArrayList<ArrayList<GraphNode>>();
   ArrayList<ArrayList<GraphNode>>  CN = new ArrayList<ArrayList<GraphNode>>();  
   
  
  //put in the queue and in the CN the tuple sets_Q which are valid CN for the algorithm
  for (int i = 0; i < G.size(); i++) {
   
   if (G.get(i).getLabel().contains("_Q")) { 
    ArrayList<GraphNode> temp = new ArrayList<GraphNode>();
    temp.add(G.get(i));
    Q.add(temp);
    CN.add(temp);
   }
   }
  System.out.println("Le CN INIZIALI sono:"+ CN.size());
  //System.out.println("E le CN (sono le Non FREE iniziali):"+CN);
    
  while (!Q.isEmpty()) {
      //set the possible candidate network to be analyzed
      ArrayList<GraphNode> PCN = Q.remove(0);
  //EXPANSION PHASE
   //find the out connections with the final node in the PCN list
   for (int i = 0; i < PCN.get(PCN.size()-1).get_OutEdge().size(); i++){
    ArrayList<GraphNode> temp = new ArrayList(PCN);                              // Memorizzo PCN a cui va aggiunto il figlio
       
       // Pruning //
       //Condition 1: # of non-free tuple don't exceed the number of keywords
       int Count_Q = 0;
       for (int k = 0; k < temp.size(); k++){                 
        if (temp.get(k).getLabel().contains("_Q")){
         Count_Q++;                                    // Count number of non-free tuple set
        }   
       }
       
       // Aggiungiamo a temp il figlio
       temp.add(G.get(PCN.get(PCN.size()-1).get_OutEdge().get(i))); 
       
       // Condition 1: # of non-free tuple don't exceed the number of keywords AND tree of tuples depth lower than max_bound
       if ((Count_Q < numKeyword) && (temp.size() <= max_bound) )  {
        
        //Condition 2: No leaf tuple set of s are free
        if (temp.get(temp.size()-1).getLabel().contains("_Q")){
         
         // Verify that temp has more than two node so verify condition 3
         if(temp.size()>2){
        //set the names of the last node and the last-3 node
           String final_node = temp.get(temp.size()-1).getLabel();
           String prepre_node = temp.get(temp.size()-3).getLabel();
                
           //verify if the two names are equals
           if(final_node.equals(prepre_node)){
                String pre_node = temp.get(temp.size()-2).getLabel();
             int pre_node_index = ((Integer)map.get(pre_node)).intValue();    
             ArrayList<Integer> inEdge = temp.get(temp.size()-1).get_InEdge();
             
             if (!(inEdge.contains(pre_node_index))){
              Q.add(temp);
              CN.add(temp);
              //System.out.println("Ho creato CN in OUT " +CN.get(i).get(0).getLabel());
                   }
              
               } else {  //l'ultimo nome ha un label diverso dal terzultimo
                Q.add(temp);
                CN.add(temp);
                //System.out.println("Ho creato CN in OUT " +CN.get(i).get(0).getLabel());
                 }
               
         } else{   // temp non contiene pi� di due nodi ma verifica le prime 2 condizioni
              Q.add(temp);
              CN.add(temp);  
              //System.out.println("Ho creato CN in OUT " + CN.get(i).get(0).getLabel());
           }
         
        } else {
            if (temp.size()>2) {
             //set the names of the last node and the last-3 node
             String final_node = temp.get(temp.size()-1).getLabel();
             String prepre_node = temp.get(temp.size()-3).getLabel();
              
             //verify if the two names are equals
             if(final_node.equals(prepre_node)){
              String pre_node = temp.get(temp.size()-2).getLabel();
              int pre_node_index = ((Integer)map.get(pre_node)).intValue();    
              ArrayList<Integer> inEdge = temp.get(temp.size()-1).get_InEdge();
              if (!(inEdge.contains(pre_node_index))){
               Q.add(temp);
              }
             } else 
                Q.add(temp);
               
             
            } else  // L'ultimo nodo aggiunto � una foglia e in totale ci sono meno di due nodi
               Q.add(temp);
            
        
             }      
   } // end if condition 1        
  } // end for cycle
   
   //find the in connections with the final node in the PCN list
   for (int i = 0; i < PCN.get(PCN.size()-1).get_InEdge().size(); i++){
    ArrayList<GraphNode> temp = new ArrayList(PCN); // Memorizzo PCN a cui va aggiunto il figlio
       
       
       
       // Pruning //
       //Condition 1: # of non-free tuple don't exceed the number of keywords
       int Count_Q = 0;
       for (int k = 0; k < temp.size(); k++){                 
        if (temp.get(k).getLabel().contains("_Q"))
         Count_Q++;                                    // Count number of non-free tuple set    
       }
       
       temp.add(G.get(PCN.get(PCN.size()-1).get_InEdge().get(i)));  // Aggiungiamo a temp il figlio
       
       // Condition 1: # of non-free tuple don't exceed the number of keywords AND tree of tuples depth lower than max_bound
       if ((Count_Q < numKeyword) && (temp.size() <= max_bound) )  {
        
        //Condition 2: No leaf tuple set of s are free
        if (temp.get(temp.size()-1).getLabel().contains("_Q")){
         
                Q.add(temp);
                CN.add(temp);
                //System.out.println("Ho creato CN in IN " +CN.get(i).get(0).getLabel());
         
        } else
               Q.add(temp);     
        
   } // end if condition 1        
  } // end for cycle
   
} //  while che svuota la coda
  System.out.println("Le CN FINALI totali sono:"+ CN.size());
  //System.out.println("Le CN FINALI:"+CN);
  return CN;
}
}