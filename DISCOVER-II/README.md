## Contributors
* Alessandro Brighente	<alessandro.brighente@studenti.unipd.it>
* Piergiorgio Ceron		<piergiorgio.ceron@studenti.unipd.it>
* Stefano Marchesin		<stefano.marchesin@studenti.unipd.it>
* Riccardo Simionato	<riccardo.simionato.1@studenti.unipd.it>

### Implementation
This project is is provided as an Eclipse Java 8 project. 
To run it, import the project in Eclipse and check that postgresql-9.4.1208.jre7
is in the list of the referenced libraries.

### Execution
IR_Index is the main file in path ..\discobranda\keyword\src\keyword\IR_Index.java, where must be set the config values:
- in rows 13-14 must be added the POSTGRE DB credentials, 
- between rows 46-63 there are the config parameters like keywords to search in a given DB,
 
 Two algorithm implementations are been done, Naive algorithm and Sparse Algorithm.
 Default algorithm is Sparse algorithm, to try Naive algorithm un-comment from row 118-128 and comment rows in range 131-149.

File auto_test is similar to IR_Index but compute evaluation of precision and mean precision given a list of queries, each in a different text file, provided by the two DBs, Mondial and IMDb.
 
Each file has descriptions and comments in most rows to understand ours implementation.




