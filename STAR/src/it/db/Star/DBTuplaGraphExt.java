package it.db.Star;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Estende SortedWeightedGraph. Aggiunge il concetto di frontiera 
 * (vertici attraverso i quali si pu� espandere il grafo), 
 *  vertici terminali (che contengono almeno una delle keyword cercate)
 *  e di completezza (il grafo � completo quando contiene tutte le keyword)
 */

public class DBTuplaGraphExt extends DBTuplaGraph implements Comparable<DBTuplaGraphExt> {

	protected ArrayList<DBTuplaVertex> front; 	//la frontiera del set
	protected ArrayList<DBTuplaVertex> terminals;
	protected ArrayList<String> keyWords;
	protected ArrayList<String> words;
	public float searchPoints;
	
	public ArrayList<ForeignKeyEdge> mergePoints;
	
	public DBTuplaGraphExt(DBTupla tupla, Collection<String> keyWords) {
		this(keyWords);
		addTupla(tupla);
	}
	
	public DBTuplaGraphExt(DBTuplaVertex vertex, Collection<String> keyWords) {
		this(keyWords);
		addVertex(vertex);
	}
	
	public DBTuplaGraphExt(Collection<String> keyWords) {
		super();
		front 	  = new ArrayList<DBTuplaVertex>();
		terminals = new ArrayList<DBTuplaVertex>();
		mergePoints  = new ArrayList<ForeignKeyEdge>();
		
		int length = keyWords.size();
		this.keyWords = new ArrayList<String>(length);
		words    	  = new ArrayList<String>(length);
		
		searchPoints = 0;
		
		this.keyWords.addAll(keyWords);
	}
	
	/* Aggiunge al grafo la nuova tupla wrappandola in un DBTuplaVertex.
	 * e inserendolo nel front. Se il vertice contiene una tupla con una 
	 * keyword questa viene aggiunta alla lista words del grafo.
	 * Restituisce il nuovo vertice del grafo.
	 * @param tupla
	 * @return
	 */
	public DBTuplaVertex addTupla(DBTupla tupla) {
		DBTuplaVertex vertex = new DBTuplaVertex(tupla);
		addVertex(vertex);
		return vertex;
	}
	
	@Override
	/**
	 * Inserisce il vertice nel grafo e lo aggiunge al front.
	 * Se il vertice contiene una tupla con una keyword questa
	 * viene aggiunta alla lista words del grafo
	 */
	public void addVertex(DBTuplaVertex vertex) {
		super.addVertex(vertex);
		front.add(vertex);
		assert(vertexes.size() >= front.size()) : "Error! Front can't be bigger than vertexes!";
		for(String newWord:vertex.tupla.keyWords) {
			if(newWord != null) {
				terminals.add(vertex);
				if(!words.contains(newWord))
					words.add(newWord);
			}
		}
		searchPoints += vertex.tupla.searchPoints;
		return;
	}
	
	public boolean specialRemoveVertex(DBTuplaVertex vertex) {
		front.remove(vertex);
		return super.specialRemoveVertex(vertex);
	}
	
	@Override
	public ArrayList<DBTuplaVertex> removeVertex(DBTuplaVertex vertex) {
		ArrayList<DBTuplaVertex> removed = super.removeVertex(vertex);
		for(DBTuplaVertex v : removed) {
			front.remove(v);
			terminals.remove(v);
			words.removeAll(v.tupla.keyWords);
		}
		return removed;
	}
	
	public void merge(DBTuplaVertex vertex1, DBTuplaVertex vertex2, ForeignKey foreignKey) {
		
		DBTuplaGraphExt graph = (DBTuplaGraphExt)vertex2.containingGraph;
		
		super.merge(graph);
		front.addAll(graph.front);
		terminals.addAll(graph.terminals);
		for(String keyWord:graph.keyWords)
			if(!keyWords.contains(keyWord))
				keyWords.add(keyWord);
		for(String word:graph.words)
			if(!words.contains(word))
				words.add(word);
		mergePoints.addAll(graph.mergePoints);
		
		ForeignKeyEdge parentEdge = vertex2.parentEdge;
		ForeignKeyEdge edge = addEdge(vertex1, vertex2, foreignKey);
		vertex2.parentEdge = parentEdge;
		mergePoints.add(edge);
	}
	

	/**
	 * Cerca nel grafo le componenti connesse e restituisce delle copie
	 * dei grafi che le rappresentano
	 * @return
	 */
	public ArrayList<DBTuplaGraphExt> getConnectedComponents() {
		ArrayList<DBTuplaGraphExt> connComp = new ArrayList<DBTuplaGraphExt>();
		LinkedList<DBTuplaVertex> toProcess = new LinkedList<DBTuplaVertex>();
		toProcess.addAll(vertexes);
		
		while(!toProcess.isEmpty()) {	// ogni vertice del grafo...
			DBTuplaVertex vertex = toProcess.pop();
			boolean alreadyProcessed = false;
			for(Iterator<DBTuplaGraphExt> iter = connComp.iterator(); 
					iter.hasNext() && !alreadyProcessed;) {
				if(iter.next().contains(vertex))
					alreadyProcessed = true;
			}
			if(!alreadyProcessed) {	// ... se non � gi� presente in una componente connessa...
				DBTuplaGraphExt newGraph = new DBTuplaGraphExt(new DBTuplaVertex(vertex), keyWords);
				newGraph.expand(this); // ... � il vertice di partenza di una nuova componente connessa
				connComp.add(newGraph);
			}
		}
		
		return connComp;
	}
	
	
	/**
	 * Espande il grafo seguendo le connessioni del grafo specificato
	 * @param graph
	 */
	private void expand(DBTuplaGraphExt subGraph) {
		
		int size = sizeVertexes();
		ArrayList<DBTuplaVertex> oldFront = front;
		clearFront();
		for(DBTuplaVertex vertex : oldFront) {
			DBTuplaVertex subVertex = subGraph.getVertex(vertex);
			for(ForeignKeyEdge edge : subVertex.edges) {
				if(!vertex.edges.contains(edge)) {
					DBTuplaVertex newVertex = new DBTuplaVertex(edge.otherSide(subVertex));
					addVertex(newVertex);
					addEdge(vertex, newVertex, edge.foreignKey);
				}
			}
		}
		
		if(size < sizeVertexes()) {
			expand(subGraph);
		}
		return;
	}
	
	/**
	 * @return un ArrayList contenente le DBTupla del front
	 */
	//public ArrayList<DBTuplaVertex> getFront()     { return front; }
	public ArrayList<DBTuplaVertex> getTerminals() { return terminals; }

	
	public DBTuplaVertex getVertex(DBTupla tupla) {
		return getVertex(new DBTuplaVertex(tupla));
	}
	
	/**
	 * Svuota il front lasciando gli elementi del front nel set
	 */
	public void clearFront() {
		front = new ArrayList<DBTuplaVertex>();
	}
	
	
	public boolean removeFront() {
		for(DBTuplaVertex vertex:front)
			if(removeVertex(vertex).isEmpty())
				return false;
		clearFront();
		return true;
	}
	
	/**
	 * Il set � completo quando contiene almeno una tupla per keyword
	 * @return true se � completo
	 * 		   false altrimenti
	 */
	public boolean isComplete() { return words.containsAll(keyWords); }
	
	@Override
	public String toString() {
		String str = "Total tuplas: " + sizeVertexes();
		str += "; front tuplas: " + front.size();
		str += "; edges: " + sizeEdges();
		str += "; SEARCHPOINTS: " + searchPoints;
		str += "; words: ";
		for(String s:words)
			str += "[" + s + "], ";
		return str;
	}

	@Override
	public int compareTo(DBTuplaGraphExt graph) {
		int size1 = words.size();
		int size2 = graph.words.size();
		if(size1 < size2)
			return 1;
		if(size1 > size2)
			return -1;
		if(searchPoints < graph.searchPoints)
			return 1;
		if(searchPoints > graph.searchPoints)
			return -1;
		return 0;
	}

}
