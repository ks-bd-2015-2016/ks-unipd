package it.db.Star;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

public class DBTuplaGraph implements Iterable<DBTuplaVertex>{

	protected TreeSet<DBTuplaVertex>  vertexes;
	protected TreeSet<ForeignKeyEdge> edges;

	public DBTuplaGraph() {
		vertexes = new TreeSet<DBTuplaVertex>();
		edges    = new TreeSet<ForeignKeyEdge>();
	}



	/**
	 * Aggiunge un edge tra due vertex che fanno parte del grafo.
	 * L'appartenenza al grafo di v1 e v2 non viene verificata
	 * @param v1
	 * @param v2
	 * @param foreignKey
	 * @return
	 */
	public ForeignKeyEdge addEdge(DBTuplaVertex v1, DBTuplaVertex v2, ForeignKey foreignKey) {
		assert (v1.containingGraph == this && v2.containingGraph == this)	: "Errore uno dei due vertici non � di questo containingGraph";
		ForeignKeyEdge edge = new ForeignKeyEdge(v1, v2, foreignKey);
		edges.add(edge);
		v1.addEdge(edge);
		v2.addEdge(edge);
		v2.parentEdge = edge;
		return edge;
	}

	/**
	 * Aggiunge al grafo il vertice specificato. Prima dell'inserimento
	 * vengono rimossi dal vertice tutti gli edge poich� esssendo inserito
	 * singolarmente non pu� essere connesso ad altri vertici del grafo
	 * @param v
	 * @return
	 */
	public void addVertex(DBTuplaVertex v) { 
		v.clearEdges();
		v.containingGraph = this;
		vertexes.add(v);
		return;
	}
	
	/**
	 * Aggiunge al grafo tutti i vertex e tutti gli edge presenti
	 * nel grafo specificato.
	 * @param graph
	 */
	public void merge(DBTuplaGraph graph) {
		for(Iterator<DBTuplaVertex> iter = graph.iteratorVertexes(); iter.hasNext();) {
			DBTuplaVertex vertex = iter.next();
			vertex.containingGraph = this;
			vertexes.add(vertex);
		}
		
		for(Iterator<ForeignKeyEdge> iter = graph.iteratorEdges(); iter.hasNext();) {
			ForeignKeyEdge edge = iter.next();
			assert (edge.vertex1.containingGraph == this && edge.vertex2.containingGraph == this) : "ERROR GNE";
			edges.add(edge);
		}
	}
	
	

	/**
	 * Restituisce l'edge del grafo che verifica edge.equals(e)
	 * se un simile edge non esiste restituisce null
	 * @param e
	 * @return
	 */
	public ForeignKeyEdge getEdge(ForeignKeyEdge edge) {
		ForeignKeyEdge e = edges.ceiling(edge);
		if(e.equals(edge))
			return e;
		return null;
	}

	/**
	 * Restituisce il vertice del grafo che verifica vertex.equals(v)
	 * se un simile vertice non esiste restituisce null
	 * @param vertex
	 * @return
	 */
	public DBTuplaVertex getVertex(DBTuplaVertex vertex) {
		DBTuplaVertex v = vertexes.ceiling(vertex);
		if(v.equals(vertex))
			return v;
		return null;
	}
	
	/**
	 * metodo che non rimuove vertici rimasti isolati
	 * @param edge
	 * @return
	 */
	public boolean specialRemoveEdge (ForeignKeyEdge edge) {
		edge.vertex1.edges.remove(edge);
		edge.vertex2.edges.remove(edge);
		return edges.remove(edge); 
	}

	/**
	 * Rimuove l'edge specificato dal grafo. Un vertice che rimane 
	 * isolato viene rimosso anch'esso dal grafo
	 * @param edge
	 * @return
	 */
	private ArrayList<DBTuplaVertex> removeEdge(ForeignKeyEdge edge) {
		ArrayList<DBTuplaVertex> removed = new ArrayList<DBTuplaVertex>();
		edge.vertex1.edges.remove(edge);
		edge.vertex2.edges.remove(edge);
		if(edge.vertex1.numOfEdges() == 0) {
			vertexes.remove(edge.vertex1);
			removed.add(edge.vertex1);
		}
		if(edge.vertex2.numOfEdges() == 0) {
			vertexes.remove(edge.vertex2);
			removed.add(edge.vertex2);
		}
		edges.remove(edge); 
		return removed;
	}
	
	public boolean specialRemoveVertex(DBTuplaVertex vertex) {
		ArrayList<ForeignKeyEdge> edges = vertex.edges;
		vertex.clearEdges();
		for(ForeignKeyEdge edge:edges)
			specialRemoveEdge(edge);
		vertexes.remove(vertex);	// TODO: trovare un modo pi� intelligente
									// per scrivere sta roba. Il vertice viene
									// probabilmente gi� rimosso da removeEdge
		return true;
	}

	/**
	 * Rimuove il vertice specificato e tutti gli edge ad esso attaccati
	 * Se a seguito di questa operazione un altro vertice rimane
	 * isolato viene rimosso anch'esso dal grafo
	 * @param vertex
	 * @return
	 */
	public ArrayList<DBTuplaVertex> removeVertex(DBTuplaVertex vertex) {
		ArrayList<DBTuplaVertex> removed = new ArrayList<DBTuplaVertex>(0);
		ArrayList<ForeignKeyEdge> edges = vertex.edges;
		vertex.clearEdges();
		for(ForeignKeyEdge edge:edges)
			removed.addAll(removeEdge(edge));
		vertexes.remove(vertex);	// TODO: trovare un modo pi� intelligente
									// per scrivere sta roba. Il vertice viene
									// probabilmente gi� rimosso da removeEdge
		removed.add(vertex);
		return removed;
	}
	
	
	/**
	 * Restituisce una lista degli edge del grafo
	 * @return
	 */
	public ArrayList<ForeignKeyEdge> listEdges() {
		ArrayList<ForeignKeyEdge> list = new ArrayList<ForeignKeyEdge>(sizeEdges());
		Iterator<ForeignKeyEdge> iter = iteratorEdges();
		while(iter.hasNext())
			list.add(iter.next());
		return list;
	}

	/**
	 * Restituisce una lista dei vertici del grafo
	 * @return
	 */
	public ArrayList<DBTuplaVertex> listVertexes() {
		ArrayList<DBTuplaVertex> list = new ArrayList<DBTuplaVertex>(sizeVertexes());
		Iterator<DBTuplaVertex> iter = iteratorVertexes();
		while(iter.hasNext())
			list.add(iter.next());
		return list;
	}

	public int sizeVertexes() { return vertexes.size();	}
	public int sizeEdges()    { return edges.size();	}

	public boolean isEmpty() { return sizeVertexes() == 0; }

	public void clear() {
		vertexes.clear();
		edges.clear();		
	}

	public boolean contains(DBTuplaVertex  v) { return vertexes.contains(v); }
	public boolean contains(ForeignKeyEdge e) {	return edges.contains(e); }
	
	public Iterator<DBTuplaVertex>  iteratorVertexes() { return vertexes.iterator(); }
	public Iterator<ForeignKeyEdge> iteratorEdges()    { return edges.iterator(); }

	@Override
	public Iterator<DBTuplaVertex> iterator() {
		return iteratorVertexes();
	}

}
