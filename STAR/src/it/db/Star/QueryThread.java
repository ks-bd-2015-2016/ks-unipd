package it.db.Star;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.db.Star.QueryManager.Query;
import it.db.Star.QueryManager.QueryType;

public class QueryThread extends Thread {

	private int thrID;
	private Connection conn;
	private ConcurrentLinkedQueue<Query> queries;
	private ArrayList<DBTupla> resultSet;
	private ArrayList<String> keyWords;
	private Pattern[] patterns;

	private String currTableName;
	private String[] currColumnNames;
	private int[] currTypes;
	private int currTable;

	public QueryThread(int id, Connection connection, ConcurrentLinkedQueue<Query> queries, ArrayList<DBTupla> resultSet) {
		thrID = id;
		conn = connection;
		this.queries = queries;
		this.resultSet = resultSet;
	}

	public void start(ArrayList<String> keyWords) {
		this.keyWords = keyWords;
		patterns = new Pattern[keyWords.size()];
		for(int i = 0; i < keyWords.size(); i++) {
			patterns[i] = Pattern.compile("(.*[\\s,-]+)?" + keyWords.get(i) + "([,\\s-]+.*)?", Pattern.CASE_INSENSITIVE);
		}
		start();
	}

	public void run() {
		Query query = queries.poll();
		while(query != null) {

			currTable = query.table;
			String sqlQuery = query.sqlQuery;
			System.out.println("Thread " + thrID + " executing query: " + sqlQuery);

			// recupera i metadati associati alla tabella
			currColumnNames = DBMetaData.getColumns(currTable);
			currTableName   = DBMetaData.getTable(currTable);
			currTypes       = DBMetaData.getTypes(currTable);

			// effettua la query
			try {
				long time = System.nanoTime();
				ResultSet result = conn.createStatement().executeQuery(sqlQuery);
				System.out.println("Thread " + thrID + " query time: " + (System.nanoTime() - time)/1000000 + "ms");
				// trasforma i risultati in DBTupla
				while(result.next()) {
					Object[] values = new Object[currColumnNames.length];
					for(int i = 0; i < currColumnNames.length; i++)
						values[i] = result.getObject(currColumnNames[i]);
				
					DBTupla tupla = evaluateRelevance(result, values, query.type);
					if(tupla != null)
						resultSet.add(tupla);
				}
			} catch (SQLException e) { 
				System.err.println(e + " " + sqlQuery);
			}

			query = queries.poll();
		}

		synchronized (queries) {
			queries.notify();
		}
	}

	DBTupla evaluateRelevance(ResultSet result, Object[] values, QueryType queryType) {
		ArrayList<String> words = new ArrayList<String>();
		float searchPoints = 0;
		boolean add = false;
		
		if(currTableName.equals("title") || currTableName.equals("name"))
			searchPoints++;
		if (currTableName.equals("name") && values[2]!= null)
			if (values[2].equals("I") )
				searchPoints++;
		
		int[][] matching = new int[values.length][keyWords.size()];
		for(int i = 0; i < keyWords.size(); i++) {
			String keyWord = keyWords.get(i);
			Integer number = null;
			try {
				number = Integer.parseInt(keyWord);
			} catch(NumberFormatException e) {	}
			if(number == null) {
				if(queryType == QueryType.queryWholeTable && currTableName.equals(keyWord)) {
					searchPoints++;
					words.add(keyWord);
					add = true;
				}
				else {
					for(int j = 0; j < currColumnNames.length; j++) {
						try {
							if(currTypes[j] != Types.INTEGER && result.getInt("col_" + i + "_" + j) > 0) {
								matching[j][i] = 1;
								if(currColumnNames[j].equals(keyWord))
									searchPoints++;
								add = true;
								if(!words.contains(keyWord))
									words.add(keyWord);
								break;
							}
						} catch(SQLException e) {
							System.err.println("Error! Can't retrieve " 
									+ "col_" + i + "_" + j);
						}
					}
				}
			}
			else {
				for(int j = 0; j < currColumnNames.length; j++) {
					try {
						if(result.getInt("col_" + i + "_" + j) > 0) {
							matching[j][i] = 1;
							if(currColumnNames[j].equals(keyWord))
								searchPoints++;
							//Matcher matcher = patterns[i].matcher(values[j].toString().toLowerCase());
							//if (matcher.matches()) {
							add = true;
							if(!words.contains(keyWord))
								words.add(keyWord);
							break;
							//}
						}
					} catch(SQLException e) {
						System.err.println("Error! Can't retrieve " 
								+ "col_" + i + "_" + j);
					}
				}
			}
		}
		float prePoints = 0;
		if (searchPoints != 0)
			prePoints = searchPoints;
		float fractions = 0;
		for (int j = 0; j < currColumnNames.length; j++) {
			if (currTypes[j] != Types.INTEGER && values[j] != null) {
				String value = (String) values[j];
				int matchedKeywordsSize = 0;
				for(int i = 0; i < keyWords.size(); i++) {
					if (matching[j][i] == 1)
						matchedKeywordsSize += keyWords.get(i).length();
				}
				fractions += (float) matchedKeywordsSize / value.length();
			}
		}
		searchPoints += fractions;
		if(add)
			return new DBTupla(currTable, values, words, searchPoints);
		return null;

	}

}
