package it.db.Star;

import java.util.ArrayList;

public class LoosePath implements Comparable<LoosePath> {
	public DBTuplaVertex start;
	public DBTuplaVertex end;
	public long weight;
	public ArrayList<DBTuplaVertex> nodes;
	
	public String toString() {
		return "From " +start + " to " + end +", weight "+ weight;
	}
	
	
	
	public LoosePath (DBTuplaVertex s, DBTuplaVertex e, long w, ArrayList<DBTuplaVertex> n) {
		start = s;
		end = e;
		weight = w;
		nodes = n;
	}
	
	public int compareTo(LoosePath other) {	//TODO non sono sicuro se vada bene cosi
		if (this.weight == other.weight)
			return 0;
		if (this.weight < other.weight)
			return -1;
		return 1;
	}
	
	public boolean contains (DBTuplaVertex t1, DBTuplaVertex t2) {
		return ((start.equals(t1) || (start.equals(t2))) && (end.equals(t1) || (end.equals(t2))));
	}
}
