package it.db.Star;

import java.util.ArrayList;

public class DBTuplaVertex implements Comparable<DBTuplaVertex> {
	
	public DBTupla tupla;
	public ArrayList<ForeignKeyEdge> edges;
	public DBTuplaGraph containingGraph;  //set che contiene questo oggetto
	public ForeignKeyEdge parentEdge;
	
	public int d1 = 0;	// servono per l'algorimo
	public int d2 = 0;	// almeno credo
	public DBTuplaVertex pred1 = null;
	public DBTuplaVertex pred2  = null;
	
	public DBTuplaVertex(DBTupla tupla) {
		this.tupla = tupla;
		edges = new ArrayList<ForeignKeyEdge>();
		parentEdge = null;
	}
	
	/**
	 * Crea un nuovo verex partendo da un vertex esistente. 
	 * Copia tutto tranne
	 * parentEdge
	 * @param vertex
	 */
	public DBTuplaVertex(DBTuplaVertex vertex) {
		this(vertex.tupla);
		edges.addAll(vertex.edges);
		containingGraph = vertex.containingGraph;
		d1 = vertex.d1;
		d2 = vertex.d2;
		pred1 = vertex.pred1;
		pred2 = vertex.pred2;
		parentEdge = vertex.parentEdge;
	}

	
	
	@Override
	public int compareTo(DBTuplaVertex vertex) {
		return tupla.compareTo(vertex.tupla);
	}
	
	@Override
	public boolean equals(Object o) {
		DBTuplaVertex vertex = (DBTuplaVertex)o;
		return tupla.equals(vertex.tupla);
	}
	
	public boolean addEdge(ForeignKeyEdge edge) {
		return edges.add(edge);
	}
	
	public int numOfEdges() {
		return edges.size();
	}
	
	public void clearEdges() {
		edges = new ArrayList<ForeignKeyEdge>();
	}
	
	@Override
	public String toString() {
		return "VERTEX TUPLA: " + tupla.toString() + "\n" 
				+ "NUMBER OF EDGES: " + edges.size();
	}

}
