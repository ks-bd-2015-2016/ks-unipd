package it.db.Star;

import java.util.ArrayList;

/**
 * Classe che rappresenta una tupla del DB
 */
public class DBTupla implements Comparable<DBTupla>{
	
	private Object[] 		  values; 			  //contenuto della tupla ordinato per nome colonna
	public  DBTuplaGraph 	  containingGraph; //set che contiene questo oggetto
	public  ArrayList<String> keyWords;			  //keyword per la quale questa tupla ha un match
	private Long 	 		  hash; 
	public  int 	 		  table;
	
	public float searchPoints;
	
	public DBTupla(int tab, Object[] val, ArrayList<String> words, float searchPoints) {	
		table 	  	    = tab;
		values 		    = val;
		containingGraph = null;
		keyWords 	    = words;
		hash 		    = null;
		this.searchPoints = searchPoints;
	}
	
	public DBTupla(int table, Object[] val) {		
		this(table, val, new ArrayList<String>(0), 0);
	}
	
	/**
	 * Restituisce i valori di una foreign key di questa tupla
	 * @param forKey
	 * @return i valori sulle colonne che compongono la foreign key
	 */
	public Object getValue(int column) {
		return 	values[column];
	}
	
	public Object getValue(ForeignKey foreignKey) {
		return values[foreignKey.column];
	}
	
	/**
	 * Metodo che verifica l'uguaglianza sulle primary key
	 * tra due oggetti DBTupla
	 * @param obj: oggetto con cui confrontare this
	 * @return: true se gli oggetti sono uguali,
	 * 			false altrimenti
	 */
	@Override
	public boolean equals(Object obj) {
		DBTupla tupla = (DBTupla)obj;
		return getHash().equals(tupla.getHash());
	}
	
	public Long getHash() {
		if(hash != null)
			return hash;
		hash = longHash();
		return hash;
	}
	
	@Override
	public int hashCode() {
		int[] primaryKey = DBMetaData.getPrimaryKeysIdx(table);
		int result = 23*37 + DBMetaData.getTable(table).hashCode();
		for(int key:primaryKey) {
			result = result*37 + values[key].hashCode();
		}
		return result;
	}
	
	private Long longHash() {
		int[] primaryKey = DBMetaData.getPrimaryKeysIdx(table);
		Long result = new Long(23);
		result = result*37 + DBMetaData.getTable(table).hashCode();
		for(int key:primaryKey) {
			result = result*37 + values[key].hashCode();
		}
		return result;
	}
	
	@Override
	public String toString() {
		String str = DBMetaData.getTable(table) + ":	";
		String[] columns = DBMetaData.getColumns(table);
		for(int i = 0; i < columns.length; i++) {
			Object val = values[i];
			if(val != null)
				str += columns[i] + " = " + val + ", ";
			else
				str += columns[i] + " = null, ";
		}
		return str;
	}

	@Override
	public int compareTo(DBTupla tupla) {
		return getHash().compareTo(tupla.getHash());
	}
}
