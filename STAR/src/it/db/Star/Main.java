package it.db.Star;

import java.sql.*;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		try{
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException cnfe){
			System.out.println("Could not find the JDBC driver!");
			System.exit(1);
		}
		System.out.println("Driver found");
		
		Connection[] conn = new Connection[5];
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter DB url (es: jdbc:postgresql://hostaddress:portnumber/dbname)");
		String url = scan.nextLine();
		System.out.println("Please enter user name");
		String userName = scan.nextLine();
		System.out.println("Please enter password");
		String password = scan.nextLine();
		System.out.println("Establishing connection...");
		try {
			for(int i = 0; i < conn.length; i++)
				//conn[i] = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "postgres");
				conn[i] = DriverManager.getConnection(url, userName, password);
		} catch (SQLException sqle) {
			System.out.println("Could not connect");
			System.exit(1);
		}
		System.out.println("Successfully connected");
		
		System.out.println("Loading DB metadata...");
		QueryManager q = new QueryManager(conn, "public");
		System.out.println("Finished loading DB metadata...");
		System.out.println(DBMetaData.getInstance().toString());
		
		String[] tables = DBMetaData.getTables();
		for(String table:tables) {
			System.out.println(table);
		}
		
		System.out.println("Creating tuplas...");
		
		ArrayList<String> keyWords = new ArrayList<String>();
		System.out.println("What are you looking for?");
		String s = scan.nextLine().toLowerCase();
		scan.close();
		String[] splitted = s.split("\\s");
		for(String word:splitted) {
			if(!keyWords.contains(word))
				keyWords.add(word);
		}
/*
		//keyWords.add("name");
<<<<<<< HEAD
		
		keyWords.add("wizard");
		keyWords.add("of");
		keyWords.add("oz");
=======
		
		keyWords.add("reeves");
		keyWords.add("wachowski");
		/*keyWords.add("offer");
		keyWords.add("he");
		keyWords.add("can't");
		keyWords.add("refuse");*/

		/*keyWords.add("my");
		keyWords.add("dear");
		keyWords.add("i");
		keyWords.add("don't");
		keyWords.add("give");
		keyWords.add("a");
		keyWords.add("damn");*/
		
		
		
		long time = System.currentTimeMillis();
		FirstPhaseTreeBuilder builder = new FirstPhaseTreeBuilder(q);
		DBTuplaGraphSet treeSet = builder.getInterconnectingTree(keyWords);
		
		System.out.println("\n\n" + (treeSet == null ? "No solution!" : treeSet.toString()));
		System.out.println("\n\nTime elapsed: " + (System.currentTimeMillis()-time));
		
		System.out.println("Look for terminals time: " + builder.lookForTerminalsTime/1000000 + "ms");
		System.out.println("Commencing star");
		//Star star = new Star(treeSet);
		
		DBTuplaGraphExt tree = treeSet.iterator().next();
		if(tree.vertexes.size() != tree.edges.size() + 1) {
			@SuppressWarnings("unused")
			boolean error = true;
		}
		
		System.out.println(tree);
		for(DBTuplaVertex vertex : tree) {
			System.out.println(vertex);
		}
		//System.out.println(treeSet.iterator().next());
		//System.out.println("Starting improvement");
		//star.tryImproveTree();
		//System.out.println(treeSet.iterator().next());
		
		/*
		System.out.println("Terminals : ");
		for(DBTuplaVertex t : star.getTerminals())
			System.out.println(t.toString());
		System.out.println("\n\nTime elapsed: " + (System.currentTimeMillis()-time));
		*/
		
	}
	
}
