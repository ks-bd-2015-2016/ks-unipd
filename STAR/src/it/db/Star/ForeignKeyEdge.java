package it.db.Star;

public class ForeignKeyEdge implements Comparable<ForeignKeyEdge> {

	public DBTuplaVertex vertex1;
	public DBTuplaVertex vertex2;
	public ForeignKey foreignKey;
	public double weight;
	
	public ForeignKeyEdge(DBTuplaVertex vertex1, DBTuplaVertex vertex2, ForeignKey foreignKey) {
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.foreignKey = foreignKey;
		weight = 1;
	}
	
	/**
	 * Restituisce la DBTupla che si trova all'altro lato dell'edge
	 * rispetto alla DBTupla data
	 * @param tupla
	 * @return
	 */
	public DBTuplaVertex otherSide(DBTuplaVertex tupla) {
		if(tupla.equals(vertex1))
			return vertex2;
		return vertex1;
	}
	
	@Override
	public int compareTo(ForeignKeyEdge forKeyEdge) {
		int compare = foreignKey.compareTo(forKeyEdge.foreignKey);
		if(compare != 0)
			return compare;
		compare = vertex1.compareTo(forKeyEdge.vertex1);
		if(compare != 0)
			return compare;
		return vertex2.compareTo(forKeyEdge.vertex2);
	}
	
	@Override
	public boolean equals(Object o) {
		ForeignKeyEdge forKeyEdge = (ForeignKeyEdge)o;
		if(foreignKey.equals(forKeyEdge.foreignKey))
			if(vertex1.equals(forKeyEdge.vertex1) && vertex2.equals(forKeyEdge.vertex2))
				return true;
			else if(vertex2.equals(forKeyEdge.vertex1) && vertex1.equals(forKeyEdge.vertex2))
				return true;
		return false;
	}
	
	@Override
	public String toString() {
		return vertex1.toString() + "\n" + vertex2.toString() + "\n" + foreignKey.toString(); 
	}

}
