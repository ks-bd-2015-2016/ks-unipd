package it.db.Star;

public class ForeignKey implements Comparable<ForeignKey>{
	public int table;
	public int column;		    //nomi delle colonne che compongono la foreign key
	public int foreignColumn;
	public int foreignTable;	//nome della tabella cui si riferiscono
	public boolean outgoing;

	public ForeignKey(int tab, int col, int foreignTab, int foreignCol, boolean outgoing) {
		table		  = tab;
		column		  = col;
		foreignTable  = foreignTab;
		foreignColumn = foreignCol;
		this.outgoing = outgoing;
	}
	
	public ForeignKey(int tab, String col, String foreignTab, String foreignCol, boolean outgoing) {
		table		  = tab;
		
		String[] columns = DBMetaData.getColumns(tab);
		for(int i = 0; i < columns.length; i++)
			if(col.equals(columns[i])) {
				column = i;
				break;
			}
		
		String[] tables = DBMetaData.getTables();
		for(int i = 0; i < tables.length; i++)
			if(foreignTab.equals(tables[i])) {
				foreignTable = i;
				break;
			}
		
		columns = DBMetaData.getColumns(foreignTable);
		for(int i = 0; i < columns.length; i++)
			if(foreignCol.equals(columns[i])) {
				foreignColumn = i;
				break;
			}
		
		this.outgoing = outgoing;
	}
	
	public ForeignKey(ForeignKey foreignKey) {
		table		  = foreignKey.table;
		column		  = foreignKey.column;
		foreignTable  = foreignKey.foreignTable;
		foreignColumn = foreignKey.foreignColumn;
		this.outgoing = foreignKey.outgoing;
	}
	
	@Override
	public String toString() {
		return DBMetaData.getColumn(table, column) 
				+ (outgoing ? " -> " : " <- ") 
				+ DBMetaData.getTable(foreignTable).toUpperCase() 
				+ "(" + DBMetaData.getColumn(foreignTable, foreignColumn) + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		ForeignKey forKey = (ForeignKey)obj;
		
		long key1 = (table << 32) + column;
		long key2 = (forKey.table << 32) + forKey.column;
		return key1 == key2;
	}

	@Override
	public int compareTo(ForeignKey forKey) {
		long key1 = (table << 32) + column;
		long key2 = (forKey.table << 32) + forKey.column;
		if(key1 < key2)
			return -1;
		if(key1 > key2)
			return 1;
		return 0;
	}
}
