package it.db.Star;

import java.util.ArrayList;

public class DBMetaData {

	private static String[] tables;
	private static String[][] columns;
	private static int[][] primaryKeys;
	private static ForeignKey[][] foreignKeys;
	private static int[][] types;
	
	private static DBMetaData singleton = new DBMetaData( );
	   
	   /* A private Constructor prevents any other 
	    * class from instantiating.
	    */
	private DBMetaData() { }

	/* Static 'instance' method */
	public static DBMetaData getInstance( ) {
		return singleton;
	}
	
	public static String getColumn(int table, int column) {
		return columns[table][column];
	}
	
	public static String[] getColumns(int table) {
		return columns[table];
	}
	
	public static ForeignKey[] getForeignKeys(int table) {
		return foreignKeys[table];
		/*
		ForeignKey[] foreignKeys = new ForeignKey[tableForKeys.length];
		for(int i = 0; i < tableForKeys.length; i++)
			foreignKeys[i] = new ForeignKey(tableForKeys[i]);
		return foreignKeys;
		*/
	}
	
	public static int[] getPrimaryKeysIdx(int table) {
		return primaryKeys[table];
	}
	
	public static String[] getPrimaryKeysStr(int table) {
		int[] keysNum = primaryKeys[table];
		String[] keysStr = new String[keysNum.length];
		for(int i = 0; i < keysNum.length; i++)
			keysStr[i] = columns[table][keysNum[i]];
		return keysStr;
	}
	
	public static int getTableNumber() {
		return tables.length;
	}
	
	public static String getTable(int table) {
		return tables[table];
	}
	
	public static String[] getTables() {
		return tables;
	}
	
	public static int[] getTypes(int table) {
		return types[table];
	}
	
	/* Other methods protected by singleton-ness */
	protected static void demoMethod( ) {
		System.out.println("demoMethod for singleton"); 
	}
	
	public static void setColumnsNames(int table, ArrayList<String> columnNames) {
		if(columns[table] != null)
			return;
		
		columns[table] = new String[columnNames.size()];
		for(int i = 0; i < columnNames.size(); i++)
			columns[table][i] = columnNames.get(i);
	}
	
	public static void setForeignKeys(int table, ArrayList<ForeignKey> forKeys) {
		if(foreignKeys[table] != null)
			return;
			
		foreignKeys[table] = new ForeignKey[forKeys.size()];
		for(int i = 0; i < forKeys.size() ; i++)
			foreignKeys[table][i] = forKeys.get(i);
	}
	
	public static void setPrimaryKeys(int table, ArrayList<String> prKeys) {
		if(primaryKeys[table] != null)
			return;
		
		primaryKeys[table] = new int[prKeys.size()];
		for(int j = 0; j < prKeys.size(); j++) {
			String key = prKeys.get(j);
			for(int i = 0; i < columns[table].length; i++) {
				if(key.equals(columns[table][i]))
					primaryKeys[table][j] = i;
			}
		}
	}
	
	public static void setTablesNames(ArrayList<String> tableNames) {
		if(tables != null)
			return;
		
		int size 	= tableNames.size();
		tables 		= new String[size];
		columns 	= new String[size][];
		primaryKeys = new int[size][];
		foreignKeys = new ForeignKey[size][];
		types		= new int[size][];
		for(int i = 0; i < tableNames.size(); i++)
			tables[i] = tableNames.get(i);
	}
	
	public static void setTypes(int table, ArrayList<Integer> typ) {
		if(types[table] != null)
			return;
		
		types[table] = new int[typ.size()];
		for(int i = 0; i < typ.size(); i++)
			types[table][i] = typ.get(i).intValue();
	}
	
	public String toString() {
		String str = "";
		for(int i = 0; i < tables.length; i++) {
			str += "\n" + tables[i] + ": ";
			for(int j = 0; j < columns[i].length; j++)
				str += columns[i][j] + ", ";
		}
		return str;
	}
	
}
