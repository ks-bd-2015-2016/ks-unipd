package it.db.Star;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.sql.*;

public class QueryManager {
	
	enum QueryType {
		querySpecial,
		queryWholeTable
	}
	
	private Connection[] conn;			// connessione col DB
	private DatabaseMetaData metaData;	// oggetto che rappresenta i metadati del DB
	private String catalog;				// ?
	private String schemaPattern;		// nome dello schema associato a QueryManager
	private int numberOfConnections;
	
	public QueryManager(Connection[] c, String schPattern) {
		conn = c;
		numberOfConnections = c.length;
		schemaPattern = schPattern;
		try{
			metaData = c[0].getMetaData();
			catalog  = c[0].getCatalog();
		} catch(SQLException e) {
			System.err.println("Unable to retrieve DB metadata");
		}
		
		System.out.println("Loading table names...");
		
		// carica i metadati delle tabelle in tables
		setTablesNames();
		int length = DBMetaData.getTableNumber();
		for (int table = 0; table < length; table++) {
			System.out.println("Loading table " + DBMetaData.getTable(table) + "...");
			
			setColumnsNames(table);
			setPrimaryKeys(table);
			setColumnTypes(table);
		}
		
		for (int table = 0; table < length; table++)
			setForeignKeys(table);
	}
	
	private void setColumnTypes(int table) {
		ArrayList<Integer> types = new ArrayList<Integer>();
		try {
			ResultSet result = metaData.getColumns(catalog, schemaPattern, DBMetaData.getTable(table), null);			// ResultSet con le primarykeys
			while (result.next())
				types.add(result.getInt(5));
		} catch (SQLException e) {
			System.out.println(e);
		}
		
		DBMetaData.setTypes(table,types);
	}
	
	private void setPrimaryKeys(int table) {
		ArrayList<String> primaryKeys = new ArrayList<String>();
		try {
			ResultSet result = metaData.getPrimaryKeys(catalog, schemaPattern, DBMetaData.getTable(table));			// ResultSet con le primarykeys
			while (result.next())
				primaryKeys.add(result.getString(4));
		} catch (SQLException e) {
			System.out.println(e);
		}

		DBMetaData.setPrimaryKeys(table,primaryKeys);
	}
	
	private void setForeignKeys(int table) {
		String tableName = DBMetaData.getTable(table);
		ArrayList<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();
		try {
			// cerca le foreign key
			ResultSet rs = metaData.getImportedKeys(catalog, schemaPattern, tableName);
			while (rs.next()) {
				String column		 = rs.getString(8);
				String foreignTable  = rs.getString(3);
				String foreignColumn = rs.getString(4);
				foreignKeys.add(new ForeignKey(table, column, foreignTable,foreignColumn, true));
			}

			// cerca le colonne a cui puntano delle foreign key
			rs = metaData.getExportedKeys(catalog, schemaPattern, tableName);
			while (rs.next()) {
				String column		 = rs.getString(4);
				String foreignTable  = rs.getString(7);
				String foreignColumn = rs.getString(8);
				foreignKeys.add(new ForeignKey(table, column, foreignTable,foreignColumn, false));
			} 
		} catch (SQLException e) {
			System.out.println(e);
		}

		DBMetaData.setForeignKeys(table, foreignKeys);
	}
	
	private void setColumnsNames(int table) {
		ArrayList<String> columnNames = new ArrayList<String>();
		try {
			ResultSet result = metaData.getColumns(catalog, schemaPattern, DBMetaData.getTable(table), null); 
			while(result.next())
				columnNames.add(result.getString(4));
		} catch (SQLException e) { 
			System.out.println(e);
		}
		
		DBMetaData.setColumnsNames(table, columnNames);
	}
	
	private void setTablesNames() {
		// altrimenti li carica dal DB
		ArrayList<String> tableNames = new ArrayList<String>();
		String[] types = {"TABLE"};
		try {
			ResultSet result = metaData.getTables(catalog, schemaPattern, null, types);
			while(result.next())
				tableNames.add(result.getString(3));
		} catch (SQLException e) {
			System.out.println(e);
		}

		DBMetaData.setTablesNames(tableNames);
	}
	
	/**
	 * Esegue la query specificata sulla tabella specificata
	 * @param table: la tabella su cui eseguire la query
	 * @param sqlQuery: la query da eseguire
	 * @return le tuple risultanti dalla query
	 */
	public ArrayList<DBTupla> executeQuery(int table, String sqlQuery) {
		ArrayList<DBTupla> tuplas = new ArrayList<DBTupla>();
		
		// recupera i metadati associati alla tabella
		String[] columnNames = DBMetaData.getColumns(table);
		
		// effettua la query
		try {
			ResultSet result = conn[0].createStatement().executeQuery(sqlQuery);
			
			// trasforma i risultati in DBTupla
			while(result.next()) {
				Object[] values = new Object[columnNames.length];
				for(int i = 0; i < columnNames.length; i++)
					values[i] = result.getObject(columnNames[i]);
				DBTupla tupla = new DBTupla(table,values);
				tuplas.add(tupla);
			}
		} catch (SQLException e) { 
			System.err.println(e + " " + sqlQuery);
		}
		return tuplas;
	}
	
	
	/**
	 * Restituisce tutti i record della tabella che hanno un valore 
	 * specifico nella colonna specificata
	 * @param tableName: tabella su cui eseguire la query
	 * @param columnName: colonna da valutare nella WHERE
	 * @param value: valore che la tupla deve avere sulla colonna columnName; se � null
	 * 				 il metodo restituisce sempre una lista vuota senza effettuare la query
	 * @return le DBTupla risultato della query
	 */
	public ArrayList<DBTupla> getTuplaByValue(int table, int column, Object value) {
		if(value == null)
			return new ArrayList<DBTupla>(0);
		return executeQuery(table, queryValue(table,column,value));
	}
	
	private String queryValue(int table, int column, Object value) {
		String sqlQuery = "SELECT * FROM " + DBMetaData.getTable(table) + " WHERE " + DBMetaData.getColumn(table, column);
		if(value instanceof String)
			sqlQuery += " LIKE '%" + value + "%';";
		else
			sqlQuery += " = " + value.toString() + ";";
		return sqlQuery;
	}

	/**
	 * Restituisce tutta la tabella
	 * @param table
	 * @return le DBTupla risultato della query
	 */
	private String queryWholeTable(int table, ArrayList<String> words) {
		String tableName = DBMetaData.getTables()[table];
		String[] columns = DBMetaData.getColumns(table);
		int[] types = DBMetaData.getTypes(table);
		
		ArrayList<String> caseWhen = new ArrayList<String>();
		ArrayList<String> addedCols = new ArrayList<String>();
		for(int i = 0; i < words.size(); i++) {
			String word = words.get(i);
			if(word.equals(tableName))
				caseWhen.add("CAST(1 AS INT) AS tablename_" + tableName);
			else { 
				for(int j = 0; j < columns.length; j++) {
					if(types[j] != Types.INTEGER) {
						String caseW = "";
						String addedCol = "col_" + i + "_" + j;
						String like  = " Case When LOWER(" + columns[j] + ") LIKE '%" + word + "%'";
						//String regex = " " + columns[j] + " ~* '(\\s|-)" + word + "(\\s|,|-)'";
						caseW += like + " Then 1 Else 0 End AS " + addedCol;
						addedCols.add(addedCol);
						caseWhen.add(caseW);
					}
				}
			}
		}
		
		String sqlQuery = "SELECT * ";
		for(String caseW:caseWhen)
			sqlQuery += ", " + caseW; 
		sqlQuery += " FROM " + DBMetaData.getTable(table) + ";";
		
		System.out.println("Query: " + sqlQuery);
		
		return sqlQuery;
	}
	
	
	/**
	 * fa sia queryAllColumns che querySingleColumn
	 */
	private String querySpecial(int table, ArrayList<String> words, boolean[] matchingColumns) {
		
		String[] columns = DBMetaData.getColumns(table);
		int[] types = DBMetaData.getTypes(table);
		
		String sqlQuery = "SELECT * FROM ( SELECT *";

		ArrayList<String> caseWhen = new ArrayList<String>();
		ArrayList<String> addedCols = new ArrayList<String>();
		String thenElse = " Then 1 Else 0 End AS ";
		for(int i = 0; i < words.size(); i++) {
			String word = words.get(i);
			Integer number = null;
			try {
				number = Integer.parseInt(word);
			} catch(NumberFormatException e) {	}
			if(number == null) {
				for(int j = 0; j < columns.length; j++) {
					if(matchingColumns[j]) {
						String addedCol = "col_" + i + "_" + j;
						String isNotNull = " Case When " + columns[j] + " IS NOT NULL";
						caseWhen.add(isNotNull + thenElse + addedCol);
					}
					else if(types[j] != Types.INTEGER) {
						String addedCol = "col_" + i + "_" + j;
						String like  = " Case When LOWER(" + columns[j] + ") LIKE '%" + word + "%'";
						//String regex = " " + columns[j] + " ~* '(\\s|-)" + word + "(\\s|,|-)'";
						addedCols.add(addedCol);
						caseWhen.add(like + thenElse + addedCol);
					}
				}
			}
			else {
				for(int j = 0; j < columns.length; j++) {
					if(types[j] == Types.INTEGER) {
						String addedCol = "col_" + i + "_" + j;
						String like  = " Case When " + columns[j] + " = " + number;
						//String regex = " " + columns[j] + " ~* '(\\s|-)" + word + "(\\s|,|-)'";
						addedCols.add(addedCol);
						caseWhen.add(like + thenElse + addedCol);
					}
					else {
						String addedCol = "col_" + i + "_" + j;
						String like  = " Case When " + columns[j] + " LIKE '%" + word + "%'";
						//String regex = " " + columns[j] + " ~* '(\\s|-)" + word + "(\\s|,|-)'";
						addedCols.add(addedCol);
						caseWhen.add(like + thenElse + addedCol);
					}
				}
			}
		}
		
		for(String caseW : caseWhen) {
			sqlQuery += " , " + caseW;
		}
		
		sqlQuery += " FROM " + DBMetaData.getTable(table) + ") AS T WHERE";
		boolean first = true;
		for(String addedCol:addedCols) {
			if(first) {
				sqlQuery += " " + addedCol + " > 0";
				first = false;
			}
			else {
				sqlQuery += " OR " + addedCol + " > 0";
			}
		}
		sqlQuery += ";";
		
		System.out.println("Query: " + sqlQuery);
		
		return sqlQuery;
	}
	
		
	/**
	 * Cerca nel DB tutte le occorrenze di keyWord. Restituisce tutte le tuple
	 * che hanno nome di tabella, nome di colonna o valore di attributo uguali
	 * a keyWord. Case insensitive
	 * @param keyWord: la stringa da cercare
	 * @return la lista delle tuple che soddisfano i criteri di ricerca
	 * @throws InterruptedException 
	 */
	
	public ArrayList<DBTupla> search(ArrayList<String> keyWords) {
		
		ArrayList<String> processedInput = new ArrayList<String>(keyWords.size());
		for(String keyWord:keyWords) {
			processedInput.add(keyWord.replace("'", "''"));
		}		
		
		ArrayList<DBTupla> tuple = new ArrayList<DBTupla>();
		ConcurrentLinkedQueue<Query> queries = new ConcurrentLinkedQueue<Query>();
		
		String[] tables = DBMetaData.getTables();
		for(int table = 0; table < tables.length; table++) {
			
			//se il nome della tabella ha un match, tira su tutta la tabella
			
			if(keyWords.indexOf(tables[table]) >= 0)
				queries.add(new Query(table, queryWholeTable(table, processedInput),keyWords,QueryType.queryWholeTable));
			else {
				String[] columns = DBMetaData.getColumns(table);
				boolean[] matchingColumns = new boolean[columns.length];
				for(int i = 0; i < columns.length; i++) {
					if(keyWords.contains(columns[i]))
						matchingColumns[i] = true;
					else
						matchingColumns[i] = false;
				}
				//tira su tutte le tuple che hanno un valore che matcha le keyWord
				queries.add(new Query(table, querySpecial(table, processedInput, matchingColumns), keyWords,QueryType.querySpecial));
			}
		}
		
		

		
		ArrayList<DBTupla>[] resultSet = (ArrayList<DBTupla>[]) new ArrayList<?>[numberOfConnections];
		QueryThread[] threads = new QueryThread[numberOfConnections];
		for(int i = 0; i < numberOfConnections; i++) {
			resultSet[i] = new ArrayList<DBTupla>();
			threads[i] = new QueryThread(i, conn[i], queries, resultSet[i]);
			threads[i].start(keyWords);
		}
	
		synchronized(queries) {
			for(int i = 0; i < threads.length; i++) {
				try {
					queries.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		for(ArrayList<DBTupla> resSet:resultSet)
			tuple.addAll(resSet);
		
		return tuple;
	}
	
	public class Query {
		
		public int table;
		public String sqlQuery;
		public ArrayList<String> keyWords;
		public QueryType type;
		
		public Query(int table, String sqlQuery, ArrayList<String> keyWord, QueryType type) {
			this.table = table;
			this.sqlQuery = sqlQuery;
			this.keyWords = keyWord;
			this.type = type;
		}
		
		
	}
	
}


