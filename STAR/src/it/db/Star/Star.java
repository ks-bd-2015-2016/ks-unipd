package it.db.Star;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;

public class Star {
	private DBTuplaGraphSet treeSet;
	private ArrayList<LoosePath> optimized;
	private DBTuplaGraphExt tree;


	public Star(DBTuplaGraphSet t) {
		treeSet = t;
		tree = t.iterator().next();
		optimized = new ArrayList<LoosePath>();
	}
	
	/**
	 * Metodo che returna i fixed nodes
	 * @return ArrayList dei fixed nodes
	 */			
	private ArrayList<DBTuplaVertex> getFixed() {
		ArrayList<DBTuplaVertex> vertexes = tree.listVertexes();
		ArrayList<DBTuplaVertex> fixed = new ArrayList<DBTuplaVertex>();
		for (DBTuplaVertex tupla: vertexes){
			if (isTerminal(tupla) || isSteiner(tupla))	
				fixed.add(tupla);
		}
		return fixed;
	}
	
	private boolean isSteiner(DBTuplaVertex vertex) {
		if (vertex.numOfEdges() >= 3)
			return true;
		return false;
	}

	private boolean isTerminal (DBTuplaVertex vertex) {
		return !vertex.tupla.keyWords.isEmpty();
	}
	
	public ArrayList<ForeignKeyEdge> getEdges() {
		return tree.listEdges();
	}
	
	private boolean alreadyFound(LoosePath l, PriorityQueue<LoosePath> q) {
		Iterator<LoosePath> iter = q.iterator();
		DBTuplaVertex t1 = l.start;
		DBTuplaVertex t2 = l.end;
		LoosePath curr = null;
		while (iter.hasNext()) {
			curr = iter.next();
			if (curr.contains(t1, t2))
				return true;
		}
		return false;
	}

	/**
	 * metodo che controlla se il vertice steiner compare gi� in un qualche loosepath della pq paths
	 * @param steiner
	 * @return
	 */
	public boolean alreadyAdded (PriorityQueue<LoosePath> paths, DBTuplaVertex steiner) {
		for (LoosePath l: paths)  {
			if (l.start.equals(steiner) || l.end.equals(steiner))
				return true;
		}
		return false;
	}

	 /**
	 * Metodo che returna i loose paths, ossia tutti i cammini tra 
	 * un nodo terminale e uno di steiner, oppure tra due steiner nodes
	 * @return PrQueue di LoosePath
	 */
	/*public PriorityQueue<LoosePath> getLoosePaths() { 
		PriorityQueue<LoosePath> loosePaths = new PriorityQueue<LoosePath>();
		ArrayList<DBTuplaVertex> steiners = new ArrayList<DBTuplaVertex>();		//steiner nodes	(deg >= 3)
		ArrayList<DBTuplaVertex> fixed = getFixed();
		for (int i = 0; i<fixed.size(); i++) {	//popolo la arraylist di steiners 
			DBTuplaVertex tupla = fixed.get(i);
			if (isSteiner(tupla))
				steiners.add(tupla);
			DBTuplaVertex vertex = fixed.get(i);
			if (isSteiner(vertex)) 
				steiners.add(vertex);
		}
		System.out.println("Steiner nodes found : " + steiners.size());
		
		ArrayList<DBTuplaVertex> nodes = new ArrayList<DBTuplaVertex>();
		long weight = 0;
		
		for (DBTuplaVertex steiner:steiners) {		//per ogni SteinerNode...
			DBTuplaVertex start = steiner;
			System.out.println("Steiner node : " + start.toString());
			System.out.println("i can follow " + start.numOfEdges() + " edges");	
			for (ForeignKeyEdge edge:start.edges) {  //per ogni ramo dello steiner node scelto...
				DBTuplaVertex loopVar = start;
				ForeignKeyEdge currentEdge = edge;
				//System.out.println("Following edge: " + currentEdge.toString());
				while (!(loopVar == null)) {				//inizio a percorrere il ramo
					DBTuplaVertex next = tree.otherSide(loopVar, currentEdge);					
					System.out.println("Starting from : " + loopVar.toString());
					if (isSteiner(next) || isTerminal(next)) {	//se il nodo sucessivo � un altro steiner node, ho trovato un loosepath
			int startindex = steiners.indexOf(start);
			System.out.println("i can follow " + start.numOfEdges() + " edges");
			for (ForeignKeyEdge edge:start.edges) {  //per ogni ramo dello steiner node scelto...
				Stack<DBTuplaVertex> stack = new Stack<DBTuplaVertex>();
				stack.push(start);
				
				System.out.println("Stack size: " + stack.size());
				ForeignKeyEdge currentEdge = edge;
				System.out.println("Following edge: " + currentEdge.toString());
				while (!stack.isEmpty()) {				//inizio a percorrere il ramo
					DBTuplaVertex current = stack.pop();
					DBTuplaVertex next = edge.otherSide(current);
					System.out.println("Starting from : " + current.toString());
					
					if (isTerminal(next)) {						//se il nodo successivo � un terminal, ho trovato un loosepath
						System.out.println("Arrived to a terminal: "+ next.toString());
						weight++;
						DBTuplaVertex end = next;
						//nodes.add(end);
						
						if (nodes.size()>1) {			//se ha almeno un nodo
							System.out.println("Creating loosePath");
							LoosePath l = new LoosePath(start, end, weight, nodes);			//lo aggiungo alla priorityQ
							loosePaths.add(l);
						}
						System.out.println("Reset parameters for next iteration");
						weight = 0;		//**		resetto i parametri per la prossima iterazione
						nodes.clear();	//**
						
					}
					
					else if (isSteiner(next)) {	//se il nodo sucessivo � un altro steiner node, ho trovato un loosepath
						System.out.println("Arrived to a steiner: "+ next.toString());
						weight++;
						DBTupla end = next;
						if (nodes.size()>1 && !isOptimized(start, end)) {	//se il loosepath non � gi� ottimo e c� almeno un nodo...
						DBTuplaVertex end = next;
						
						if (steiners.indexOf(next)>startindex && nodes.size()>1) {	//se il loosepath non � gi� stato inserito nella priorityQ e c� almeno un nodo...
							System.out.println("Creating loosePath");
							LoosePath l = new LoosePath(start, end, weight, nodes);			
							if (!alreadyFound(l, loosePaths)) {			//se il LoosePath non � gi� stato inserito nella priorityQ
								loosePaths.add(l);					//lo aggiungo alla priorityQ
								System.out.println("loosepath found");
							}
						}
						weight = 0;			//**	resetto i parametri per la prossima iterazione
						nodes.clear();		//**
						loopVar = null;
					}
					
					else {					//in questo caso sono in un nodo "di passaggio" oppure in una foglia inutile
						if (next.numOfEdges()==1) {	//se next � una foglia inutile (non terminal)
							System.out.println("Arrived to a useless leave: " + next.toString());
							tree.remove(next);	
							for (DBTupla t:nodes)
								tree.remove(t);
							weight = 0;			//**	resetto i parametri per la prossima iterazione
							nodes.clear();		//**
							loopVar = null;
						}
						else {
							System.out.println("Arrived to a passage node: " + next.toString());
							weight++;
							nodes.add(next);
							loopVar = next;
							System.out.println("continuing on this branch...");
							for (ForeignKeyEdge thisEdge:tree.edgesOf(next)) {		//seleziono il ramo successivo
							stack.push(next);
							
							for (ForeignKeyEdge thisEdge:next.edges) {		//seleziono il ramo successivo
								if (thisEdge!=currentEdge) {
									currentEdge = thisEdge;
									break;
								}
							}
						}
					}
				}
			}
			//break;
		}
		return loosePaths;
	}*/
	
	/**
	 * returna i loosepaths, dovrebbe essere pi� efficente del precedente metodo
	 * @return
	 */

	public PriorityQueue<LoosePath> getLoosePaths() { 
		PathComparator comp = new PathComparator();
		PriorityQueue<LoosePath> loosePaths = new PriorityQueue<>(1, comp);
		LinkedList<DBTuplaVertex> fixedNodes = new LinkedList<DBTuplaVertex>();
		fixedNodes.addAll(tree.getTerminals());
		System.out.println("");
		System.out.println("Starter nodes");
		for (DBTuplaVertex starter : fixedNodes)
			System.out.println(starter);
		System.out.println("");
		
		while (!fixedNodes.isEmpty()) {
			DBTuplaVertex start = fixedNodes.pop();
			System.out.println("starting from " + start.toString());
			
			for (ForeignKeyEdge e: start.edges) {  //per ogni ramo dello steiner node scelto...
				ForeignKeyEdge edge = e;
				DBTuplaVertex loopVar = start;
				long weight = 0;
				ArrayList<DBTuplaVertex> nodes = new ArrayList<DBTuplaVertex>();

				while (loopVar != null) {				//inizio a percorrere il ramo
					DBTuplaVertex next = edge.otherSide(loopVar);
					if (isTerminal(next)) {
						System.out.println("Arrived to a terminal : " + next);
						weight++;
						DBTuplaVertex end = next;
						if (weight>1 && !isOptimized(start, end)) {	//se il loosepath non � gi� ottimo e c� almeno un nodo...
							System.out.println("Creating loosePath");
							LoosePath l = new LoosePath(start, end, weight, nodes);			
							if (!alreadyFound(l, loosePaths)) {				//se il loosepath non � gi� stato inserito nella priorityQ
								loosePaths.add(l);							//lo aggiungo alla priorityQ
								System.out.println("loosepath found");	
							}
						}
						weight = 0;			//**	resetto i parametri per la prossima iterazione
						loopVar = null;
						fixedNodes.remove(next);
					}
					else if(isSteiner(next)) {  //se il nodo sucessivo � un altro steiner node, ho trovato un loosepath
						System.out.println("Arrived to a steiner: " + next);
						weight++;  /*
						if (alreadyAdded(loosePaths, next)) {		//aggiungo lo steiner node alla lista se non lo ho gi� aggiunto in precedenza
							fixedNodes.add(next);
							System.out.println("Adding steiner to fixedNodes : " + next);
						}  */
						if (weight>1 && !isOptimized(start, next)) {	//se il loosepath non � gi� ottimo e c� almeno un nodo...
							System.out.println("Creating loosePath");		
							LoosePath l = new LoosePath(start, next, weight, nodes);			
							if (!alreadyFound(l, loosePaths)) {				//se il loosepath non � gi� stato inserito nella priorityQ
								loosePaths.add(l);							//lo aggiungo alla priorityQ
								System.out.println("loosepath found");
							}
						}
						weight = 0;			//**	resetto i parametri per la prossima iterazione
						loopVar = null;
					}

					else {					//in questo caso sono in un nodo "di passaggio" oppure in una foglia inutile
						System.out.println("Arrived to a passage node: " + next.toString());
						weight++;
						nodes.add(next);
						System.out.println("nodes size : " + nodes.size());
						loopVar = next;
						System.out.println("continuing on this branch...");
						for (ForeignKeyEdge thisEdge:next.edges) {		//seleziono il ramo successivo
							if (thisEdge!=edge) {
								edge = thisEdge;
								break;
							}
						}
					}
				}
				System.out.println("");
			}
		}
		return loosePaths;
	}
	
	public LoosePath altFindShortestPath (LoosePath l) {
		//System.out.println("Set pre split " + treeSet);
		ArrayList<DBTuplaGraphExt> graphs = treeSet.split(treeSet.iterator().next(), l);
		//System.out.println("Set post split " + treeSet);
		DBTuplaGraphExt g1 = graphs.get(0);
		DBTuplaGraphExt g2 = graphs.get(1);
		ArrayList<DBTuplaVertex> startGraph1 = new ArrayList<DBTuplaVertex>(g1.vertexes.size());
		ArrayList<DBTuplaVertex> startGraph2 = new ArrayList<DBTuplaVertex>(g2.vertexes.size());
		startGraph1.addAll(g1.vertexes);
		startGraph2.addAll(g2.vertexes);
		g1.front.addAll(g1.vertexes);
		g2.front.addAll(g2.vertexes);
		DBTuplaGraphExt tree = treeSet.recursiveBFS();
		/*System.out.println("Set post bfs " + treeSet);
		System.out.println(tree); */
		/*System.out.println("Set port retain " + treeSet);
		System.out.println(treeSet.iterator().next());*/
		ForeignKeyEdge mergepoint = tree.mergePoints.iterator().next();
		DBTuplaVertex[] vertexes = {mergepoint.vertex1, mergepoint.vertex2};
		ArrayList<DBTuplaVertex> nodes = new ArrayList<DBTuplaVertex>();
		DBTuplaVertex[] startEnd = new DBTuplaVertex[2];
		for(int i = 0; i < 2; i++) {
			DBTuplaVertex vertex = vertexes[i];
			ForeignKeyEdge parentEdge = vertex.parentEdge;
			while(!startGraph1.contains(vertex) && !startGraph2.contains(vertex)) {
				nodes.add(vertex);
				vertex = parentEdge.otherSide(vertex);
				parentEdge = vertex.parentEdge;
			}
			startEnd[i] = vertex;
		}
		LoosePath newL = new LoosePath(startEnd[0], startEnd[1], nodes.size() +1, nodes);
		treeSet.getSteinerTree(tree);
		return newL;
	}
	
	public void improveTree() {
		PriorityQueue<LoosePath> q = getLoosePaths();
		while (!q.isEmpty()) {		//TODO fare il compator
			LoosePath l = q.poll();
			System.out.println("Selected loosepath: " + l.toString());
			LoosePath newL = altFindShortestPath(l);
			if (newL.weight < l.weight) {
				optimum(newL);
				System.out.println("Successfully replaced");
				q = getLoosePaths();
			}
			else optimum(l);
		}
	}

	
	/**
	 * metodo che salva un LoosePath nell'ArrayList contenente i LoosePath
	 * ottimizzati, in modo che alle prossime iterazioni non venga riconsiderato tra
	 * quelli da ottimizzare 
	 * @param l
	 */
	private void optimum(LoosePath l) {
		optimized.add(l);
	}
	
	/**
	 * metodo che verifica se tra i due nodi t1 e t2 esiste gi� un LoosePath
	 * ottimo
	 * @param t1
	 * @param t2
	 * @return
	 */
	private boolean isOptimized (DBTuplaVertex t1, DBTuplaVertex t2) {
		for(LoosePath l: optimized) {
			if (l.end.equals(t1) && l.start.equals(t2) || l.end.equals(t2) && l.start.equals(t1))
				return true;
		}
		return false;
	}

	/**
	 * metodo che sostituisce un loosepath
	 * @param l loosepath ottimo, unico parametro perch� in realt� il 
	 * 			loosepath originario � gi� stato rimosso nel metodo findShortestPath
	 */
	public void replace(LoosePath l) {	
		DBTuplaVertex currentTupla = l.start;
		ForeignKey f;
		for (DBTuplaVertex tupla: l.nodes) {	//TODO controllare ordine
			tree.addVertex(tupla);
			f = getFKey(currentTupla, tupla);		//TODO qui c'e' un problema, esiste una ForeignKey tra start e il nodo successivo anche se non esiste pi� l'albero nel quale esistevano?
			tree.addEdge(currentTupla, tupla, f);
			currentTupla = tupla;
		}
		f = getFKey(currentTupla, l.end);
		tree.addEdge(currentTupla, l.end, f);
	}
	
	private ForeignKey getFKey(DBTuplaVertex t1, DBTuplaVertex t2) {
		int tab1 = t1.tupla.table;
		int tab2 = t2.tupla.table;
		ForeignKey[] f1 = DBMetaData.getForeignKeys(tab1);
		ForeignKey[] f2 = DBMetaData.getForeignKeys(tab2);
		for (ForeignKey i : f1) {
			for (ForeignKey k : f2) {
				if (k==i)		//TODO ???
					return k;
			}
		}
		return null;
	}
	
	public String toString() {
		return this.tree.toString();
	}
	
	
	class Comparator1 implements Comparator<DBTuplaVertex> {	//TODO non sono sicuro del natural ordering
		public int compare (DBTuplaVertex t1, DBTuplaVertex t2) {
			if (t1.d1 > t2.d1)
				return -1;
			else if (t2.d1 < t1.d1)
				return 1;
			return 0;
		}
	}
	
	class Comparator2 implements Comparator<DBTuplaVertex> {	//TODO non sono sicuro del natural ordering
		public int compare (DBTuplaVertex t1, DBTuplaVertex t2) {
			if (t1.d2 > t2.d2)
				return -1;
			else if (t2.d2 < t1.d2)
				return 1;
			return 0;
		}
	}
	
	class PathComparator implements Comparator<LoosePath> {
		public int compare (LoosePath l1, LoosePath l2) {
			if (l1.weight < l2.weight)
				return -1;
			else return 1;
		}
	}
	
}
