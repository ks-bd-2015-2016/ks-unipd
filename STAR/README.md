## INSTRUCTIONS

STAR algorithm implementation is provided as an Eclipse Java 8 project.
To run it, import the project in Eclipse and check that postgresql-9.4.1208.jre6.jar
is in the list of the referenced libraries.


### DEVELOPERS
Alberto Cenzato alberto.cenzato@studenti.unipd.it
Giovanni Cogato giovanni.cogato.1@studenti.unipd.it