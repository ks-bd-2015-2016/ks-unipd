/**
 * Keyword Search Project - Bidirectional search (BANKS-II)
 * @author Giovanni Bonato, Alberto Minetto, Leonardo Pellegrina
 * @version 1.0.0
 Giovanni Bonato giovanni.bonato@studenti.unipd.it
 Alberto Minetto alberto.minetto@studenti.unipd.it
 Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 */

public class Table{

	public String table_name;

	public String[] columns_name;
	public int[] columns_type;
	public boolean[] compatible_type;
	public String[] primary_key_columns;
	public String[] foreign_key_columns;
	public String[] private_key_tables;
	public String[] private_key_columns;

	public Table(String name){

		this.table_name = name;

	}

	/**
     *
     * @return The hash code of this Table's name
     */
    public int hashCode(){
        return this.table_name.hashCode();
    }

    /**
     *
     * @param other The object to compare
     * @return true if other instanceof Vertex and the two Vertex objects have the same label
     */
    public boolean equals(Object other){
        if(!(other instanceof Table)){
            return false;
        }

        Table v = (Table)other;
        return this.table_name.equals(v.table_name);
    }

}
