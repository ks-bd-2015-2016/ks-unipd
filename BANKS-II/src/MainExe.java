import java.sql.*;
import java.util.Scanner;

/**
 * Main class for Keyword Search Project - Bidirectional search (BANKS-II)
 * @author Giovanni Bonato, Alberto Minetto, Leonardo Pellegrina
 Giovanni Bonato giovanni.bonato@studenti.unipd.it
 Alberto Minetto alberto.minetto@studenti.unipd.it
 Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 * @version 1.0.0
 */

public class MainExe {
	// access to db
	private static final String DATABASE = "jdbc:postgresql://localhost/imdb";
	private static final String USER ="keywordsearch";
	private static final String PASSWORD = "keywordsearch";

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner sc2 = new Scanner(System.in);
		System.out.println("Use an undirected graph? y/n");

		String input = sc2.nextLine();
		boolean unconnected = input.equals("y");

		keyWaitText("Press enter to start creating the graph");

		// keywork search object
		KeywordSearchBS ksbs = new KeywordSearchBS(USER , PASSWORD , DATABASE , unconnected);

		while(true)  {

			System.out.println("New Single Search-----press CTRL-C to stop ");

			String[] keywords = getKeys();

			ksbs.startSearch(keywords , unconnected);

			System.out.println("\nEnd of Main!");

		}

	}

	private static String[] getKeys(){

		Scanner sc = new Scanner(System.in);
		System.out.println("type the number of keywords");
		int dimension = sc.nextInt();
		String[] keywords = new String[dimension];
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Insert the keywords, one for each line");
		int index = 0;

		while(index < dimension) {

			String input = sc1.nextLine();
			if (input.equals("f"))   {break;}
			keywords[index] = input;
			System.out.println("added keyword " + input);
			index++;



		}

		return keywords;

	}

	private static void keyWaitText(String message){

		Scanner sc = new Scanner(System.in);

		System.out.println(message);

		sc.nextLine();

	}

}
