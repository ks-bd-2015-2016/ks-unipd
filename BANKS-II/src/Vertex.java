import java.util.ArrayList;

/**
 * This class models a vertex in a graph. For ease of
 * the reader, a label for this vertex is required.
 * Note that the Graph object only accepts one Vertex per label,
 * so uniqueness of labels is important. This vertex's neighborhood
 * is described by the Edges incident to it.
 *
 * @author originally by Michael Levet. edited to support directed edges
 Giovanni Bonato giovanni.bonato@studenti.unipd.it
 Alberto Minetto alberto.minetto@studenti.unipd.it
 Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 * @date August, 2016
 */
public class Vertex {

    private ArrayList<Edge> neighborhood_in;
    private ArrayList<Edge> neighborhood_out;
    private String label;
    private String tableName;
    private double activation;
    private ArrayList<Double> k_activation;
    private String hashstring;
    /**
     *
     * @param label The unique label associated with this Vertex
     */
    public Vertex(String label , String tableName){
        this.label = label;
        this.tableName = tableName;
        this.neighborhood_in = new ArrayList<Edge>();
        this.neighborhood_out = new ArrayList<Edge>();
        this.hashstring = tableName + "_" +  label;
    }

     /**
     * This method adds an Edge to the incidence neighborhood of this graph iff
     * the edge is not already present.
     *
     * @param edge The edge to add
     */
    public void addNeighbor(Edge edge , boolean in){

        if(in){
            if(this.neighborhood_in.contains(edge)){
                return;
            }

            this.neighborhood_in.add(edge);
        }
        else{
            if(this.neighborhood_out.contains(edge)){
                return;
            }

            this.neighborhood_out.add(edge);
        }
    }


    /**
     *
     * @param other The edge for which to search
     * @return true iff other is contained in this.neighborhood
     */
    public boolean containsNeighbor(Edge other , boolean in){
        if(in)
            return this.neighborhood_in.contains(other);
        return this.neighborhood_out.contains(other);
    }

    /**
     *
     * @param index The index of the Edge to retrieve
     * @return Edge The Edge at the specified index in this.neighborhood
     */
    public Edge getNeighbor(int index , boolean in){
        if(in)
            return this.neighborhood_in.get(index);
        return this.neighborhood_out.get(index);
    }


    /**
     *
     * @param index The index of the edge to remove from this.neighborhood
     * @return Edge The removed Edge
     */
    Edge removeNeighbor(int index , boolean in){
        if(in)
            return this.neighborhood_in.remove(index);
        return this.neighborhood_out.remove(index);
    }

    /**
     *
     * @param e The Edge to remove from this.neighborhood
     */
    public void removeNeighbor(Edge e){
        this.neighborhood_in.remove(e);
        this.neighborhood_out.remove(e);
    }


    /**
     *
     * @return int The number of neighbors of this Vertex
     */
    public int getNeighborCount(boolean in){
        if(in)
            return this.neighborhood_in.size();
        else
            return this.neighborhood_out.size();
    }


    /**
     *
     * @return String The label of this Vertex
     */
    public String getLabel(){
        return this.label;
    }


    /**
     *
     * @return String A String representation of this Vertex
     */
    public String toString(){
        return "Vertex " + label;
    }

    /**
     *
     * @return The hash code of this Vertex's label
     */
    public int hashCode(){
        return this.hashstring.hashCode();
    }

    /**
     *
     * @param other The object to compare
     * @return true iff other instanceof Vertex and the two Vertex objects have the same label
     */
    public boolean equals(Object other){
        if(!(other instanceof Vertex)){
            return false;
        }

        Vertex v = (Vertex)other;
        return this.hashstring.equals(v.getKey());
    }

    /**
     *
     * @return ArrayList<Edge> A copy of this.neighborhood. Modifying the returned
     * ArrayList will not affect the neighborhood of this Vertex
     */
    public ArrayList<Edge> getNeighbors(boolean in){
        if(in)
            return new ArrayList<Edge>(this.neighborhood_in);
        return new ArrayList<Edge>(this.neighborhood_out);
    }

    public void setKeywordNumber(int k){
        k_activation = new ArrayList<Double>(k);
        for(int i = 0; i < k; i++){
            k_activation.add(0.0);
        }
    }

    public void setActivation(int i , double act){
        activation -= k_activation.get(i);
        k_activation.set(i, act);
        activation += act;
    }

    public void scaleActivation(double coeff){
        activation = 0;
        for(int i = 0; i < k_activation.size(); i++){
            k_activation.set(i, k_activation.get(i) * coeff);
            activation += k_activation.get(i);
        }
    }

    public double getActivation(){
        return activation;
    }

    public double getActivation(int i){
        return k_activation.get(i);
    }

	public Vertex clone(){

		Vertex new_v = new Vertex(this.label, this.tableName);
		return new_v;
	}

    public String getKey(){
        return hashstring;
    }

    public String getTableName(){
        return tableName;
    }
}
