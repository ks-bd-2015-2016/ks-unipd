import java.util.*;

/**
 * This class models a simple, undirected graph using an
 * incidence list representation. Vertices are identified
 * uniquely by their labels, and only unique vertices are allowed.
 * At most one Edge per vertex pair is allowed in this Graph.
 *
 * Note that the Graph is designed to manage the Edges. You
 * should not attempt to manually add Edges yourself.
 *
 * @author originally by Michael Levet. edited to support weighted directed edges
  Giovanni Bonato giovanni.bonato@studenti.unipd.it
  Alberto Minetto alberto.minetto@studenti.unipd.it
  Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 * @date August, 2016
 */
public class Graph {

    private HashMap<String, HashMap<String, Vertex>> vertices;
    private HashMap<Integer, Edge> edges;
    private Integer score;
    private Vertex root;

    public Graph(){
        this.vertices = new HashMap<String, HashMap<String, Vertex>>();
        this.edges = new HashMap<Integer, Edge>();
        this.score = -1;
    }

    /**
     * This constructor accepts an ArrayList<Vertex> and populates
     * this.vertices. If multiple Vertex objects have the same label,
     * then the last Vertex with the given label is used.
     *
     * @param vertices The initial Vertices to populate this Graph
     */
    /*public Graph(ArrayList<Vertex> vertices){
        this.vertices = new HashMap<String, Vertex>();
        this.edges = new HashMap<Integer, Edge>();

        for(Vertex v: vertices){
            this.vertices.put(v.getLabel(), v);
        }

    }*/

    /**
     * This method adds am edge between Vertices one and two
     * of weight 1, if no Edge between these Vertices already
     * exists in the Graph.
     *
     * @param one The first vertex to add
     * @param two The second vertex to add
     * @return true iff no Edge relating one and two exists in the Graph
     */
    public boolean addEdge(Vertex one, Vertex two){
        return addEdge(one, two, 1);
    }


    /**
     * Accepts two vertices and a weight, and adds the edge
     * ({one, two}, weight) iff no Edge relating one and two
     * exists in the Graph.
     *
     * @param one The first Vertex of the Edge
     * @param two The second Vertex of the Edge
     * @param weight The weight of the Edge
     * @return true iff no Edge already exists in the Graph
     */
    public boolean addEdge(Vertex one, Vertex two, int weight){
        if(one.equals(two)){
            System.out.println("failed 1");
            return false;
        }

        //ensures the Edge is not in the Graph
        Edge e = new Edge(one, two, weight);
        Edge e_ = new Edge(two, one, weight);
        if(edges.containsKey(e.hashCode()) && edges.containsKey(e_.hashCode())){
            Edge conflict = edges.get(e.hashCode());
            //if(one.getKey().equals("country_UZB") || two.getKey().equals("country_UZB"))
            //    System.out.println("fallito qui!");
            //System.out.println("conflict: one: " + conflict.getOne().getKey() + " two: " + conflict.getTwo().getKey());
            //System.out.println("new: one: " + e.getOne().getKey() + " two: " + e.getTwo().getKey());
            //System.out.println("failed 2 : " + e.hashCode() + " " + conflict.hashCode());
            //System.exit(-1);
            return false;
        }
        //and that the Edge isn't already incident to one of the vertices
        else if(one.containsNeighbor(e , false) || two.containsNeighbor(e , true)){
            System.out.println("failed 3");
            return false;
        }

        if(!edges.containsKey(e.hashCode())){
            edges.put(e.hashCode(), e);
            one.addNeighbor(e , false);
            two.addNeighbor(e , true);
        }
        else{
            edges.put(e_.hashCode(), e_);
            one.addNeighbor(e_ , false);
            two.addNeighbor(e_ , true);
        }

        /*if((one.getKey().equals("country_UZB") || two.getKey().equals("country_UZB")) &&
            (one.getKey().equals("religion_Eastern Orthodox_UZB") || two.getKey().equals("religion_Eastern Orthodox_UZB")))
            System.out.println("--> Aggiunto edge!");*/

        return true;
    }

    /**
     *
     * @param e The Edge to look up
     * @return true iff this Graph contains the Edge e
     */
    public boolean containsEdge(Edge e){
        if(e.getOne() == null || e.getTwo() == null){
            return false;
        }

        return this.edges.containsKey(e.hashCode());
    }


    /**
     * This method removes the specified Edge from the Graph,
     * including as each vertex's incidence neighborhood.
     *
     * @param e The Edge to remove from the Graph
     * @return Edge The Edge removed from the Graph
     */
    public Edge removeEdge(Edge e){
       e.getOne().removeNeighbor(e);
       e.getTwo().removeNeighbor(e);
       return this.edges.remove(e.hashCode());
    }

    /**
     *
     * @param vertex The Vertex to look up
     * @return true iff this Graph contains vertex
     */
    public boolean containsVertex(Vertex vertex , String table_name){
        String v_name = vertex.getLabel();
        HashMap<String, Vertex> t_name = this.vertices.get(table_name);
        t_name.get(v_name);
        return this.vertices.get(table_name).get(vertex.getLabel()) != null;
    }

    /**
     *
     * @param label The specified Vertex label
     * @return Vertex The Vertex with the specified label
     */
    public Vertex getVertex(String label , String table_name){
        return vertices.get(table_name).get(label);
    }

    public void addTable(String table_name){
        HashMap<String, Vertex> nodes = new HashMap<String, Vertex>();
        vertices.put(table_name , nodes);
    }

    /**
     * This method adds a Vertex to the graph. If a Vertex with the same label
     * as the parameter exists in the Graph, the existing Vertex is overwritten
     * only if overwriteExisting is true. If the existing Vertex is overwritten,
     * the Edges incident to it are all removed from the Graph.
     *
     * @param vertex
     * @param overwriteExisting
     * @return true iff vertex was added to the Graph
     */
    public boolean addVertex(Vertex vertex, String table_name , boolean overwriteExisting){

        HashMap<String, Vertex> nodes = this.vertices.get(table_name);

        if(nodes != null){

            Vertex current = nodes.get(vertex.getLabel());

            if(current != null){
                if(!overwriteExisting){
                    return false;
                }

                while(current.getNeighborCount(true) > 0){
                    this.removeEdge(current.getNeighbor(0 , true));
                }
                while(current.getNeighborCount(false) > 0){
                    this.removeEdge(current.getNeighbor(0 , false));
                }
            }

        }
        else{
            nodes = new HashMap<String, Vertex>();
            vertices.put(table_name , nodes);
        }

        nodes.put(vertex.getLabel(), vertex);
        vertices.put(table_name , nodes);

        return true;
    }

    /**
     *
     * @param label The label of the Vertex to remove
     * @return Vertex The removed Vertex object
     */
    public Vertex removeVertex(String label , String table_name){
        Vertex v = vertices.get(table_name).remove(label);

        while(v.getNeighborCount(true) > 0){
            this.removeEdge(v.getNeighbor(0 , true));
        }

        while(v.getNeighborCount(false) > 0){
            this.removeEdge(v.getNeighbor(0 , false));
        }

        return v;
    }

    /**
     *
     * @return Set<String> The unique labels of the Graph's Vertex objects
     */
    public Set<String> vertexKeys(String table_name){
        return this.vertices.get(table_name).keySet();
    }

    public HashMap<String , Vertex> getVertexes(String table_name){
        return this.vertices.get(table_name);
    }

    /**
     *
     * @return Set<Edge> The Edges of this graph
     */
    public Set<Edge> getEdges(){
        return new HashSet<Edge>(this.edges.values());
    }

	public void setScore(Integer sc){

		this.score = sc;

	}

	public Integer getScore(){

		return this.score;

	}

    public void setRoot(Vertex new_root){

        this.root = new_root;

    }

    public Vertex getRoot(){

        return this.root;

    }

}
