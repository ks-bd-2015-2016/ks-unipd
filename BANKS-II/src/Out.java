/**
 * Keyword Search Project - Bidirectional search (BANKS-II)
 * @author Giovanni Bonato, Alberto Minetto, Leonardo Pellegrina
 * @version 1.0.0
 Giovanni Bonato giovanni.bonato@studenti.unipd.it
 Alberto Minetto alberto.minetto@studenti.unipd.it
 Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 */
 
class Out{

	public String out = "";
	public int score = 0;

	public Out(){}

	public int getScore(){
		return score;
	}

}
