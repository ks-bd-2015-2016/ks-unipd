import java.sql.*;
import java.util.*;
import java.lang.*;

/**
 * Keyword Search Project - Bidirectional search (BANKS-II)
 * @author Giovanni Bonato, Alberto Minetto, Leonardo Pellegrina
 * @version 1.0.0
 Giovanni Bonato giovanni.bonato@studenti.unipd.it
 Alberto Minetto alberto.minetto@studenti.unipd.it
 Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
 */
public class KeywordSearchBS {

	// db login informations
	private String database;
	private String user;
	private String password;
	final static String DRIVER = "org.postgresql.Driver";
	final static boolean DEBUG = false;
	private boolean unconnected = false;

	private HashMap<String, Table> tables;

	private Graph ks_graph;


	/**
	 * Constructor
	 */

	public KeywordSearchBS(String usr , String pwd , String addr , boolean unconnected_){

		this.database = addr;
		this.user = usr;
		this.password = pwd;

		this.unconnected = unconnected_;

		tables = new HashMap<String, Table>();

		System.out.println("KS object created with:");
		System.out.println("DB:" + this.database);
		System.out.println("User:" + this.user);

		this.init_db();

		this.populate_tables_info();
		this.print_tables_info();

		this.build_graph();

	}


	public void startSearch(String[] keys , boolean unconnected_){

		Long elapsed_[] = new Long[5];

		for(int i = 0; i < 5; i++){

			SingleSearch search = new SingleSearch(keys);
			long elapsed = search.startBSSearch();
			elapsed_[i] = elapsed;
			System.out.println("-----------> execution time = " + elapsed + "ms!");

		}
		for(int i = 0; i < 5; i++){
			System.out.println("-----------> execution time = " + elapsed_[i]);

		}

	}

	// TODO
	public void getResults(){



	}

	private void init_db(){

		try {
			Class.forName(DRIVER);
			System.out.printf("Driver %s successfully registered.%n", DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.printf("Driver %s not found: %s.%n", DRIVER, e.getMessage());
			System.exit(-1);
		}

	}



	// TODO
	private void build_graph(){

		ks_graph = new Graph();

		create_vertexes();

		connect_vertexes();

	}

	private void create_vertexes(){

		// Get a set of the entries
    	Set set = tables.entrySet();
    	// Get an iterator
    	Iterator it = set.iterator();

    	System.out.println("Creating vertexes...");

    	while(it.hasNext()) {

        	Map.Entry me = (Map.Entry)it.next();
        	Table table = (Table)me.getValue();

        	// query creation

        	String query = "SELECT ";

        	for(int i = 0; i < table.primary_key_columns.length; i++){

        		if(i > 0)
        			query = query + ",";

        		query = query + table.primary_key_columns[i];

        	}

        	query = query + " FROM " + table.table_name + ";";

        	ArrayList<String[]> rs = this.execute_query(query , table.primary_key_columns);

        	ks_graph.addTable(table.table_name);

        	// now get the primary keys of the node and creates the vertexes
        	for(int i = 0; i < rs.size(); i++){

        		// create index string
        		String v_key = "";
        		String[] row = rs.get(i);

        		for(int j = 0; j < table.primary_key_columns.length; j++){

        			if(j > 0)
        				v_key = v_key + "_";

        			v_key = v_key + "" + row[j];

        		}

        		Vertex new_vertex = new Vertex(v_key , table.table_name);
        		// for debug
        		if(DEBUG)
        		System.out.println("New vertex created with key " + v_key + " and table name " + table.table_name);

        		ks_graph.addVertex(new_vertex, table.table_name , true);


        	}

        }

	}

	private void connect_vertexes(){

		// Get a set of the entries
    	Set set = tables.entrySet();
    	// Get an iterator
    	Iterator it = set.iterator();

    	System.out.println("Connecting vertexes...");

    	while(it.hasNext()) {

    		Map.Entry me = (Map.Entry)it.next();
        	Table table = (Table)me.getValue();

        	int i = 0;
        	while(i < table.private_key_tables.length){
    		//for(int i = 0; i < table.private_key_tables.length; i++){

    			Table othertable = tables.get(table.private_key_tables[i]);

    			String query = "SELECT ";

	        	for(int j = 0; j < table.primary_key_columns.length; j++){

	        		if(j > 0)
	        			query = query + ",";

	        		query = query + "A." + table.primary_key_columns[j] + " AS A" + table.primary_key_columns[j];

	        	}

	        	for(int j = 0; j < othertable.primary_key_columns.length; j++){

	        		query = query + ",";

	        		query = query + "B." + othertable.primary_key_columns[j] + " AS B" + othertable.primary_key_columns[j];

	        	}

	        	query = query + " FROM " + table.table_name + " AS A INNER JOIN " +
	        		othertable.table_name + " AS B ON";

	        	boolean first = true;
	        	int key_count = 0;

	        	while(i < table.private_key_tables.length){

	        		Table new_othertable = tables.get(table.private_key_tables[i]);

	        		if(othertable.table_name != new_othertable.table_name || key_count >= othertable.primary_key_columns.length)
	        			break;

	        		if(!first)
	        			query = query + " AND ";

	        		query = query  + " A." + table.foreign_key_columns[i] + " = B." + table.private_key_columns[i];
	        		i++;
	        		key_count++;
	        		first = false;
	        	}
	        	query = query + ";";

	        	String[] query_columns = new String[table.primary_key_columns.length + othertable.primary_key_columns.length];

	        	// usign aliases to get correct results
	        	for(int j = 0; j < table.primary_key_columns.length; j++)
	        		query_columns[j] = "A"+table.primary_key_columns[j];
	        	for(int j = 0; j < othertable.primary_key_columns.length; j++)
	        		query_columns[j + table.primary_key_columns.length] = "B"+othertable.primary_key_columns[j];

	        	ArrayList<String[]> rs = this.execute_query(query , query_columns);

	        	// just for debug
	        	/*System.out.println(query);
	        	try{
	        	Thread.sleep(0);
	        	}
	        	catch(InterruptedException e){}*/

	        	for(int j = 0; j < rs.size(); j++){

	        		// get full primary key of table
	        		String table_key = "";
	        		String[] row = rs.get(j);

	        		for(int k = 0; k < table.primary_key_columns.length; k++){
	        			if(k > 0)
        					table_key = table_key + "_";
	        			table_key = table_key + row[k];
	        		}

	        		// get full primary key of othertable
	        		String othertable_key = "";

	        		for(int k = table.primary_key_columns.length; k < row.length; k++){
	        			if(k > table.primary_key_columns.length)
        					othertable_key = othertable_key + "_";
	        			othertable_key = othertable_key + row[k];
	        		}

	        		Vertex start_vertex = ks_graph.getVertex(table_key , table.table_name);
	        		Vertex end_vertex = ks_graph.getVertex(othertable_key , othertable.table_name);


	        		if(start_vertex != null && end_vertex != null){

	        			boolean success = ks_graph.addEdge(start_vertex, end_vertex);
	        			// connect in both directions
	        			if(unconnected)
	        				ks_graph.addEdge(end_vertex, start_vertex);
	        			if(DEBUG){
		        			System.out.println("Connecting vertex " + table.table_name + "." + table_key +
		        				 " with " + othertable.table_name + "."  + othertable_key);
		        			if(success)
		        				System.out.println("done!");
	        			}

	        		}
	        		else{
	        			System.out.println("Error: null Vertex");
	        			System.out.println("Tried to connect " + table_key + " with " + othertable_key);
	        			System.out.println("Query:");
	        			System.out.println(query);
	        		}

	        	}

	        }

	    }



	}


	/**
	 * Execute a query. TODO: return type and/or value
	 */
	private ArrayList<String[]> execute_query(String query_ , String[] columns){

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		ArrayList<String[]> result = new ArrayList<String[]>();

		try{
			con = DriverManager.getConnection(database, user, password);
			stmt = con.createStatement();

			rs = stmt.executeQuery(query_);

			while(rs.next()){

				String[] row = new String[columns.length];

        		for(int i = 0; i < columns.length; i++){

        			row[i] = rs.getString(columns[i]);

        		}

        		result.add(row);

        	}

		} catch(SQLException e) {
			//System.err.println("1 - Connection error: " + e.getMessage());
			//System.err.println("Query: " + query_);
			/*for(int i = 0; i < columns.length; i++){
        		System.err.println(columns[i]);
        	}*/
			//System.exit(-1);
		}

		// release resources
		try{

			if(rs != null)
				rs.close();

			if(stmt != null)
				stmt.close();

			if(con != null)
				con.close();

		} catch(SQLException e) {
			System.err.println("Releasing error: " + e.getMessage());
		}
		finally {
			stmt = null;
			con = null;
		}

		return result;

	}


	private void populate_tables_info(){

		try {
			Connection con = DriverManager.getConnection(database, user, password);
			DatabaseMetaData metaData = con.getMetaData();

			String tableType[] = {"TABLE"};

			ResultSet result = metaData.getTables(null,"public",null,tableType);

			while(result.next())
			{

				String tableName = result.getString("TABLE_NAME");

				// create a table object
				Table new_table = new Table(tableName);

				ResultSet columns = metaData.getColumns(null,"public",tableName,null);

				ArrayList<String> col_names = new ArrayList<String>();
				ArrayList<Integer> col_types = new ArrayList<Integer>();

				while(columns.next())
				{
					String columnName = columns.getString("COLUMN_NAME");
					int columnType = columns.getInt("DATA_TYPE");
					//System.out.println(columnType);
					col_names.add(columnName);
					col_types.add(columnType);
				}

				// add columns names to table object
				new_table.columns_name = new String[col_names.size()];
				new_table.columns_type = new int[col_types.size()];
				new_table.compatible_type = new boolean[col_types.size()];

				for(int i = 0; i < col_names.size(); i++){
					new_table.columns_name[i] = col_names.get(i);
					new_table.columns_type[i] = col_types.get(i);
					new_table.compatible_type[i] = (new_table.columns_type[i] == java.sql.Types.VARCHAR);
					/*if(new_table.compatible_type[i])
						System.out.println("Found var char: " + new_table.columns_name[i]);*/
				}

				addPrimaryKeys(con , new_table);

				addForeignKeys(con , new_table);

				// the table object can be added to the hashmap
				tables.put(new_table.table_name , new_table);

			}

		} catch (SQLException e) {
			System.err.println("2 - Connection error: " + e.getMessage());
		}

	}

	private void print_tables_info(){

		// Get a set of the entries
    	Set set = tables.entrySet();

    	// Get an iterator
    	Iterator it = set.iterator();

		System.out.println("Retrieved db schema:");

    	// Display elements
    	while(it.hasNext()) {

        	Map.Entry me = (Map.Entry)it.next();
        	Table entry = (Table)me.getValue();

        	System.out.println("Table Name: " + entry.table_name);
        	System.out.println("Columns: ");
        	System.out.print("(");
        	for(int i = 0; i < entry.columns_name.length; i++){
        		if(i > 0)
        			System.out.print(",");
        		System.out.print(entry.columns_name[i]);
        	}
        	System.out.print(")\n");

        	System.out.println("Primary Key: ");
        	System.out.print("(");
        	for(int i = 0; i < entry.primary_key_columns.length; i++){
        		if(i > 0)
        			System.out.print(",");
        		System.out.print(entry.primary_key_columns[i]);
        	}
        	System.out.print(")\n");

        	System.out.println("Relations:");
        	for(int i = 0; i < entry.foreign_key_columns.length; i++){
        		System.out.println(entry.table_name + "." + entry.foreign_key_columns[i] +
        			" -> " + entry.private_key_tables[i] + "." + entry.private_key_columns[i]);
        	}

        	System.out.println("----------------");

    	}

    }


	private void addPrimaryKeys(Connection connection, Table new_table) throws SQLException {

		String tableName = new_table.table_name;
		DatabaseMetaData metaData = connection.getMetaData();
		ResultSet primaryKeys = metaData.getPrimaryKeys(connection.getCatalog(), null, tableName);
		ArrayList<String> p_col_names = new ArrayList<String>();

		while (primaryKeys.next()) {

			String pkTableName = primaryKeys.getString("TABLE_NAME");
			String pkColumnName = primaryKeys.getString("COLUMN_NAME");
			p_col_names.add(pkColumnName);

		}

		// add primary key columns names to table object
		new_table.primary_key_columns = new String[p_col_names.size()];

		for(int i = 0; i < p_col_names.size(); i++){
			new_table.primary_key_columns[i] = p_col_names.get(i);
		}
	}


	private void addForeignKeys(Connection connection, Table new_table) throws SQLException {

		String tableName = new_table.table_name;
		DatabaseMetaData metaData = connection.getMetaData();
		ResultSet foreignKeys = metaData.getImportedKeys(connection.getCatalog(), null, tableName);
		ArrayList<String> fk_col_names = new ArrayList<String>();
		ArrayList<String> pk_tab_names = new ArrayList<String>();
		ArrayList<String> pk_col_names = new ArrayList<String>();

		while (foreignKeys.next()) {

			String fkTableName = foreignKeys.getString("FKTABLE_NAME");
			String fkColumnName = foreignKeys.getString("FKCOLUMN_NAME");
			String pkTableName = foreignKeys.getString("PKTABLE_NAME");
			String pkColumnName = foreignKeys.getString("PKCOLUMN_NAME");
			//System.out.println(fkTableName + "." + fkColumnName + " -> " + pkTableName + "." + pkColumnName);
			fk_col_names.add(fkColumnName);
			pk_tab_names.add(pkTableName);
			pk_col_names.add(pkColumnName);

		}

		new_table.foreign_key_columns = new String[fk_col_names.size()];
		new_table.private_key_tables = new String[fk_col_names.size()];
		new_table.private_key_columns = new String[fk_col_names.size()];

		for(int i = 0; i < fk_col_names.size(); i++){
			new_table.foreign_key_columns[i] = fk_col_names.get(i);
			new_table.private_key_tables[i] = pk_tab_names.get(i);
			new_table.private_key_columns[i] = pk_col_names.get(i);
		}

	}


public class SingleSearch{


	// variables related to the single keyword search
	private String[] keywords;

	private final int d_max = 2;
	private final double mu = 0.5;
	private final double threshold = 0.001;

	ArrayList<HashMap<String,HashMap<String,Vertex>>> matching_sets;

	int[] ms_size;

	PriorityQueue<Vertex> Q_in;

	PriorityQueue<Vertex> Q_out;

	HashMap<String , Vertex> X_in;

	HashMap<String , Vertex> X_out;

	HashMap<String , Integer> v_depth;

	HashMap<String , HashMap<String , Vertex>> sp_u_i;

	HashMap<String , HashMap<String , Integer>> dist_u_i;

	HashMap<String , Integer> results_trees_keys;

	PriorityQueue<Graph> results_trees;

	PriorityQueue<Out> results_out;


	// costruttore

	public SingleSearch(String[] keywords_){

		this.keywords = keywords_;

		Q_in = new PriorityQueue<Vertex>(new Comparator<Vertex>() {
			public int compare(Vertex v1, Vertex v2) {
				double act1 = v1.getActivation();
				double act2 = v2.getActivation();
		        if(act2 > act1)
		        	return 1;
		        if(act2 < act1)
		        	return -1;
		        return 0;
			}
		});

        Q_out = new PriorityQueue<Vertex>(new Comparator<Vertex>() {
			public int compare(Vertex v1, Vertex v2) {
				double act1 = v1.getActivation();
				double act2 = v2.getActivation();
				if(act2 > act1)
		        	return 1;
		        if(act2 < act1)
		        	return -1;
		        return 0;
			}
		});

	 X_in = new HashMap<String , Vertex>();

	 X_out = new HashMap<String , Vertex>();

	 v_depth = new HashMap<String , Integer>();

	 sp_u_i = new HashMap<String , HashMap<String , Vertex>>();

	 dist_u_i = new HashMap<String , HashMap<String , Integer>>();

	 results_trees_keys = new HashMap<String , Integer>();

	 results_trees = new PriorityQueue<Graph>(new Comparator<Graph>() {
		public int compare(Graph v1, Graph v2) {
			double act1 = v1.getScore();
			double act2 = v2.getScore();
			if(act2 > act1)
	        	return -1;
	        if(act2 < act1)
	        	return 1;
	        return 0;
		}
	});

	results_out = new PriorityQueue<Out>(new Comparator<Out>() {
		public int compare(Out v1, Out v2) {
			double act1 = v1.getScore();
			double act2 = v2.getScore();
			if(act2 > act1)
	        	return 1;
	        if(act2 < act1)
	        	return -1;
	        return 0;
		}
	});


	}

    public long startBSSearch() {

    	resetActivations();

		System.out.println("Finding matching sets...");

		long start_ms = System.currentTimeMillis();
		findMatchingnodes();
		populate_Qin();
		long end_ms = System.currentTimeMillis();

		System.out.println("Searching...");

		long start = System.currentTimeMillis();

		executeBanks2();

		long end = System.currentTimeMillis();

		System.out.println("Searching complete");
	    try{
	        	Thread.sleep(3000);
	    }
	   catch(InterruptedException e){}

	   System.out.println("Displaying results...");
	   	    try{
	        	Thread.sleep(1000);
	    }
	   catch(InterruptedException e){}



		printResults();

		System.out.println("Setup elapsed time: " + (end_ms - start_ms));

		return (end - start);

	}

	public void resetActivations(){

    	Set set = tables.entrySet();
    	Iterator it = set.iterator();

    	while(it.hasNext()) {

        	Map.Entry me = (Map.Entry)it.next();
        	Table table = (Table)me.getValue();

			HashMap<String , Vertex> vertexes_ = ks_graph.getVertexes(table.table_name);
			if(vertexes_ == null){
				System.out.println(table.table_name + " NULL!");
			}
			Set set1 = vertexes_.entrySet();
			Iterator it1 = set1.iterator();

			while (it1.hasNext())  {

				Map.Entry me1 = (Map.Entry)it1.next();
				Vertex vertex_ = (Vertex)me1.getValue();

				// set the keyworks number and reset every value to 0
				vertex_.setKeywordNumber(keywords.length);

			}

		}

	}

	public void findMatchingnodes(){

		matching_sets = new ArrayList<HashMap<String,HashMap<String,Vertex>>>(keywords.length);

		ms_size = new int[keywords.length];

		for(int i = 0; i < keywords.length; i++)  {

			HashMap<String,HashMap<String,Vertex>> map_ = new HashMap<String,HashMap<String,Vertex>>();
			matching_sets.add(i , map_);

			Set set = tables.entrySet();
			Iterator it = set.iterator();



			while (it.hasNext())  {

				Map.Entry me = (Map.Entry)it.next();
				String table_name = (String)me.getKey();
				Table table = (Table) me.getValue();
				HashMap <String, Vertex> temp = new HashMap< String, Vertex >();

				if (table_name.equals(keywords[i])) {

					HashMap<String , Vertex> vertexes_ = ks_graph.getVertexes(table_name);
					Set set1 = vertexes_.entrySet();
					Iterator it1 = set1.iterator();

					while (it1.hasNext())   {

						Map.Entry me1 = (Map.Entry)it1.next();
						Vertex m_vertex = (Vertex)me1.getValue();
						m_vertex.setKeywordNumber(keywords.length);
						String vertex_key = (String)me1.getKey();
						temp.put(vertex_key, m_vertex);

					}

					ms_size[i] += temp.size();
					matching_sets.get(i).put(table_name, temp);

				}

				else {

					for ( int j = 0; j < table.columns_name.length;j++) {

						String query = "SELECT ";

							for (int k = 0; k < table.primary_key_columns.length; k++)  {

								if (k > 0) query = query + ", ";
								query = query + table.primary_key_columns[k];
							}

						query = query + " FROM " + table.table_name + " WHERE " + table.columns_name[j] + "= " + "'" +  keywords[i] + "';";

							ArrayList<String[]> rs = execute_query(query, table.primary_key_columns);

							if (rs.size() > 0)   {


								for (int h= 0; h < rs.size(); h++)  {

									String vkey1 = "";
									String[] row = rs.get(h);

									for (int k = 0; k < table.primary_key_columns.length; k++)  {

										if (k > 0) { vkey1 = vkey1 + "_"; }

										vkey1 = vkey1 + row[k];

									}

									if (temp.get(vkey1) == null) {

										Vertex new_vertex = ks_graph.getVertex(vkey1, table.table_name);
										new_vertex.setKeywordNumber(keywords.length);
										temp.put(vkey1, new_vertex);

									}
								}
							}
					}

					ms_size[i] += temp.size();
					matching_sets.get(i).put(table.table_name, temp);

				}
			}
		}
	}



	public void populate_Qin(){

		for(int i = 0; i < keywords.length; i++)  {

			HashMap<String, HashMap<String,Vertex>> match_set = matching_sets.get(i);

			HashMap<String , Integer> distances = new HashMap<String , Integer>();
			dist_u_i.put(keywords[i] , distances);

			HashMap<String , Vertex> sp_path = new HashMap<String , Vertex>();
			sp_u_i.put(keywords[i] , sp_path);

			Set m_set = match_set.entrySet();
			Iterator m_it = m_set.iterator();

			while (m_it.hasNext())  {

				Map.Entry me = (Map.Entry)m_it.next();
				String table_name = (String)me.getKey();

				HashMap<String,Vertex> vertexes_ = match_set.get(table_name);

				Set v_set = vertexes_.entrySet();
				Iterator v_it = v_set.iterator();

				while (v_it.hasNext())  {

					Map.Entry v_me = (Map.Entry)v_it.next();

					Vertex t_vertex = (Vertex)v_me.getValue();

					double activation_i = 1.0 / (double)ms_size[i];

					t_vertex.setActivation(i, activation_i);

					if(DEBUG)
						System.out.println("Activation to: " + activation_i + " for k: "+ i + " for node " + t_vertex.getKey());

					// set the distance
					dist_u_i.get(keywords[i]).put(t_vertex.getKey() , 0);

					// insert the new vertex in the priority queue
					if(!Q_in.contains(t_vertex)){
						Q_in.add(t_vertex);
						if(DEBUG)
						System.out.println("Added " + t_vertex.getKey());
						v_depth.put(t_vertex.getKey() , 0);
					}
				}
			}
		}
	}

	public void executeBanks2(){

		while(Q_in.size() > 0 || Q_out.size() > 0){

			double act = 0;

			if(Q_in.size() == 0)
				act = -1;
			else
				if(Q_out.size() == 0)
					act = 1;
				else
					act = Q_in.peek().getActivation() - Q_out.peek().getActivation();

			// Qin has highest activation
			if(act > 0){


				Vertex v = Q_in.poll();

				if(DEBUG)
				System.out.println("Extraction from Qin: " + v.getKey());

				X_in.put(v.getKey() , v);

				if(isComplete(v)){

					emit(v);

				}

				if(v_depth.get(v.getKey()) < d_max){

					ArrayList<Edge> incoming = v.getNeighbors(true);

					for(int i = 0; i < incoming.size(); i++){

						Vertex f = incoming.get(i).getOne();
						Vertex u = f;
						Vertex g = incoming.get(i).getTwo();

						if(v.getKey().equals(f.getKey()))
							u = g;

						if(DEBUG)
						System.out.println("Incoming " + i + " is " + u.getKey());

						exploreEdge(u , v);

						if(X_in.get(u.getKey()) == null){

							Q_in.remove(u);
							Q_in.add(u);

							if(DEBUG)
							System.out.println("Adding  " + u.getKey() + " in Qin ");

							Integer depth = v_depth.get(v.getKey());
							v_depth.put(u.getKey() , depth + 1);

						}

					}

					v.scaleActivation((1 - mu));

					if(Q_in.remove(v))
						Q_in.add(v);
					if(Q_out.remove(v))
						Q_out.add(v);

					if(DEBUG)
					System.out.println("Setting "+v.getKey() + " to "+v.getActivation());

					if(X_out.get(v.getKey()) == null){

						Q_out.add(v);

					}

				}

			}
			// Qout has the highest activation
			else{

				Vertex u = Q_out.poll();

				if(DEBUG)
				System.out.println("Extraction from Qout: " + u.getKey());

				X_out.put(u.getKey() , u);

				if(isComplete(u)){

					emit(u);

				}

				if(v_depth.get(u.getKey()) < d_max){

					ArrayList<Edge> outgoing = u.getNeighbors(false);

					for(int i = 0; i < outgoing.size(); i++){

						Vertex f = outgoing.get(i).getOne();
						Vertex v = f;
						Vertex g = outgoing.get(i).getTwo();

						if(u.getKey().equals(f.getKey()))
							v = g;

						if(DEBUG)
						System.out.println("Outgoing " + i + " is " + v.getKey());

						exploreEdge(u , v);

						if(X_out.get(v.getKey()) == null){

							Q_out.remove(v);
							Q_out.add(v);

							if(DEBUG)
							System.out.println("Adding  " + v.getKey() + " in Qout ");

							Integer depth = v_depth.get(u.getKey());
							v_depth.put(v.getKey() , depth + 1);

						}

					}


					u.scaleActivation((1 - mu));

					if(Q_in.remove(u))
						Q_in.add(u);
					if(Q_out.remove(u))
						Q_out.add(u);

					if(DEBUG)
					System.out.println("Setting " + u.getKey() + " to " + u.getActivation());

				}

			}

		}

	}

	public void exploreEdge(Vertex u, Vertex v){

		if(DEBUG)
		System.out.println("Exploring u: " + u.getKey() + " and v: " + v.getKey());

		for(int i = 0; i < keywords.length; i++){

			HashMap<String , Integer> vertexes = dist_u_i.get(keywords[i]);

			Integer dist_u = vertexes.get(u.getKey());
			Integer dist_v = vertexes.get(v.getKey());

			if(dist_u == null && dist_v != null){
				dist_u = dist_v + 1;
				vertexes.put(u.getKey() , dist_u);

				HashMap<String , Vertex> nexts = sp_u_i.get(keywords[i]);
				nexts.remove(u.getKey());
				nexts.put(u.getKey() , v);
				sp_u_i.remove(keywords[i]);
				sp_u_i.put(keywords[i] , nexts);
				if(DEBUG)
				System.out.println("Connecting u = "+ u.getKey() + " to v = " + v.getKey() + " for k = " + keywords[i]);

				attach(u , i);

				if(isComplete(u)){

					emit(u);

				}
			}

			if(dist_u != null && dist_v != null){
				if(dist_u > dist_v + 1){

					vertexes.remove(u.getKey());
					vertexes.put(u.getKey() , dist_v + 1);

					HashMap<String , Vertex> nexts = sp_u_i.get(keywords[i]);
					nexts.remove(u.getKey());
					nexts.put(u.getKey() , v);
					sp_u_i.remove(keywords[i]);
					sp_u_i.put(keywords[i] , nexts);
					if(DEBUG)
					System.out.println("Connecting u = "+ u.getKey() + " to v = " + v.getKey() + " for k = " + keywords[i]);

					attach(u , i);

					if(isComplete(u)){

						emit(u);

					}

				}
			}

			if((mu * v.getActivation(i) / v.getNeighborCount(true)) > u.getActivation(i)){

				u.setActivation(i , mu * v.getActivation(i) / v.getNeighborCount(true));

				if(DEBUG)
				System.out.println(v.getKey() + " has " + v.getNeighborCount(true) + " incoming nodes");

				if(Q_in.remove(u))
					Q_in.add(u);
				if(Q_out.remove(u))
					Q_out.add(u);

				if(DEBUG)
				System.out.println("Setting "+u.getKey() + " to "+u.getActivation(i));

				activate(u , i);

			}

		}

	}


	public void activate(Vertex v , Integer k){

		if(DEBUG)
		System.out.println("Activate v:" + v.getKey() + " for k: " + k);

		ArrayList<Edge> incoming = v.getNeighbors(true);

		for(int i = 0; i < incoming.size(); i++){

			Vertex f = incoming.get(i).getOne();
			Vertex s = f;
			Vertex g = incoming.get(i).getTwo();

			if(v.getKey().equals(f.getKey()))
				s = g;

			double act = mu * v.getActivation(k);
			if(s.getActivation(k) == 0 || act <= threshold)
				return;

			if(s.getActivation(k) < mu * v.getActivation(k)){

				double v_act = v.getActivation(k);
				double new_v_act = (1 - mu) * v_act;
				v.setActivation(k , new_v_act);

				if(Q_in.remove(v))
					Q_in.add(v);
				if(Q_out.remove(v))
					Q_out.add(v);

				double new_s_act = mu * v_act / incoming.size();
				s.setActivation(k , new_s_act);

				if(Q_in.remove(s))
					Q_in.add(s);
				if(Q_out.remove(s))
					Q_out.add(s);

				if(DEBUG)
				System.out.println("Set " + v.getKey() + " to " + new_v_act + " and " + s.getKey() + " to " + new_s_act);

				activate(s , k);

			}

		}


	}

	public void attach( Vertex v , Integer k){

		ArrayList<Edge> incoming = v.getNeighbors(true);

		HashMap<String,Integer> vert = dist_u_i.get(keywords[k]);
		int dist = vert.get(v.getKey());

		for(int i = 0; i < incoming.size(); i++){

			Vertex f = incoming.get(i).getOne();
			Vertex s = f;
			Vertex g = incoming.get(i).getTwo();

			if(v.getKey().equals(f.getKey()))
				s = g;

			int distance = -1;

			if(vert.get(s.getKey()) != null)
				distance = vert.get(s.getKey());

			if(distance == -1 || distance > dist +1){
				vert.remove(s.getKey());
				vert.put(s.getKey(), dist+1);

				HashMap<String , Vertex> nexts = sp_u_i.get(keywords[k]);
				nexts.remove(s.getKey());
				nexts.put(s.getKey() , v);
				sp_u_i.remove(keywords[k]);
				sp_u_i.put(keywords[k] , nexts);
				if(DEBUG)
				System.out.println("Connecting s = "+ s.getKey() + " to v = " + v.getKey() + " for k = " + keywords[k]);


				attach(s , k);

			}

		}
	}


	public boolean isComplete(Vertex v){

		if(DEBUG)
		System.out.println("test for " + v.getKey());

		for(int i =0; i < keywords.length; i++){

			HashMap<String,Integer> vert = dist_u_i.get(keywords[i]);
			Integer distance = vert.get(v.getKey());

			if(distance == null)
				return false;

		}

		if(DEBUG)
			System.out.println("complete! " + v.getKey());
		return true;
	}




	public void emit(Vertex v) {

		Out new_out = new Out();

		//System.out.println("\n\n--->emit di " + v.getKey());

		String to_put = "";

		Vertex root = v.clone();

		to_put += "root " + root.getKey();

		//System.out.println("root " + root.getKey());


		Graph ans_tree = new Graph();

		ans_tree.addVertex(root , root.getTableName() , true);
		ans_tree.setRoot(root);
		int total_count = 0;
		String tree_unique_key = root.getKey();

		for (int i = 0; i <keywords.length;i++) {

			Vertex pointer = root;
			HashMap<String,Integer> vert = dist_u_i.get(keywords[i]);
			int distance = vert.get(pointer.getKey());

			//System.out.println("distanza da k " + keywords[i]+ " = " + distance);
			to_put += " distance to k " + keywords[i]+ " = " + distance + "\n";

			while ( distance != 0) {

				HashMap<String , Vertex> nexts = sp_u_i.get(keywords[i]);

				Vertex neighbor = nexts.get(pointer.getKey()).clone();

				//System.out.println("prossimo: " + neighbor.getKey());
				to_put += " next node: " + neighbor.getKey()+ "\n";

				//if(!ans_tree.containsVertex(neighbor, neighbor.getTableName())){
					ans_tree.addVertex(neighbor , neighbor.getTableName(), true);
					tree_unique_key = tree_unique_key + "_" +  neighbor.getKey();

					ans_tree.addEdge(pointer , neighbor);
				//}

				pointer = neighbor;
				distance = vert.get(pointer.getKey());
				total_count++;

			}


		}

		ans_tree.setScore(total_count);
		new_out.score = total_count;
		new_out.out = to_put;

		if(results_trees_keys.get(tree_unique_key) == null){

			results_trees_keys.put(tree_unique_key , 1);

			//System.out.println("Aggiungo risultato! ");
			//System.out.println("total count: " + total_count);
			results_trees.add(ans_tree);
			results_out.add(new_out);

		}
		//else
			//System.out.println("Albero risultato già inserito!");


	}

	public void printQueryResult() {


			System.out.println("Found "+ results_trees.size() + " results");

		while(results_trees.size() > 0){

			Graph result = results_trees.poll();
			Vertex res = result.getRoot();

			String table_name = res.getTableName();
			String id = res.getLabel();
			String[] search = id.split("_");
			Table table = tables.get(table_name);
			String query = "SELECT * FROM " + table_name + " WHERE ";
			for (int i = 0; i < table.primary_key_columns.length; i++) {

				if (i > 0)  { query = query + " AND "; }
				query = query + table.primary_key_columns[i] + " = " + "'" + search[i] + "'";

			}

			query = query + ";";

			//System.out.println("Query: "+ query);

			ArrayList<String[]> rs = execute_query(query , table.primary_key_columns);

			if (rs.size() == 0) {System.out.println("ERROR: nothing found for vertex root");}

			//
			else if (rs.size() > 1) {System.out.println("ERROR: more than one result found for vertex root");}

			else {

				String to_print = "";
				String[] row = rs.get(0);
				for (int i = 0; i < row.length; i++) {

					if (i > 0) to_print += " ";
					to_print = to_print + row[i];

				}

				System.out.println("root vertex of the answer tree:");
				System.out.println("table " +  table.table_name + " pk "+ to_print);
				System.out.println("total edges weight: " +  result.getScore());

			}
		}

	}  //end printResult

	public void printResults(){

		int size = results_trees.size();

		while(results_out.size() > 0){

			results_trees.poll();

			Out out_ = results_out.poll();

			System.out.println("\nResult with score = " + out_.getScore());
			System.out.println(out_.out);

		}

		System.out.println("Found "+ size + " results");

	}


  }    // end class

}
