
We provide BANKS-II algorithm implementation as a Java project.

### Configuration

The algorithm runs on a generic relational DB uploaded in PostgreSQL.
To use a specific DB update the DB access variables on MainExe.java (line 12-15).

### Execution

Check that the DRIVER postgresql-9.4.1208.jar binary is in the same folder.

To run the algoritmh compile every .java file, then run MainExe from command line using the following command (windows):

java -cp.;postgresql-9.4.1208.jar MainExe

## Developers

* Giovanni Bonato giovanni.bonato@studenti.unipd.it

* Alberto Minetto alberto.minetto@studenti.unipd.it

* Leonardo Pellegrina leonardo.pellegrina@studenti.unipd.it
