import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BasicTupleSet {
	
	public final String tableName;		
	public final String relation;
	public final String keyword;
	private String query;
	
	public BasicTupleSet(String relation, String keyword, String whereCond, Connection con) throws SQLException {
		this.keyword = keyword;
		this.relation = relation;
		tableName = relation+"_"+keyword;
		PreparedStatement stmtTable = con.prepareStatement("CREATE TABLE "+tableName+" AS SELECT * FROM "+relation+" WHERE "+whereCond);
		stmtTable.execute();
		query = "SELECT * from " + tableName;
	}

	public String getQuery() {
		return query;
	}	
	
	public void delete(Connection con) throws SQLException {
		PreparedStatement stmtView = con.prepareStatement("DROP TABLE "+ tableName + " CASCADE");
		stmtView.execute();
	}
}
