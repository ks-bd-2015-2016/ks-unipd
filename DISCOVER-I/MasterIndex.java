import java.sql.*;
import java.util.ArrayList;

public class MasterIndex {

	public static BasicTupleSet[][] generateBasicTupleSetMatrix(String[] keywords, ArrayList<String> tableNames, Connection con) throws SQLException {
		int n = tableNames.size();
		int m = keywords.length;

		// Create output structure to be filled		
		BasicTupleSet[][] basicTupleSetQuery = new BasicTupleSet[n][m];

		// For each h relation/table
		for (int i = 0; i < n; i++){

			// Extract column names
			ArrayList<String> columnNames = SchemaEstractor.getTableColumnNames(tableNames.get(i), con);
			ArrayList<String> columnTypes = SchemaEstractor.getColumnType(tableNames.get(i), con);

			// For each keyword	
			for (int j = 0; j < keywords.length; j++) {
				boolean flag = false;
				String baseQuery = "";

				// Concatenate each name into the query string			
				for (int k = 0; k < columnNames.size(); k++){	
					if (columnTypes.get(k).equals("text") || columnTypes.get(k).equals("character varying")) {
						if (flag)
							baseQuery += " OR to_tsvector('simple'," + columnNames.get(k) + ") @@ to_tsquery('simple','" + keywords[j] + "')" ;
						else {
							baseQuery += "to_tsvector('simple'," + columnNames.get(k) + ") @@ to_tsquery('simple','" + keywords[j] + "')" ;
							flag = true;	
						}	
					}
				}	
				// Fill i-th ROW relative to the i- Table with each keyword
				basicTupleSetQuery[i][j] = new BasicTupleSet(tableNames.get(i), keywords[j], baseQuery, con);
			}
		}
		return basicTupleSetQuery;
	}

	public static ArrayList<TupleSet> getTupleSetsList(final BasicTupleSet[][] basicTupleSetsQuery, final ArrayList<String> tableNames, int n, int m, Connection con) throws SQLException {

		int numbOfKeywordSubsets = (int) Math.pow(2, m); // number of subsets
		ArrayList<TupleSet> tupleSetsList = new ArrayList<TupleSet>();

		for (int i=0; i<n; i++) {
			String keywordsSubSet = String.format("%"+ m +"s", Integer.toBinaryString(0)).replace(' ', '0');
			tupleSetsList.add(new TupleSet(keywordsSubSet, i, "SELECT * FROM " + tableNames.get(i))); //free tuple sets
			
			for (int j=1; j<numbOfKeywordSubsets; j++) {
				keywordsSubSet = String.format("%"+ m +"s", Integer.toBinaryString(j)).replace(' ', '0');

				String interception = "(";
				String union = "(";

				for (int k=0; k<keywordsSubSet.length(); k++) {
					if (keywordsSubSet.charAt(k)=='0') {
						union += basicTupleSetsQuery[i][k].getQuery() + " UNION ";
					}
					else
						interception += basicTupleSetsQuery[i][k].getQuery() + " INTERSECT ";
				}
				//remove the last UNION-INTERSECT
				interception = interception.substring(0, interception.length()-10) + ")";
				String q = "";
				if (j==numbOfKeywordSubsets-1)
					q = interception;
				else {
					union = union.substring(0, union.length()-6) + ")";
					q = interception + " EXCEPT " + union;
					}
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(q+";");
				if (rs.next())
					tupleSetsList.add(new TupleSet(keywordsSubSet, i, q));
			}
		}

		return tupleSetsList;
	}

	public static int[][] generateGraph_Gts(final int[][] Gu, final ArrayList<TupleSet> tupleSetsList) {
		
		int numbOfNodes = tupleSetsList.size(); 
		int[][] Gts = new int[numbOfNodes][numbOfNodes];
		System.out.println();
		for (int i = 0; i < Gts.length; i++) {
			for (int j = 0; j < Gts[1].length; j++) {
				int t1 = tupleSetsList.get(i).getTableNameSchemaIndex(); //first refernce
				int t2 = tupleSetsList.get(j).getTableNameSchemaIndex(); //second reference
				Gts[i][j] = Gu[t1][t2]; //verifies if there is an edge or not
			}
		}
		return Gts;
	}
}
