
import java.util.ArrayList;

public class Queue<T> {
	
	ArrayList<T> Q = new ArrayList<T>();
	
	public T dequeue() {
		if(!isEmpty())
			return Q.remove(0);
		else
			return null;
	}
	
	public void enqueue(T elem) {
		Q.add(elem);
	}
	
	public boolean isEmpty() {
		return Q.size()==0;		
	}
	
	public static void main(String[] args) {
		String a = "aaa";
		System.out.println(a.substring(3,4));
	}
}
