import java.sql.*;
import java.util.*;

public class MainExe {

	final static String DRIVER = "org.postgresql.Driver";
	private static final String DATABASE = "jdbc:postgresql://localhost/imdb";
	private static final String USER ="postgres";
	private static final String PASSWORD = ""; //insert DB password!!

	public static void main(String[] args) {

		try {
			//Register Driver
			System.out.print("Test Driver....");
			Class.forName(DRIVER);
			System.out.println("OK");

		} catch (ClassNotFoundException e) {
			System.err.printf("Driver %s not found: %s.%n", DRIVER, e.getMessage());
			System.exit(-1);
		}

		// Read input keywords
		Scanner input = new Scanner(System.in);
		System.out.print("Digit the keywords (separated by a space): ");
		String input_line = input.nextLine();
		String[] keywordNames = input_line.split("\\s");

		//oper DB connection
		Connection con = null;
		ArrayList<String> tableNames = null;
		String[][][] schemaJoiningConditions = null;
		int[][] Gu = null;
		BasicTupleSet[][] basicTupleSetsMatix = null;
		ArrayList<TupleSet> tupleSetsList = null;

		long exeTime; long totalTime = 0;

		try{
			con = DriverManager.getConnection(DATABASE, USER, PASSWORD); //connection
			
			//get DB schema 
			System.out.print("Getting the DB schema...."); 
			exeTime = System.currentTimeMillis();
			tableNames = SchemaEstractor.getTableName(con);
			SchemaEstractor.makeColumnNameUnique(con, tableNames);
			Gu = SchemaEstractor.getGraph(con);
			schemaJoiningConditions = SchemaEstractor.getJoiningCondition(con);
			exeTime = System.currentTimeMillis()-exeTime; totalTime+=exeTime;
			System.out.println("OK.\tExecution time: "+ exeTime +"ms");

			//generates Tuple Sets
			System.out.print("TupleSets list generation....");
			exeTime = System.currentTimeMillis();
			basicTupleSetsMatix = MasterIndex.generateBasicTupleSetMatrix(keywordNames, tableNames, con);
			tupleSetsList = MasterIndex.getTupleSetsList(basicTupleSetsMatix, tableNames, tableNames.size(), keywordNames.length, con);
			exeTime = System.currentTimeMillis()-exeTime; totalTime+=exeTime;
			System.out.println("OK.\tExecution time: "+ exeTime +"ms");
		}
		catch(SQLException e) {
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}

		for (int i = 0; i < tupleSetsList.size(); i++) {
			System.out.println(tupleSetsList.get(i).toString(keywordNames, tableNames));;
		}

		//Generate Gts graph
		System.out.print("Graph Gts generation...."); exeTime = System.currentTimeMillis();
		int[][] G_TS = MasterIndex.generateGraph_Gts(Gu, tupleSetsList); 
		exeTime = System.currentTimeMillis()-exeTime; totalTime+=exeTime;
		System.out.println("OK.\tExecution time: "+ exeTime +"ms");

		System.out.print("\nInsert param T (max number of joins): ");
		int T = Integer.parseInt(input.nextLine());
		input.close();

		//Candidate Network Generator Algorithm
		System.out.print("Candidate Networks Generator...."); exeTime = System.currentTimeMillis();
		ArrayList<CandidateNetwork> candidateNetworkList = CandidateNetworkGenerator.generateNetworks(G_TS, Gu, T, keywordNames.length, tupleSetsList);
		exeTime = System.currentTimeMillis()-exeTime; totalTime+=exeTime;
		System.out.println("OK.\tExecution time: "+ exeTime +"ms");
		
		System.out.println("\nFound "+candidateNetworkList.size() +" candidate networks");
		int h=1;
		for (CandidateNetwork CN : candidateNetworkList) {
			System.out.println((h++) + "- " + CN.toString(keywordNames, tableNames));
		}
		System.out.println();
		
		ArrayList<TupleSetJoin> TSJlist = PlanGenerator.selectIntermediateResults(candidateNetworkList, tupleSetsList, schemaJoiningConditions, G_TS, con);
		System.out.println("Intermediate (repeated) results");
		h=1;
		for (int i = 0; i < TSJlist.size(); i++) {
			System.out.println((h++) + "- " +TSJlist.get(i).toString(keywordNames, tableNames));
		}
		System.out.println();
		
		try {
			
			//print results
			System.out.println("Evaluation of queries...."); exeTime = System.currentTimeMillis();
			printResults(candidateNetworkList, con); 
			exeTime = System.currentTimeMillis()-exeTime; totalTime+=exeTime;
			System.out.println("OK.\tExecution time: "+ exeTime +"ms");
			
			//eliminates temp tables
			for (int i = 0; i < basicTupleSetsMatix.length; i++) {
				for (int j = 0; j < basicTupleSetsMatix[0].length; j++) {
					
					basicTupleSetsMatix[i][j].delete(con);
				}
			}
			
			for (int i = 0; i < TSJlist.size(); i++) {
				TSJlist.get(i).deleteTSJ(con);
			}
			
			con.close();
		} catch (Exception e) {
			System.err.println("Connection Error!"); e.printStackTrace();
		}
		finally {
			con = null;
		}
		
		System.out.println("\nTotal Execution Time: "+totalTime/1000.0);
	}

	public static void printResults(ArrayList<CandidateNetwork> candidateNetworks, Connection con) throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;

		stmt = con.createStatement(); //statement
		for (int i=0; i<candidateNetworks.size(); i++) {
			String query = candidateNetworks.get(i).getQuery();
			rs = stmt.executeQuery(query);
			ResultSetMetaData md = rs.getMetaData();
			System.out.println("Results of candidate network n." + (i+1));
			while (rs.next()) {
				for(int j=1; j<= md.getColumnCount(); j++) {
					System.out.print(rs.getString(j)+"\t");
				}
				System.out.println();
			}
			System.out.println();
		}
	}
}
