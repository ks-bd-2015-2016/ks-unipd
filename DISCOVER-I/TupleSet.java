

import java.util.ArrayList;

public class TupleSet {
	public static int counterID = 0;
	
	protected String keywords;  			// 0 and 1 string for each keyword 
	protected int tableNameSchemaIndex; 	// Relation number
	protected boolean empty;				// empty
	protected String query;					// Query to retrieve the TupleSet
	public String TSname;					// unique ID
	
	public TupleSet() {
		
	}
	
	public TupleSet(String keywords, int tableNameRef, String query) {
		this.keywords = keywords;
		this.tableNameSchemaIndex = tableNameRef;
		setQuery(query);
		TSname = "TS" + (counterID++);
	}

	public int getTableNameSchemaIndex() {
		return tableNameSchemaIndex;
	}

	public String getKeywords() {
		return keywords;
	}
	
	public String getQuery() {
		return query;
	}
	
	public void setQuery(String q) {
		this.query = q;
	}
	
	public boolean isFree() {
		for (int i = 0; i < keywords.length(); i++) {
			if (keywords.charAt(i)=='1')
				return false;
		}
		return true;
	}
	
	public boolean equals(TupleSet ts) {
		return this.TSname.equals(ts.TSname);
	}
	
	public String toString(String[] keywordNames, ArrayList<String> tableNames) {
		String s = tableNames.get(tableNameSchemaIndex) +"(";
		if (isFree())
			return s += ")";
		
		for (int i = 0; i < keywords.length(); i++) {
			if (keywords.charAt(i)=='1')
				s += keywordNames[i]+", ";
		}
		s = s.substring(0, s.length()-2);
		s+=")";		
		return s;
	}
}
