
import java.sql.*;
import java.util.ArrayList;

public class TupleSetJoin extends TupleSet {
	
	private TupleSet ts1,ts2;
	
	public TupleSetJoin(TupleSet ts1, TupleSet ts2, String ts1_key, String ts2_key, Connection con)  {
		this.TSname = "TSJ"+(counterID++);
		this.ts1 = ts1;
		this.ts2 = ts2;
		query = "CREATE TABLE " + this.TSname + " AS SELECT * FROM (" + ts1.query + ") AS " + ts1.TSname + " INNER JOIN (" + ts2.query + ") AS " + ts2.TSname + " ON " + ts1.TSname+"."+ts1_key+" = "+ts2.TSname+"."+ts2_key+";";
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM " + this.TSname; 
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	public String toString(String[] keywordNames, ArrayList<String> tableNames) {
		return ts1.toString(keywordNames, tableNames) + " JOIN " + ts2.toString(keywordNames, tableNames);
	}
	
	public void deleteTSJ(Connection con) {
		String q = "DROP TABLE " + TSname + " CASCADE;";
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(q);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
}
