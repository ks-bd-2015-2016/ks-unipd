import java.util.ArrayList;

public class CandidateNetworkGenerator {

	public static ArrayList<CandidateNetwork> generateNetworks(int[][] Gts, int[][] Gu , int T, int m, ArrayList<TupleSet> tupleSetsList) {
		ArrayList<CandidateNetwork> candidateNetworkSet = new ArrayList<CandidateNetwork>();	
		Queue<CandidateNetwork> queue = new Queue<CandidateNetwork>();
		for (int i = 0; i < tupleSetsList.size(); i++) {
			TupleSet ts = tupleSetsList.get(i);
			if (ts.getKeywords().charAt(0)=='1') { 
				queue.enqueue(new CandidateNetwork(ts));
			}
		}

		while (!queue.isEmpty()) {
			CandidateNetwork C = queue.dequeue();
			if (verifyPruningCondition(C, Gu))
				continue;	//ignore
			else if (verifyAcceptanceCondition(C)) {
				candidateNetworkSet.add(C);	//add it to the final set
			}
			else if (C.getSize()<T)  {

				ArrayList<TupleSet> netTupleSets = C.getTS_list();

				//find corresponding indexes in tupleSetsList
				int[] netTupleSetsIndexes = new int[tupleSetsList.size()];
				for (int i=0; i<=C.getSize(); i++) {
					for (int j=0; j<tupleSetsList.size(); j++) {
						if (tupleSetsList.get(j).equals(netTupleSets.get(i))) {
							netTupleSetsIndexes[i]=j;
							break;
						}
					}
				}

				//expand the candidate network
				for (int i=0; i<=C.getSize(); i++) {
					for (int j=0; j<Gts[1].length; j++) {
						if (Gts[netTupleSetsIndexes[i]][j]==1 || Gts[j][netTupleSetsIndexes[i]]==1) {
							TupleSet newTS = tupleSetsList.get(j);
							TupleSet TS_where_newTS_is_attached = tupleSetsList.get(netTupleSetsIndexes[i]);
							CandidateNetwork expandedCN = expand(C, newTS, TS_where_newTS_is_attached, Gu); //try to expand
							if (expandedCN != null)	{	//if it has worked
								queue.enqueue(expandedCN);
							}
						}
					}
				}
			}	
		}		
		return candidateNetworkSet;
	}

	private static boolean verifyAcceptanceCondition(CandidateNetwork CN) {

		String netKeywords = CN.getNetKeywords();
		int[][] candidateNetwork = CN.getCandidateNetwork();
		ArrayList<TupleSet> TS_list = CN.getTS_list();

		//verify CN is total
		for (int i=0; i < netKeywords.length(); i++)
			if (netKeywords.charAt(i)=='0') {
				return false; 	//does not contain any keyword
			}

		//verify is minimal
		int numberOfTupleSets = CN.getSize()+1; //actual number of nodes in the CN
		for (int i=0; i < numberOfTupleSets; i++) {

			//the node is a leaf iif contains a singe "1" in the line and in the column
			int counter = 0;
			for (int j=0; j < numberOfTupleSets; j++) {
				if (candidateNetwork[i][j]==1 || candidateNetwork[j][i]==1)
					counter += 1;
			}

			if(counter==1 && TS_list.get(i).isFree()) { //is a leaf and it's a free tuple set
				return false;
			}
		}

		return true;
	}

	private static boolean verifyPruningCondition(CandidateNetwork CN, int[][] Gu) {

		int numberOfTupleSets = CN.getSize()+1; //number of actual nodes
		ArrayList<TupleSet> TS_list = CN.getTS_list();

		for (int i = 0; i < numberOfTupleSets; i++) {
			for (int j = i+1; j < numberOfTupleSets; j++) {
				for (int h = j+1; h < numberOfTupleSets; h++) {
					//for each triple i,j,k
					int r_i = TS_list.get(i).getTableNameSchemaIndex(); //ref of TupleSet i in tableNames
					int r_j = TS_list.get(j).getTableNameSchemaIndex(); //ref TupleSet j
					int r_h = TS_list.get(h).getTableNameSchemaIndex(); //ref TupleSet h

					if 	   (	(r_j == r_h && Gu[r_j][r_i]+Gu[r_h][r_i]==2)	//OR  TupleSet i in the middle + verify pruning condition
							||	(r_i == r_h && Gu[r_i][r_j]+Gu[r_h][r_j]==2)	//OR  TupleSet j in the middle + verify pruning condition
							||  (r_i == r_j && Gu[r_i][r_h]+Gu[r_j][r_h]==2))	//OR  TupleSet k in the middle + verify pruning condition
						return true;
				}
			}
		}

		return false;
	}

	private static CandidateNetwork expand(CandidateNetwork CN, TupleSet newTS, TupleSet TS_where_to_attach, int[][] Gu) {

		int numberOfTupleSets = CN.getSize()+1; //number of acutual nodes
		ArrayList<TupleSet> TS_list = CN.getTS_list();

		//if newTS is already in the list, do not expand
		for (int i=0; i < numberOfTupleSets; i++) 
			if (TS_list.get(i).equals(newTS)) 
				return null; 

		//find Tuple Set index where to attach
		int TS_where_to_attach_index = -1;
		for (int i=0; i < numberOfTupleSets; i++) {
			if (TS_list.get(i).equals(TS_where_to_attach)) {
				TS_where_to_attach_index = i;
				break;
			}	
		}

		if(TS_where_to_attach_index==-1)
			throw new RuntimeException("Errore in Espansione.");

		//copy all the objects to create a new CN 
		String newNetKeywords = keywords_OR(CN.getNetKeywords(), newTS.getKeywords());
		ArrayList<TupleSet> newTS_list = generateExpandedTS_list(CN, newTS);
		int[][] newNetwork = generateExpandedCandidateNetwork(CN, TS_where_to_attach_index);

		CandidateNetwork newCN = new CandidateNetwork(newNetKeywords, newTS_list, newNetwork);

		//verify if newTS is a free tuple set o contributes for at least a keyword
		if (newTS.isFree() || everyTS_contributes(newCN))
			return newCN; 

		return null;
	}

	private static ArrayList<TupleSet> generateExpandedTS_list(CandidateNetwork CN, TupleSet newTS) {
		ArrayList<TupleSet> TS_list = CN.getTS_list();
		ArrayList<TupleSet> newTS_list = new ArrayList<TupleSet>(TS_list.size());
		for (int i = 0; i < TS_list.size(); i++) {
			newTS_list.add(i, TS_list.get(i));
		}
		newTS_list.add(newTS);
		return newTS_list;
	}

	private static int[][] generateExpandedCandidateNetwork(CandidateNetwork CN, int TS_where_to_attach_index) {

		int[][] candidateNetwork = CN.getCandidateNetwork();

		int[][] newNetwork = new int[candidateNetwork.length+1][candidateNetwork.length+1];
		for (int i = 0; i < candidateNetwork.length; i++) {
			for (int j = 0; j < candidateNetwork.length; j++) {
				newNetwork[i][j] = candidateNetwork[i][j];
			}
		}
		newNetwork[candidateNetwork.length][TS_where_to_attach_index] = 1;
		return newNetwork;
	}

	private static boolean everyTS_contributes(CandidateNetwork CN) {
		ArrayList<TupleSet> TS_list = CN.getTS_list();

		for (int i = 0; i < TS_list.size() ; i++) { //consider every TS...  
			if (!TS_list.get(i).isFree()) { //not free
				//inizialize a string of 0s
				String keywordWithout_i_th_TS = String.format("%"+ CN.getNetKeywords().length() +"s", Integer.toBinaryString(0)).replace(' ', '0'); 
				for (int j = 0; j < TS_list.size(); j++) { //consider the union of the keyword related to every TS != i-th 
					if (i!=j && !TS_list.get(j).isFree())
						keywordWithout_i_th_TS = keywords_OR(keywordWithout_i_th_TS, TS_list.get(j).getKeywords());
				}

				if (keywordWithout_i_th_TS.equals(CN.getNetKeywords())) {
					return false; // i-th TS does not contribute
				}
			}
		}
		return true;
	}

	private static String keywords_OR(String netKeywords, String newTSKeywords) {
		String newNetKeywords = "";
		for (int i = 0; i < netKeywords.length(); i++) {
			if (netKeywords.charAt(i)=='1' || newTSKeywords.charAt(i)=='1')
				newNetKeywords += 1;
			else
				newNetKeywords += 0;
		}
		return newNetKeywords;
	}
}
