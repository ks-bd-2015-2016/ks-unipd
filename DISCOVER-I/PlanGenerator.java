
import java.sql.Connection;
import java.util.ArrayList;

public class PlanGenerator {
	
	public static ArrayList<TupleSetJoin> selectIntermediateResults(ArrayList<CandidateNetwork> candidateNetworkList, ArrayList<TupleSet> tupleSetList, String[][][] schemaJoiningCondition, int[][] G_TS, Connection con) {
		ArrayList<TupleSetJoin> L = new ArrayList<TupleSetJoin>(); 	//Intermediate results
		String[][][] joiningConditions = generateJoiningConditions(tupleSetList, schemaJoiningCondition);
		
		while (true) {
			int[] maxFreqJoin = findMaxFrequentJoin(candidateNetworkList, tupleSetList);
			if (maxFreqJoin[0]==-1) //Check whether intermediate results can be used
				break;
			
			int i = maxFreqJoin[0]; //i<j
			int j = maxFreqJoin[1];
			TupleSet ts1 = tupleSetList.get(i);
			TupleSet ts2 = tupleSetList.get(j);
			
			TupleSetJoin TSJ = new TupleSetJoin(ts1, ts2, joiningConditions[i][j][0], joiningConditions[i][j][1], con);
			
			joiningConditions = addAJoiningCondition(joiningConditions, G_TS, i, j);
			G_TS = addNodeInAGraph(G_TS, i, j);
			tupleSetList.add(TSJ);
			L.add(TSJ);
			
			for (int k = 0; k < candidateNetworkList.size(); k++) {
				int i1 = candidateNetworkList.get(k).contains(ts1);
				int i2 = candidateNetworkList.get(k).contains(ts2);
				if(i1!=-1 && i2!=-1) {
					int[][] network = candidateNetworkList.get(k).getCandidateNetwork();
					ArrayList<TupleSet> TS_list = candidateNetworkList.get(k).getTS_list();
					TS_list.set(i1, TSJ);
					TS_list.remove(i2);
					network = merge2NodesInAGraph(network, i1, i2);
					candidateNetworkList.get(k).setCandidateNetwork(network);
				}
			}	
		}
		generateCandidateNetworkQuery(candidateNetworkList, tupleSetList, joiningConditions);
		return L;
	}
	
	public static void generateCandidateNetworkQuery(ArrayList<CandidateNetwork> CNlist, ArrayList<TupleSet> tupleSetList, String[][][] joiningConditions) {
		
		for (int k = 0; k < CNlist.size(); k++) {
			CandidateNetwork candidateNetwork = CNlist.get(k);
			int[][] tempNet = candidateNetwork.getCandidateNetwork();
			ArrayList<TupleSet> TS_list = candidateNetwork.getTS_list();

			Queue<TupleSet> tempQueue = new Queue<TupleSet>();
			int numberOfTupleSets = candidateNetwork.getSize()+1;
			String q0 =  TS_list.get(0).getQuery();
			String query = "SELECT * FROM (" + q0 + ") AS " + TS_list.get(0).TSname;
			tempQueue.enqueue(TS_list.get(0));		
			
			while(!tempQueue.isEmpty()) {
				TupleSet tempTS = tempQueue.dequeue();
				int i = findIndex(candidateNetwork, tempTS);
				for (int j=0; j<numberOfTupleSets; j++) {
					if (tempNet[i][j]==1 || tempNet[j][i]==1) { 	//If there is a neighbour
						tempNet[i][j]=0; tempNet[j][i]=0; 			//Delete the edge already considered
																	//Find the tables involved in the join condition
						int r1 = findIndex(tupleSetList, TS_list.get(i));
						int r2 = findIndex(tupleSetList, TS_list.get(j));

						String joinCond = TS_list.get(i).TSname+ "."+joiningConditions[r1][r2][0] +" = "+ TS_list.get(j).TSname+"."+joiningConditions[r1][r2][1];

						//Adding JOIN
						if (joiningConditions[r1][r2].equals(""))
							System.err.println("Error: Join condition does not exists");
						String qj = TS_list.get(j).getQuery();
						query += " INNER JOIN (" + qj  +") AS " + TS_list.get(j).TSname + " ON " + joinCond ;
						tempQueue.enqueue(TS_list.get(j));
					}
				}
			}
			query += " LIMIT 10;";
			candidateNetwork.setQuery(query);
		}
	}
	
	private static int findIndex(ArrayList<TupleSet> tupleSetList, TupleSet ts) {

		int numberOfTupleSets = tupleSetList.size();
		for(int i=0; i<numberOfTupleSets; i++) {
			if(ts.equals(tupleSetList.get(i)))
				return i;
		}
		return -1; //Should never happen
	}
	
	private static int findIndex(CandidateNetwork CN, TupleSet ts) {

		int numberOfTupleSets = CN.getSize()+1;
		ArrayList<TupleSet> TS_list = CN.getTS_list();
		for(int i=0; i<numberOfTupleSets; i++) {
			if(ts.equals(TS_list.get(i)))
				return i;
		}
		return -1; //Should never happen
	}
	
	private static String[][][] addAJoiningCondition(String[][][] joiningCond ,int[][] G_TS, int i, int j) {
		int n = G_TS.length;
		String[][][] newJoiningCond = new String[n+1][n+1][2];
		
		for (int k = 0; k < newJoiningCond.length; k++) {
				newJoiningCond[n][k][0]="";
				newJoiningCond[n][k][1]="";
				newJoiningCond[k][n][0]="";
				newJoiningCond[k][n][1]="";
		}
		
		for (int k = 0; k < joiningCond.length; k++) {
			if (!joiningCond[j][k][0].equals("") && k!=i) {
				newJoiningCond[n][k][0] = joiningCond[j][k][0];
				newJoiningCond[n][k][1] = joiningCond[j][k][1];
				
				newJoiningCond[k][n][0] = joiningCond[j][k][1];
				newJoiningCond[k][n][1] = joiningCond[j][k][0];
				
			}
			else if (!joiningCond[i][k][0].equals("") && k!=j) {
				newJoiningCond[n][k][0] = joiningCond[i][k][0];
				newJoiningCond[n][k][1] = joiningCond[i][k][1];
				
				newJoiningCond[k][n][0] = joiningCond[i][k][1];
				newJoiningCond[k][n][1] = joiningCond[i][k][0];
			}
		}
		
		for (int k = 0; k < n; k++) { 			//copy from 0,0 to j-1,j-1
			for (int k2 = 0; k2 < n; k2++) {
				newJoiningCond[k][k2][0] = joiningCond[k][k2][0];
				newJoiningCond[k][k2][1] = joiningCond[k][k2][1];
			}
		}
		
		newJoiningCond[n][n][0] = ""; 
		newJoiningCond[n][n][1] = "";
		
		return newJoiningCond;
	}
	
	private static int[][] addNodeInAGraph(int[][] G_TS, int i, int j) {
		int n = G_TS.length;
		int[][] newGTS = new int[n+1][n+1];
		
		for (int k = 0; k < G_TS.length; k++) {
			if (G_TS[i][k] == 1 || G_TS[j][k] == 1) //OR between i and j rows
				newGTS[n][k] = 1;
			if (G_TS[k][i] == 1 || G_TS[k][j] == 1) //OR between i and j columns
				newGTS[k][n] = 1;
		}
		
		for (int k = 0; k < n; k++) { 			//copy from 0,0 to j-1,j-1
			for (int k2 = 0; k2 < n; k2++) {
				newGTS[k][k2] = G_TS[k][k2];
			}
		}
		
		newGTS[n][n]=0;
		newGTS[i][n]=0;
		newGTS[j][n]=0;
		newGTS[n][i]=0;
		newGTS[n][j]=0;
		
		return newGTS;
	}
	
	private static int[][] merge2NodesInAGraph(int[][] G_TS, int i, int j) {
		int n = G_TS.length;
		int[][] newGTS = new int[n-1][n-1];
		
		for (int k = 0; k < G_TS.length; k++) {
			if (G_TS[i][k] == 1 || G_TS[j][k] == 1) //OR between i and j rows
				G_TS[i][k] = 1;
			if (G_TS[k][i] == 1 || G_TS[k][j] == 1) //OR between i and j columns
				G_TS[k][i] = 1;
		}
		
		for (int k = 0; k < j; k++) { //copy from 0,0 to j-1,j-1
			for (int k2 = 0; k2 < j; k2++) {
				newGTS[k][k2] = G_TS[k][k2];
			}
		}
		
		for (int k = 0; k < j; k++) { //copy from 0,j+1 to j,n 
			for (int k2 = j; k2 < n-1 ; k2++) {
				newGTS[k][k2] = G_TS[k][k2+1];
			}
		}
		
		for (int k = j; k < n-1; k++) { //copy from j+1,0 to n,j 
			for (int k2 = 0; k2 < j ; k2++) {
				newGTS[k][k2] = G_TS[k+1][k2];
			}
		}
		
		for (int k = j; k < n-1; k++) { //copy from j+1,j+1 to n,n 
			for (int k2 = j; k2 < n-1 ; k2++) {
				newGTS[k][k2] = G_TS[k+1][k2+1];
			}
		}
		
		newGTS[i][i]=0;
		
		return newGTS;
	}
	
	private static int findTSindex(TupleSet ts, ArrayList<TupleSet> tupleSetList) {
		for (int i = 0; i < tupleSetList.size(); i++) {
			if (tupleSetList.get(i).equals(ts))
				return i;
		}
		return -1;
	}	
	
	private static int[] findMaxFrequentJoin(ArrayList<CandidateNetwork> candidateNetworkList, ArrayList<TupleSet> tupleSetList) {
		int n = tupleSetList.size();
		int[][] freq = new int[n][n];
		for (int h = 0; h < candidateNetworkList.size(); h++) {
			CandidateNetwork CN = candidateNetworkList.get(h);
			int[][] network = CN.getCandidateNetwork();
			ArrayList<TupleSet> TS_list = CN.getTS_list();
			for (int i = 0; i < network.length; i++) {
				for (int j = 0; j < network.length; j++) {
					if(network[i][j]==1) {
						int i1 = findTSindex(TS_list.get(i), tupleSetList);
						int i2 = findTSindex(TS_list.get(j), tupleSetList);
						freq[i1][i2]++;
					}	
				}
			}
		}
			
		int max=1, max_i = -1, max_j = -1;
		for (int i = 0; i < freq.length; i++) {
			for (int j = 0; j < freq.length; j++) {
				if(freq[i][j]>max) {
					max_i = i;
					max_j = j;
				}
			}
		}
		int[] cella = {Math.min(max_i, max_j), Math.max(max_i, max_j)}; 
		return cella;	//return -1 -1 if there aren't join with frequency > 2
	}
	
	private static String[][][] generateJoiningConditions(ArrayList<TupleSet> tupleSetList, String[][][] schemaJoiningCondition) {		
		int n = tupleSetList.size();
		String[][][] joiningConditions = new String[n][n][2];
		for (int i = 0; i < joiningConditions.length; i++) {
			for (int j = 0; j < joiningConditions.length; j++) {
				int r1 = tupleSetList.get(i).getTableNameSchemaIndex();
				int r2 = tupleSetList.get(j).getTableNameSchemaIndex();
				
				joiningConditions[i][j][0] = schemaJoiningCondition[r1][r2][0];
				joiningConditions[i][j][1] = schemaJoiningCondition[r1][r2][1];
			}
		}		
		return joiningConditions;
	}
}
