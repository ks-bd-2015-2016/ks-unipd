-- This is specific for the Imdb dataset.
-- Creates an index on each text field using PostgreSQL simple configuration for the to_tsvector function 
-- See PostgreSQL Full Text Search Documentation

CREATE INDEX cast_note_idx ON cast_info USING gin(to_tsvector('simple',note));

CREATE INDEX char_name_name_idx ON char_name USING gin(to_tsvector('simple',name));
CREATE INDEX char_imdb_name_idx ON char_name USING gin(to_tsvector('simple',imdb_index));
CREATE INDEX name_pcode_nf_idx ON char_name USING gin(to_tsvector('simple',name_pcode_nf));
CREATE INDEX surname_pcode_idx ON char_name USING gin(to_tsvector('simple',surname_pcode));

CREATE INDEX movie_info_idx ON movie_info USING gin(to_tsvector('simple',info));
CREATE INDEX movie_info_note_idx ON movie_info USING gin(to_tsvector('simple',note));

CREATE INDEX name_name_idx ON name USING gin(to_tsvector('simple',name));
CREATE INDEX name_imdb_idx ON name USING gin(to_tsvector('simple',imdb_index));
CREATE INDEX name_pcode_cf_idx ON name USING gin(to_tsvector('simple',name_pcode_cf));
CREATE INDEX name_name_pcode_nf_idx ON name USING gin(to_tsvector('simple',name_pcode_nf));
CREATE INDEX name_surname_pcode_idx ON name USING gin(to_tsvector('simple',surname_pcode));

CREATE INDEX role_idx ON role_type USING gin(to_tsvector('simple',role));

CREATE INDEX title_idx ON title USING gin(to_tsvector('simple',title));
CREATE INDEX imdb_idx ON title USING gin(to_tsvector('simple',imdb_index));
CREATE INDEX phonetic_code_idx ON title USING gin(to_tsvector('simple',phonetic_code));
CREATE INDEX years_idx ON title USING gin(to_tsvector('simple',series_years));












