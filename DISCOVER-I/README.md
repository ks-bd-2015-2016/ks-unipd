INSTRUCTIONS
DISCOVER algorithm implementation. Are provided the source code files and classes
To run it, make sure that postgresql-9.4.1208.jre6.jar is in the list of the referenced libraries (if using Eclipse or simila IDE).
No parameter are required on the command line.

DEVELOPERS
Nicolò Soleti - nicolo.soleti@studenti.unipd.it
Federico Vendramin federico.vendramin@studenti.unipd.it