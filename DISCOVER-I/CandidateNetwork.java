import java.util.ArrayList;

public class CandidateNetwork {
	private final String netKeywords;
	private ArrayList<TupleSet> TS_list;
	private int[][] candidateNetwork;
	private String query = "";

	public CandidateNetwork(TupleSet ts) {
		TS_list = new ArrayList<TupleSet>();
		TS_list.add(ts);
		netKeywords = ts.getKeywords();
		candidateNetwork = new int[1][1];
	}

	public CandidateNetwork(String keywords, ArrayList<TupleSet> TS_list, int[][] network) {
		this.netKeywords = keywords;
		this.candidateNetwork = network;
		this.TS_list = TS_list;
	}

	public int getSize() { //number of edges
		return TS_list.size()-1;
	}

	public ArrayList<TupleSet> getTS_list() {
		return TS_list;
	}

	public int[][] getCandidateNetwork() {
		return candidateNetwork;
	}
	public void setCandidateNetwork(int[][] network) {
		this.candidateNetwork = network;		
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getNetKeywords() {
		return netKeywords;
	}
	
	public int contains(TupleSet ts) {
		for (int i = 0; i < TS_list.size(); i++) {
			if (TS_list.get(i).equals(ts)) {
				return i;
			}
		}
		return -1;
	}

	public String toString(String[] keywordNames, ArrayList<String> tableNames) {
		String s = "";
		for (int i = 0; i < TS_list.size(); i++) {
			s += TS_list.get(i).toString(keywordNames, tableNames) + " JOIN ";
		}
		s = s.substring(0, s.length()-5); //removes last JOIN
		return s;
	}

	
}
