
import java.sql.*;
import java.util.ArrayList;

public class SchemaEstractor {

	private static final String TABLE_NAMES_QUERY = "SELECT table_name FROM information_schema.tables WHERE table_schema = ? ";
	private static final String TABLE_COLUMN_NAMES_QUERY = "SELECT column_name FROM information_schema.columns WHERE table_name = ?";
	private static final String COLUMN_TYPE_QUERY = "SELECT data_type FROM information_schema.columns WHERE table_name= ? ";
	private static final String SCHEMA_FOREIGN_KEY_REFERENCES_QUERY = "SELECT DISTINCT kcu.table_name AS table, ccu.table_name as foreigned_by FROM "
																		+"information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name "
																		+"JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name "
																		+"WHERE constraint_type = 'FOREIGN KEY' AND tc.table_schema= ? ";
	private static final String SCHEMA_JOINING_COLUMNS_REFERENCES_QUERY = "SELECT DISTINCT kcu.table_name AS table, kcu.column_name AS columnfk, ccu.table_name as foreigned_by, ccu.column_name as columnpk FROM "
																		+"information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name "
																		+"JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name "
																		+"WHERE constraint_type = 'FOREIGN KEY' AND tc.table_schema= ?";	

	/**
	 * 
	 * @param con DB Connection Object 
	 * @return list of relations names of the DB. Assuming that schema where all the tables are defined its the DEFAULT: 'public'
	 * @throws SQLException
	 */
	public static ArrayList<String> getTableName(Connection con) throws SQLException{
		return getTableName("public", con);
	}
	
	public static void makeColumnNameUnique(Connection con, ArrayList<String> table_names) throws SQLException{
		@SuppressWarnings("unchecked")
		ArrayList<String>[] mtr_tab_cl_name = new ArrayList[table_names.size()];
		ArrayList<boolean[]> tobechangecolumn = new ArrayList<boolean[]>();
		for(int i=0; i<table_names.size(); i++)
			mtr_tab_cl_name[i]= new ArrayList<String>();
		
		for(int i=0; i<table_names.size(); i++){
			ArrayList<String> column_name = getTableColumnNames(table_names.get(i), con);
			boolean[] b = new boolean[column_name.size()];
			ArrayList<String> column = mtr_tab_cl_name[i];
			for(int j=0; j<column_name.size(); j++){
				column.add(column_name.get(j));
			}
			for(int j=0; j<column_name.size(); j++){
				//System.out.print(column.get(j));
			}
			//System.out.println();
			mtr_tab_cl_name[i] = column;
			tobechangecolumn.add(b);
		}
		
		for(int i=0; i<table_names.size(); i++){
			ArrayList<String> column = mtr_tab_cl_name[i];
			for(int j=0; j<column.size(); j++){				// For each column of each table check if there are duplicates
				if(!tobechangecolumn.get(i)[j]){			// If the element doesn't need to be changed already
					String nomecolonna = column.get(j);
			
					for(int k=i+1; k<table_names.size(); k++){
						ArrayList<String> columnver = mtr_tab_cl_name[k];
						for(int t=0; t<columnver.size(); t++){		//Looking for duplicates
							if(nomecolonna.equalsIgnoreCase(columnver.get(t))){
								tobechangecolumn.get(i)[j] = true;
								tobechangecolumn.get(k)[t] = true;
							}
						}
					}
				}
			}
		}
		// Found all duplicates
		
		Statement stmt = null;
		stmt = con.createStatement();
		for(int i=0; i<tobechangecolumn.size(); i++){
			boolean[] b = tobechangecolumn.get(i);
			for(int j=0; j<b.length; j++){
				if(b[j]){
					String sql = "ALTER TABLE "+table_names.get(i)+" RENAME COLUMN "+mtr_tab_cl_name[i].get(j)+" TO "+table_names.get(i)+mtr_tab_cl_name[i].get(j)+";";
					//System.out.println(sql);
					stmt.execute(sql);
					b[j]=false;
				}
			}
		}
		stmt.close();
	} 
	
	public static ArrayList<String> getColumnType(String tablename, Connection con) throws SQLException{
		ArrayList<String> result = new ArrayList<String>(10);
		PreparedStatement stmt = con.prepareStatement(COLUMN_TYPE_QUERY); 	//statement
		stmt.setString(1, tablename);
		
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			result.add(rs.getString("data_type"));
		}

		rs.close();
		stmt.close();
		return result;
	}

	/**
	 * 
	 * @param schemaname Schema name where all the tables are defined 
	 * @param con DB Connection Object 
	 * @return List of tables names in the database schemaname
	 * @throws SQLException 
	 */
	public static ArrayList<String> getTableName(String schemaname, Connection con) throws SQLException {
		ArrayList<String> result = new ArrayList<String>(10);

		PreparedStatement stmtTables = con.prepareStatement(TABLE_NAMES_QUERY); //statement	
		stmtTables.setString(1, schemaname);
		ResultSet rs = stmtTables.executeQuery();

		while (rs.next()) {
			result.add(rs.getString("table_name"));
		}
		rs.close();
		stmtTables.close();

		return result;
	}

	/**
	 * 
	 * @param tablename Table name
	 * @param con DB Connection Object
	 * @return list of column names
	 * @throws SQLException
	 */
	public static ArrayList<String> getTableColumnNames(String tablename, Connection con) throws SQLException{
		ArrayList<String> result = new ArrayList<String>(10);

		PreparedStatement stmtColumns = con.prepareStatement(TABLE_COLUMN_NAMES_QUERY); //statement	
		stmtColumns.setString(1, tablename);
		ResultSet rs = stmtColumns.executeQuery();

		while (rs.next()) {
			result.add(rs.getString("column_name"));
		}

		rs.close();
		stmtColumns.close();
		return result;
	}

	/**
	 * 
	 * @param con DB Connection Object
	 * @return Integer matrix representing an oriented graph with [i][j] = 1 if the table i is referenced from foreign key of the j-th table. Assuming that schema where all the tables are defined its the DEFAULT: 'public'
	 * @throws SQLException
	 */
	public static int[][] getGraph(Connection con) throws SQLException{
		return getGraph("public", con);
	}

	/**
	 * @param schemaname Schema name if different from public
	 * @param con DB Connection Object
	 * @return Integer matrix represented an oriente graph with [i][j] = 1 if the i-th table is pointed by foreign key of the j-th table
	 * @throws SQLException
	 */
	public static int[][] getGraph(String schemaname, Connection con) throws SQLException{
		ArrayList<String> tablename = getTableName(schemaname, con);
		int[][] result = new int[tablename.size()][tablename.size()];
		PreparedStatement stmtForeignKeys = con.prepareStatement(SCHEMA_FOREIGN_KEY_REFERENCES_QUERY); //statement
		stmtForeignKeys.setString(1, schemaname);

		ResultSet rs= stmtForeignKeys.executeQuery();	//Evaluate SQL
		while(rs.next()){
			String tablefk = rs.getString("table");
			String tablepk = rs.getString("foreigned_by");
			int fkindex = searchStringArray(tablename, tablefk);
			int pkindex = searchStringArray(tablename, tablepk);
			result[pkindex][fkindex] = 1;			
		}		
		rs.close();
		stmtForeignKeys.close();
		return result;
	}

	/**
	 * 
	 * 
	 * @param con DB Connection Object
	 * @return String matrix representing a directed graph with [i][j] = textual condition to join the table using SQL joins
	 * @throws SQLException
	 */
	public static String[][][] getJoiningCondition( Connection con) throws SQLException{
		return getJoiningCondition("public", con);
	}

	/**
	 * 
	 * @param schemaname Schema name if different from public
	 * @param con DB Connection Object
	 * @return String matrix representing a directed graph with [i][j] = textual condition to join the table using SQL joins. Assuming that schema where all the tables are defined its the DEFAULT: 'public'
	 * @throws SQLException
	 */
	public static String[][][] getJoiningCondition(String schemaname, Connection con) throws SQLException{
		ArrayList<String> tablename = getTableName(schemaname, con);
		int numbOfTables = tablename.size();
		String[][][] schemaJoiningCondition = new String[numbOfTables][numbOfTables][2];
		PreparedStatement stmtJoiningConditions = con.prepareStatement(SCHEMA_JOINING_COLUMNS_REFERENCES_QUERY); //statement
		stmtJoiningConditions.setString(1, schemaname);

		for(int i=0; i<numbOfTables; i++)
			for(int j=0; j<numbOfTables; j++)
				for (int h = 0; h < 2; h++) 
					schemaJoiningCondition[i][j][h]="";

		ResultSet rs= stmtJoiningConditions.executeQuery(); // Evaluate SQL
		while(rs.next()){
			String tablefk = rs.getString("table");
			String columnfk = rs.getString("columnfk");
			String tablepk = rs.getString("foreigned_by");
			String columnpk = rs.getString("columnpk");

			int fkindex = searchStringArray(tablename, tablefk);
			int pkindex = searchStringArray(tablename, tablepk);

			schemaJoiningCondition[pkindex][fkindex][0] = columnpk;
			schemaJoiningCondition[pkindex][fkindex][1] = columnfk;
			schemaJoiningCondition[fkindex][pkindex][0] = columnfk;
			schemaJoiningCondition[fkindex][pkindex][1] = columnpk;
		}		
		rs.close();
		stmtJoiningConditions.close();
		/*
		System.out.println();
		
		for (int i = 0; i < tablename.size(); i++) {
			System.out.print(tablename.get(i)+"\t");
		}
		System.out.println();
		
		for (int i = 0; i < schemaJoiningCondition.length; i++) {
			for (int j = 0; j < schemaJoiningCondition.length; j++) {
				System.out.print(schemaJoiningCondition[i][j][0] +" - " + schemaJoiningCondition[i][j][1] + "\t");
			}
			System.out.println();
		}
		*/
		return schemaJoiningCondition;
	}

	private static int searchStringArray(ArrayList<String> sa, String s){
		for(int i=0; i<sa.size(); i++){
			if(sa.get(i).equalsIgnoreCase(s))
				return i;
		}
		return -1;
	}
}
