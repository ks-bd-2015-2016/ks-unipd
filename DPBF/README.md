###LIBRARY INFORMATION
This library uses our priority queue implementation to avoid Java Standard Library imperfections.
To develop the code we used IntelliJ(R).

###DEPENDENCES
It requires 9.4.1208 postregres version. The correct .jar file is given in the package. 

###EXECUTION INFORMATION
This implementation works only with Mondial and Imdb databases. The methods to build the graph are designed specifically on their schema. 
It is important to set correctly the database that you want to explore in execution phase: a menu will appear in the shell to choose it.
Introducting a new method it's possible to use this library with every database.

The main is ExampleExecution.java.

##CONTRIBUTORS
Badan Alex - alex.badan@studenti.unipd.it
Benvegnù Luca - luca.benvegnu.2@studenti.unipd.it
Tessarotto Matteo - matteo.tessarotto.1@studenti.unipd.it


