/**
 * PopulationInterface.java
 */
 
package interfaces;

import graph.Vertex;

import java.util.ArrayList;
import java.util.HashMap;

public interface PopulationInterface{

    /**
     * Method to create graph of the database
     * @return true if graph is created correctly
     */
    boolean createGraph();

    /**
     * Method to add all the keywords
     * @return true if keyword are added correctly
     */
    boolean addKeywords();

    /**
     * Get the ArrayList of vertexes associated to given keyword
     * @param keyword [String] keyword to find
     * @return the ArrayList of vertexes associated to given keyword if it exist, NULL otherwise
     */
    ArrayList<Vertex> getKeyword(String keyword);

    /**
     * Add a vertex to keywordmap
     * @param keyword [String] key for the vertex
     * @param v [Vertex] vertex to be added to $keywordMap
     */
    //void putKeyword(String keyword, Vertex v);

    Vertex getVertex(String label);

    HashMap<String, Vertex> getVertexMap();

}//interfaces::PopulationInterface

