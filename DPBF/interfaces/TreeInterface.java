/**
 * TreeInterface.java
 */

package interfaces;

import graph.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;


public interface TreeInterface{

    /**
     * It adds a vertex in the tree and it gives it the role of root
     * @param node = new root node
     * @param edge = it is the edge that links the parent with the child.
     * @param kw = it contains the keywords of the vertex to add.
     */
    void addRootNode(Node node, Edge edge, ArrayList<String> kw);

    /**
     * Version of the previously addRootNode with no update of keyword. The programmer must to verify the right version to use
     * @param node = new root node
     * @param edge = it is the edge that links the parent with the child.
     */
    void addRootNode(Node node, Edge edge);

    /**
     * It concatenates two trees.
     * @param tree = the tree to add.
     * @throws IllegalArgumentException = it is thrown only if the root of the second tree isn't equal to a node in the first tree.
     * This means that the two trees aren't compatible.
     */
    void concatenateTree(TreeInterface tree) throws IllegalArgumentException;

    /**
     * It returns the total weight of the tree
     * @return = total weight of the tree.
     */
    double getWeight();

    /**
     *
     * @param weight value of the weight to set
     */
     void setWeight(double weight);

    /**
     * It returns the keyword in the subgraph
     * @return the arrayList of keywords
     */
    HashSet<String> getKeywords();

    /**
     *
     * @return the number of nodes in this tree
     */
    int size();

    /**
     * It returns the node that has the role of root in the tree.
     * @return = the root node.
     */
    Node getRoot();

    /**
     * It returns true if the interception of the two keyword sets is empty.
     * @param tree = second tree.
     * @return
     */
    boolean hasEmptyKeywordInterception(TreeInterface tree);

    /**
     * It returns true if the two trees has the same keywords.
     * @param tree = second tree.
     * @return
     */
    boolean hasTheSameKeyword(TreeInterface tree);


    /**
     * It returns true if the tree contains the node.
     * @param node = node to check.
     * @return
     */
    boolean containsNode(Node node);

    /**
     * To add keyword to the tree
     * @param keyword = keyword to put in the tree
     */
    void addKeyword(String keyword);

    /**
     * To add a group of keywords to the tree
     * @param keywords = arraylist of keyword to put in the tree
     */
    void addKeyword(Collection<String> keywords);

    /**
     * It returns a node from the tree
     * @param label = it is the name of the tree node.
     * @return
     */
    Node getNode(String label);

    /**
     * It returns the hashmap that contains all the nodes of the tree.
     * @return
     */
    HashMap<String,Node> getTreeNode();

    /**
     *
     * @param nodeMap the HashMap of the nodes to add to the tree
     */
    void setTreeNode(HashMap<String,Node> nodeMap);

    /**
     *
     * @return a deep copy of the current object
     */
    TreeInterface copy();
}//interfaces::TreeInterface
