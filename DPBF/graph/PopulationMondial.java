/**
 * PopulationMondial.java
 */

package graph;

import interfaces.PopulationInterface;
import jdbc.DatabaseConnection;

import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by exto on 27/06/16.
 */
public class PopulationMondial implements PopulationInterface{
    // Constants
    private static final String DEFAULT_SERIALIZE = "mondial_keyword.ser";
    private static final String DEFAULT_GRAPH = "mondial_graph.ser";

    // Graph variables
    private Connection con;
    private HashMap< String, Vertex > vertexesMap;
    private HashMap< String, ArrayList<Vertex>> keywordMap;
    private String graphFile, serFile;
    private int edges;

    private RemoveString remover;

    private boolean isFirstTuple;
    private String[] colsName;



    /**
     * Constructor
     */
    public PopulationMondial(Connection con) {
        this(con, DEFAULT_GRAPH, DEFAULT_SERIALIZE);
    }//constructor

    /**
     * Constructor
     */
    public PopulationMondial(Connection con, String graphFile, String serFile) {
        this.con = con;
        this.vertexesMap = new HashMap<>();
        this.keywordMap = new HashMap<>();
        this.edges = 0;

        this.graphFile = graphFile;
        this.serFile = serFile;

        this.remover = new RemoveString();
    }//constructor


    /**
     * @see interfaces.PopulationInterface;
     */
    public boolean createGraph() {
        try {
            System.out.println("\nTrying to load serialized graph file ");
            loadGraph();
            System.out.println("Files loaded correctly!");
            System.out.println("Loaded " + vertexesMap.size() + " vertexes!");

            return true;

        } catch(IOException e) { // File does not exist - populate graph
            System.out.println("\nFile " + graphFile + " does not exist. Creating it...");

            // Get DB structure and datas
            String[][] tables = DatabaseConnection.getNumCols(con, "mondial");
            System.out.print("\n> [" + tables.length + "] tables found --- ");
            System.out.println("Tables [" + tables.length +"x"+tables[0].length + "]");
            for(int i=0; i<tables.length; i++){
                System.out.print(" -- ");
                for(int j=0; j<tables[i].length; j++){
                    System.out.print( tables[i][j] + "\t" );
                }//for
                System.out.print("\n");
            }//for

            addWithoutFKs(tables, false);
            addForeignKeys(tables, false);

            try {
                saveGraph();
            }catch (IOException exc){
                System.err.println("## Error saving " + graphFile + " --- Details: " + exc );
            }//try-catch

            try {
                saveKeyword();
            }catch (IOException exc){
                System.err.println("## Error saving " + serFile + " --- Details: " + exc );
            }//try-catch

            return true;
        }catch(ClassNotFoundException e){ } //try - catch - catch

        return false;
    }//createGraph

    public boolean addKeywords() {
        try {
            System.out.println("\nTrying to load serialized graph file ");
            if(keywordMap.size() == 0)
            {
                loadKeyword();
            }
            System.out.println("Files loaded correctly!");
            System.out.println("Loaded " + keywordMap.size() + " words!");
            return true;

        } catch(IOException e) { // File does not exist - populate graph
            System.out.println("\nFile " + serFile + " does not exist. Creating it...");

            // Get DB structure and datas
            String[][] tables = DatabaseConnection.getNumCols(con, "mondial");
            System.out.print("\n> [" + tables.length + "] tables found --- ");
            System.out.println("Tables [" + tables.length +"x"+tables[0].length + "]");
            for(int i=0; i<tables.length; i++){
                System.out.print(" -- ");
                for(int j=0; j<tables[i].length; j++){
                    System.out.print( tables[i][j] + "\t" );
                }//for
                System.out.print("\n");
            }//for

            addWithoutFKs(tables, true);

            try{
                remover = null;
                System.gc();

                saveKeyword();
            }catch (IOException exc){
                System.err.println("## Error saving " + serFile + " --- Details: \n" + exc );
            }//try-catch

            return true;
        }catch(ClassNotFoundException e){ } //try - catch - catch

        return false;
    }//createGraph

    /**
     * @see interfaces.PopulationInterface;
     */
    public ArrayList<Vertex> getKeyword(String keyword) {
        return keywordMap.get(keyword);
    }//getKeyword

    public Vertex getVertex(String label){
        return vertexesMap.get(label);
    }//getVertex

    public HashMap<String, Vertex> getVertexMap(){
        return vertexesMap;
    }//getVertexMap


    // ######### PRIVATE METHODS #########
    private void addWithoutFKs(String[][] tables, boolean onlyKeyword){
        System.out.println("\nReading node from mondial...");

        String result[][][] = new String[tables.length][][];
        for(int i = 0; i< tables.length; i++){
            result[i] = DatabaseConnection.getContentTable(con, tables[i][0], Integer.parseInt(tables[i][1]));
        }//for

        for (int i = 0; i < tables.length; i++) {

            isFirstTuple = true;

            if( tables[i][0].equals("sea") ||  tables[i][0].equals("mountain") ||  tables[i][0].equals("island") ||
                    tables[i][0].equals("desert") ||  tables[i][0].equals("lake") ||  tables[i][0].equals("continent") ){
                leafTables(result[i], tables[i][0], tables[i][1], onlyKeyword);

            }else if( tables[i][0].equals("country") ){
                country(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("city") ){
                city(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("province") ){
                province(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("language") || tables[i][0].equals("religion") || tables[i][0].equals("ethnic_group")){
                doubleKey(result[i], tables[i][0], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("economy") || tables[i][0].equals("politics") || tables[i][0].equals("population") ){
                singleKey(result[i], tables[i][0], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("geo_river") || tables[i][0].equals("geo_lake") || tables[i][0].equals("geo_desert") ||
                    tables[i][0].equals("geo_island") || tables[i][0].equals("geo_mountain") || tables[i][0].equals("geo_sea") ){
                geoTabs(result[i], tables[i][0], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("borders") ){
                borders(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("organization") ){
                organization(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("is_member") ){
                isMember(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("river") ){
                river(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("merges_with") ){
                merges_with(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("located") ){
                located(result[i], tables[i][1], false, onlyKeyword);

            }else if( tables[i][0].equals("encompasses") ){
                encompasses(result[i], tables[i][1], false, onlyKeyword);

            }// end-if

            result[i] = null;
        }//for_i
    }//addWithoutFKs

    private void addForeignKeys(String[][] tables, boolean onlyKeyword){
        System.out.println("\nAdding foreign keys...");

        for (int i = 0; i < tables.length; i++) {
            String[][] result = DatabaseConnection.getContentTable(con, tables[i][0], Integer.parseInt(tables[i][1]));

            String tab = tables[i][0];
            if( tab.equals("country") ){
                edges += country(result, tables[i][1], true, onlyKeyword);

            }else if( tab.equals("city") ){
                edges += city(result, tables[i][1], true, onlyKeyword);

            }else if( tab.equals("province") ){
                edges += province(result, tables[i][1], true, onlyKeyword);

            }else if( tab.equals("language") || tab.equals("religion") || tab.equals("ethnic_group")){
                edges += doubleKey(result, tab, tables[i][1], true, onlyKeyword);

            }else if( tab.equals("economy") || tab.equals("politics") || tab.equals("population") ){
                edges += singleKey(result, tab, tables[i][1], true, onlyKeyword);

            }else if( tab.equals("geo_river") || tab.equals("geo_lake") || tab.equals("geo_desert") ||
                    tab.equals("geo_island") || tab.equals("geo_mountain") || tab.equals("geo_sea") ){
                edges += geoTabs(result, tab, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("borders") ){
                edges += borders(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("organization") ){
                edges += organization(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("is_member") ){
                edges += isMember(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("river") ){
                edges += river(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("merges_with") ){
                edges += merges_with(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("located") ){
                edges += located(result, tables[i][1], true, onlyKeyword);

            }else if( tables[i][0].equals("encompasses") ){
                edges += encompasses(result, tables[i][1], true, onlyKeyword);

            }// end-if
        }//for_i
        System.out.println(">> Added " + edges + " edges.\n\n");
    }//addForeignKeys

    private void computeKeywords(Vertex v, String [] result, String tableName){
        String text = "";
        for(int i = 0; i < result.length; i++) {
            if( result[i] != null && !result[i].equals(null) ){
                text += result[i] + " ";
            }//if
        }//for_i

        if( isFirstTuple ) {
            isFirstTuple = false;
            colsName = DatabaseConnection.getColumnsNames(con, tableName);
        }//if
        text += tableName;
        for (int i = 0; i < colsName.length; i++) {
            text += " " + colsName[i];
        }//for_i

        String kw = remover.remove(text.toLowerCase());
        String[] kws = kw.toLowerCase().split(" ");

        for(int i = 0; i < kws.length; i++){
            ArrayList<Vertex> alv = keywordMap.get(kws[i]);
            if( alv == null ){
                alv = new ArrayList<>();
                alv.add(v);
                keywordMap.put(kws[i], alv);
            }//if
            else
            {
                alv.add(v);
            }
        }//for_z
    }//computeKeywords

    private void saveGraph() throws IOException {
        System.out.println( "Saving 'mondial' graph, number of vertex = " + vertexesMap.size() );
        System.out.print("\n> 'Mondial' graph serialization... ");

        File file = new File(graphFile);
        FileOutputStream f = new FileOutputStream(file);
        ObjectOutputStream s = new ObjectOutputStream(f);
        s.writeObject(vertexesMap);

        s.close();
        f.close();

        System.out.println("Done!");
    }//saveGraph

    private void saveKeyword() throws IOException{
        System.out.println("> 'Mondial' hashMap serialization... ");
        System.out.println("> Number of keywords to save " + keywordMap.size() );

        File file = new File(serFile);
        FileOutputStream f = new FileOutputStream(file);
        ObjectOutputStream s = new ObjectOutputStream(f);
        s.writeObject(keywordMap);

        s.close();
        f.close();

        System.out.println("Done!");
    }//saveKeyword

    private void loadGraph() throws IOException, ClassNotFoundException{
        System.out.print("> Mondial graph loading... ");
        File file = new File(graphFile);
        FileInputStream f = new FileInputStream(file);
        ObjectInputStream s = new ObjectInputStream(f);
        vertexesMap = (HashMap<String, Vertex>) s.readObject();
        s.close();
    }//loadGraph

    private void loadKeyword() throws IOException, ClassNotFoundException {
        System.out.print("> Keyword loading... ");
        File file = new File(DEFAULT_SERIALIZE);
        FileInputStream f = new FileInputStream(file);
        ObjectInputStream s = new ObjectInputStream(f);
        keywordMap = (HashMap<String, ArrayList<Vertex>>) s.readObject();
        s.close();
    }//loadKeyword



    // ######### METHODS TO SCAN DATABASE #########
    /**
     * sea, mountain, island, desert, lake, continent
     */
    private void leafTables(String[][] result, String tab, String size, boolean onlyKeyword){
        long time = System.currentTimeMillis();
        System.out.println(">> Table: '" + tab + "' of size (" + size + ")");

        int search_id_index = result[0].length - 1;
        for(int i = 0; i < result.length; i++){
            printProgress(tab, result, i, 100);
            Vertex v;
            String label = tab + ":" + result[i][0];
            if(!onlyKeyword) {
                v = new Vertex(label, result[i][search_id_index]);
                vertexesMap.put(v.getLabel(), v);
            } else {
                v = vertexesMap.get(label);
            }//if - else
            computeKeywords(v, result[i],tab);
            result[i] = null;
        }//for_i

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>>Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
    }//leafTables

    /**
     * language, religion, etnich_group
     */
    private int doubleKey(String[][] result, String tab, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: '" + tab + "' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 50);
                Vertex v;
                String label = tab + ":" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], tab);
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 100);

                Vertex from, to;
                String label = tab + ":" + result[i][0] + ":" + result[i][1];

                if(result[i][0] != null){
                    from = vertexesMap.get(label);
                    to = vertexesMap.get("country:" + result[i][0]);

                    Edge e = new Edge(from, to);
                    from.addNeighbor(e);
                    to.addNeighbor(e);

                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");

        return edges;
    }//leafTables

    /**
     * population, economy, politics
     */
    private int singleKey(String[][] result, String tab, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: '" + tab + "' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 50);
                Vertex v;
                String label = tab + ":" + result[i][0];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], tab);
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 100);

                Vertex from, to;

                int search_id_index = result[0].length - 1;
                String label = tab + ":" + result[i][0];
                from = vertexesMap.get(label);

                if(result[i][0] != null) {
                    to = vertexesMap.get("country:" + result[i][0]);
                    Edge e = new Edge(from, to);
                    from.addNeighbor(e);
                    to.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//leafTables

    private int geoTabs(String[][] result, String tab, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: '" + tab + "' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 50);
                Vertex v;
                String label = tab + ":" + result[i][0] + ":" + result[i][1] + ":" + result[i][2];

                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], tab);
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress(tab, result, i, 100);

                Vertex from, province, geo;
                String label = tab + ":" + result[i][0] + ":" + result[i][1] + ":" + result[i][2];
                from = vertexesMap.get(label);

                if(result[i][2] != null && result[i][1] != null) {
                    province = vertexesMap.get("province:" + result[i][2] + ":" + result[i][1]);
                    Edge e = new Edge(from, province);
                    from.addNeighbor(e);
                    province.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][0] != null) {
                    geo = vertexesMap.get(tab.substring(4) + ":" + result[i][0]);
                    Edge e = new Edge(from, geo);
                    from.addNeighbor(e);
                    geo.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//getTabs

    private int country(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'country' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("country", result, i, 100);
                Vertex v;
                if(!onlyKeyword) {
                    v = new Vertex("country:" + result[i][1], result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get("country:" + result[i][1]);
                }//if - else
                computeKeywords(v, result[i], "country");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("country", result, i, 100);

                if( result[i][1] != null && result[i][0] != null && result[i][2] != null) {
                    Vertex country, city;
                    country = vertexesMap.get("country:" + result[i][1]);
                    city = vertexesMap.get("city:" + result[i][2] + ":" + result[i][1] + ":" + result[i][3]);   // Vertex with primary key

                    Edge e = new Edge(country, city);  // coutry(code, capital, province) => city(country, name, province)
                    country.addNeighbor(e);
                    city.addNeighbor(e);
.
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");

        return edges;
    }//country

    private int city(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'city' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("city", result, i, 100);
                Vertex v;
                String label = "city:" + result[i][0] + ":" + result[i][1] + ":" + result[i][2];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "city");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("city", result, i, 100);

                if(result[i][2] != null && result[i][1] != null) {
                    Vertex city, province;
                    String label = "city:" + result[i][0] + ":" + result[i][1] + ":" + result[i][2];

                    city = vertexesMap.get(label);
                    province = vertexesMap.get("province:" + result[i][2] + ":" + result[i][1]);

                    Edge e = new Edge(city, province);
                    city.addNeighbor(e);
                    province.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");

        return edges;
    }//city

    private int province(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'province' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("province", result, i, 100);
                Vertex v;
                String label = "province:" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "province");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("province", result, i, 100);

                Vertex province, city, country;
                String label = "province:" + result[i][0] + ":" + result[i][1];
                province = vertexesMap.get(label);

                if( result[i][4] != null && result[i][1] != null && result[i][5] != null) {
                    city = vertexesMap.get("city:" + result[i][4] + ":" + result[i][1] + ":" + result[i][5]);
                    Edge e = new Edge(province, city);
                    province.addNeighbor(e);
                    city.addNeighbor(e);
                    edges++;
                }//if

                if( result[i][1] != null ) {
                    country = vertexesMap.get("country:" + result[i][1]);
                    Edge e = new Edge(province, country);
                    province.addNeighbor(e);
                    country.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//province

    private int merges_with(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'merges_with' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("merges_with", result, i, 10);
                Vertex v;
                String label = "merges_with:" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "merges_with");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("merges_with", result, i, 10);

                Vertex merge, sea1, sea2;
                String label = "merges_with:" + result[i][0] + ":" + result[i][1];

                merge = vertexesMap.get(label);
                sea1 = vertexesMap.get("sea:" + result[i][0]);
                sea2 = vertexesMap.get("sea:" + result[i][1]);

                Edge e = new Edge(merge, sea1);
                merge.addNeighbor(e);
                sea1.addNeighbor(e);

                e = new Edge(merge, sea2);
                merge.addNeighbor(e);
                sea2.addNeighbor(e);

                edges += 2;
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//merged_with

    private int encompasses(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'encompasses' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("encompasses", result, i, 100);
                Vertex v;
                String label = "encompasses:" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword) {
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "encompasses");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("encompasses", result, i, 100);

                Vertex encom, country, continent;
                String label = "encompasses:" + result[i][0] + ":" + result[i][1];

                encom = vertexesMap.get(label);
                country = vertexesMap.get("country:" + result[i][0]);
                continent = vertexesMap.get("continent:" + result[i][1]);

                Edge e = new Edge(encom, country);
                encom.addNeighbor(e);
                country.addNeighbor(e);

                e = new Edge(encom, continent);
                encom.addNeighbor(e);
                continent.addNeighbor(e);

                edges+= 2;
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//encompasses

    private int organization(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'organization' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("organization", result, i, 100);
                Vertex v;
                String label = "organization:" + result[i][0];
                if(!onlyKeyword){
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "organization");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("organization", result, i, 100);

                Vertex org, city;
                String label = "organization:" + result[i][0];
                org = vertexesMap.get(label);

                if(result[i][2] != null && result[i][3] != null && result[i][4] != null) {
                    city = vertexesMap.get("city:" + result[i][2] + ":" + result[i][3] + ":" + result[i][4]);
                    Edge e = new Edge(org, city);
                    org.addNeighbor(e);
                    city.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print(">> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//organization

    private int isMember(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'is_member' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("is_member", result, i, 100);
                Vertex v;
                String label = "is_member:" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword){
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "is_member");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("is_member", result, i, 100);

                Vertex member, country, org;
                String label = "is_member:" + result[i][0] + ":" + result[i][1];
                member = vertexesMap.get(label);

                if(result[i][1] != null) {
                    org = vertexesMap.get("organization:" + result[i][1]);
                    Edge e = new Edge(member, org);
                    member.addNeighbor(e);
                    org.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][0] != null) {
                    country = vertexesMap.get("country:" + result[i][0]);
                    Edge e = new Edge(member, country);
                    member.addNeighbor(e);
                    country.addNeighbor(e);
                    edges++;
                }//if
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//isMember

    private int borders(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'borders' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("borders", result, i, 100);
                Vertex v;
                String label = "borders:" + result[i][0] + ":" + result[i][1];
                if(!onlyKeyword){
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "borders");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("borders", result, i, 100);

                Vertex borders, c1, c2;
                String label = "borders:" + result[i][0] + ":" + result[i][1];

                borders = vertexesMap.get(label);
                c1 = vertexesMap.get("country:" + result[i][0]);
                c2 = vertexesMap.get("country:" + result[i][1]);

                Edge e = new Edge(borders, c1);
                borders.addNeighbor(e);
                c1.addNeighbor(e);

                e = new Edge(borders, c2);
                borders.addNeighbor(e);
                c2.addNeighbor(e);

                edges+= 2;
            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//borders

    private int river(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'river' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("river", result, i, 50);
                Vertex v;
                String label = "river:" + result[i][0];
                if(!onlyKeyword){
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "river");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("river", result, i, 50);

                Vertex river, lake, river1, sea;
                String label = "river:" + result[i][0];
                river = vertexesMap.get(label);

                if(result[i][2] != null){
                    lake = vertexesMap.get("lake:" + result[i][2]);
                    Edge e = new Edge(river, lake);
                    river.addNeighbor(e);
                    lake.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][1] != null) {
                    river1 = vertexesMap.get("river:" + result[i][1]);
                    Edge e = new Edge(river, river1);
                    river.addNeighbor(e);
                    river1.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][3] != null) {
                    sea = vertexesMap.get("sea:" + result[i][3]);
                    Edge e = new Edge(river, sea);
                    river.addNeighbor(e);
                    sea.addNeighbor(e);
                    edges++;
                }//if

            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//river

    private int located(String[][] result, String size, boolean isFK, boolean onlyKeyword){
        int edges = 0;
        long time = System.currentTimeMillis();
        System.out.println(">> Table: 'located' of size (" + size + ")");

        if(!isFK){
            int search_id_index = result[0].length - 1;
            for(int i = 0; i < result.length; i++){
                printProgress("located", result, i, 50);
                Vertex v;
                String label = "located:" + result[i][search_id_index];
                if(!onlyKeyword){
                    v = new Vertex(label, result[i][search_id_index]);
                    vertexesMap.put(v.getLabel(), v);
                } else {
                    v = vertexesMap.get(label);
                }//if - else
                computeKeywords(v, result[i], "located");
                result[i] = null;
            }//for_i

        }else{
            for(int i = 0; i < result.length; i++){
                printProgress("located", result, i, 50);

                Vertex located, city, lake, river, sea;
                String label = "located:" + result[i][6];

                located = vertexesMap.get(label);

                if(result[i][0] != null && result[i][1] != null && result[i][2] != null) {
                    city = vertexesMap.get("city:" + result[i][0] + ":" + result[i][2] + ":" + result[i][1]);
                    Edge e = new Edge(located, city);
                    located.addNeighbor(e);
                    city.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][3] != null) {
                    river = vertexesMap.get("river:" + result[i][3]);
                    Edge e = new Edge(located, river);
                    located.addNeighbor(e);
                    river.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][4] != null) {
                    lake = vertexesMap.get("lake:" + result[i][4]);
                    Edge e = new Edge(located, lake);
                    located.addNeighbor(e);
                    lake.addNeighbor(e);
                    edges++;
                }//if

                if(result[i][5] != null) {
                    sea = vertexesMap.get("sea:" + result[i][5]);
                    Edge e = new Edge(located, sea);
                    located.addNeighbor(e);
                    sea.addNeighbor(e);
                    edges++;
                }//if

            }//for_i
        }// if - else

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.print("\t>> Time: " + time + " sec");
        System.out.println("\tAdded " + result.length + " vertexes.\n");
        return edges;
    }//located

    private void printProgress(String tab, String[][] result, int prog, int step){
        if( prog%step == 0) {
            System.out.println("* '" + tab + "' computing row " + prog + " out of " + result.length );
        }//if
    }//printProgress

}//class::graph::PopulationMondial
