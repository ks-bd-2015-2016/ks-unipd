package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by luca on 07/06/16.
 */
public class RemoveString{
    private static Set<String> wordsToRemove;

    public RemoveString(){
        wordsToRemove = new HashSet<String>();
        insertElements("/graph/words/PunctuationToDelete.txt", " ");
        insertElements("/graph/words/AllWords.txt", ",");
    }//constructor

    private void insertElements(String fileName, String delimiter)
    {
        Scanner in;
        FileReader input;
        try
        {
            String filePath = new File("").getAbsolutePath();
            input = new FileReader(filePath+fileName);
            in = new Scanner(input);

            while(in.hasNextLine())
            {
                String line = in.nextLine();
                Scanner token = new Scanner(line);
                token.useDelimiter(delimiter);
                while(token.hasNext())
                {
                    wordsToRemove.add(token.next().trim());
                }
                token.close();
            }

            input.close();
            in.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
        catch (IOException e)
        {
            e.printStackTrace();

        }finally {
            input = null;
            in = null;
            System.gc();
        }
    }

    public String remove (String text)
    {

        // this code will run in a loop reading one line after another from the file
        StringBuffer outputLine = new StringBuffer();
        Scanner target = new Scanner(text);
        target.useDelimiter("[\\[\\]\"\'\\-/,.?!#$%@^&*(){};:-=+<> ]+");
        while(target.hasNext())
        {
            String word = target.next();
            if (!wordsToRemove.contains(word) && word.length() > 2) {
                if (outputLine.length() > 0 ) {
                    outputLine.append(' ');
                }
                if(!wordsToRemove.contains(word.substring(word.length()-1)))
                    outputLine.append(word);
                else
                    outputLine.append(word.substring(0,word.length()-1));
            }
        }

        return outputLine.toString();
    }
}
