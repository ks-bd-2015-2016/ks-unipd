package graph;

import java.util.HashSet;

public class Node implements Cloneable
{
    private String label, searchId;
    private String parentLabel;
    private HashSet<String> childrenLabel;

    public Node(String label, String searchId)
    {
        this.label = label;
        this.searchId = searchId;
        parentLabel = "";
        childrenLabel = new HashSet<>();
    }

    public void insertParent(String label)
    {
        parentLabel = label;
    }

    public void insertChildren(String label)
    {
        childrenLabel.add(label);
    }

    public void insertChildren(HashSet<String> labels)
    {
        childrenLabel.addAll(labels);
    }

    public String getParent()
    {
        return parentLabel;
    }

    public HashSet<String> getChildren()
    {
        return childrenLabel;
    }

    public String getLabel()
    {
        return label;
    }

    public void removeParent()
    {
        parentLabel = null;
    }


    public Node copy()
    {
        Node newNode = new Node(this.label, this.searchId);
        newNode.insertParent(this.parentLabel);
        newNode.insertChildren((HashSet<String>) this.childrenLabel.clone());
        return newNode;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Node)) {
            return false;
        }

        Node t = (Node) other;
        boolean first = label.equals(t.getLabel());
        boolean second = parentLabel.equals(t.getParent());
        boolean third = childrenLabel.equals(t.getChildren());
        return ( first && second && third);

    }

    /**
     * @return String A String representation of this Tree
     */
    public String toString()
    {
        if(!parentLabel.equals(""))
        {
            return "Node Label: " + label + " Node search id: " + searchId + " Parent label: " + parentLabel;
        }
        else
        {
            return "Node Label: " + label + " Node search id: " + searchId;
        }
    }
}