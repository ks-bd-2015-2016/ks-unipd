package graph;

import java.io.Serializable;
import java.util.ArrayList;

public class Vertex implements Serializable{
    private static final long serialVersionUID = 2743249886986442906L;
    private ArrayList<Edge> neighborhood;
    private String label, searchId;
    private double weight;

    /**
     * @param label The unique label associated with this Vertex
     */
    public Vertex(String label, String searchId) {
        this(label, searchId, 0);
    }

    /**
     * @param label The unique label associated with this Vertex
     * @param weight Weigth assosciated to vertex or to subgraph
     */
    public Vertex(String label, String searchId, double weight){
        this.label = label;
        this.searchId = searchId;
        this.neighborhood = new ArrayList<Edge>();
        this.weight = weight;
    }//costruttore


    /**
     * This method adds an Edge to the incidence neighborhood of this graph iff the edge is not
     * already present.
     *
     * @param edge The edge to add
     */
    public void addNeighbor(Edge edge) {
        if (this.neighborhood.contains(edge)) {
            return;
        }
        this.neighborhood.add(edge);
    }

    /**
     * @param other The edge for which to search
     * @return true iff other is contained in this.neighborhood
     */
    public boolean containsNeighbor(Edge other) {
        return this.neighborhood.contains(other);
    }

    /**
     * @param index The index of the Edge to retrieve
     * @return Edge The Edge at the specified index in this.neighborhood
     */
    public Edge getNeighbor(int index) {
        return this.neighborhood.get(index);
    }

    /**
     * @param index The index of the edge to remove from this.neighborhood
     * @return Edge The removed Edge
     */
    public Edge removeNeighbor(int index) {
        return this.neighborhood.remove(index);
    }

    /**
     * @param e The Edge to remove from this.neighborhood
     */
    public void removeNeighbor(Edge e) {
        this.neighborhood.remove(e);
    }

    /**
     * remove all the neighbors
     */
    public void removeAllNeighbors()
    {
        this.neighborhood.clear();
    }

    /**
     * @return int The number of neighbors of this Vertex
     */
    public int getNeighborCount() {
        return this.neighborhood.size();
    }

    /**
     * @return String The label of this Vertex
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * @return String A String representation of this Vertex
     */
    public String toString() {
        return "Vertex name:" + label + " Neighbor: " + neighborhood;
    }

    /**
     * @return The hash code of this Vertex's label
     */
    public int hashCode() {
        return this.label.hashCode();
    }

    /**
     * @param other The object to compare
     * @return true iff other instanceof Vertex and the two Vertex objects have the same label
     */
    public boolean equals(Object other) {
        if (!(other instanceof Vertex)) {
            return false;
        }

        Vertex v = (Vertex) other;
        return this.label.equals(v.label);
    }

    /**
     * @return ArrayList<Edge> A copy of this.neighborhood. Modifying the returned ArrayList will
     * not affect the neighborhood of this Vertex
     */
    public ArrayList<Edge> getNeighbors() {
        return new ArrayList<Edge>(this.neighborhood);
    }

    /**
     *
     * @param tabName The name of the table to create
     * @return The string that rapresent the query
     */
    public String getInsertQuery(String tabName) {
        return "INSERT INTO " + tabName + " (" + label + ") VALUES " + label + ";";
    }//getInsertQuery

    /**
     *
     * @return the weight of this vertex
     */
    public double getWeight(){
        return this.weight;
    }//getWeight

    /**
     *
     * @param weight the weight this vertex will have
     */
    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    /**
     * This methods returns the tuple searchId values
     * @return __search_id values
     */
    public String getSearchId(){
        return searchId;
    }//getSearchId

}//class::Vertex
