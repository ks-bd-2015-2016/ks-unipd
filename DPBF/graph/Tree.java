package graph;

import interfaces.*;

import java.util.*;

public class Tree implements TreeInterface
{
    private static int count = 0;
    private int id;

    private double weight;
    private HashSet<String> keywords;
    private HashMap<String, Node> nodes;
    private String rootLabel;

    public Tree(Node node, HashSet<String> keyword)
    {
        nodes = new HashMap<>();
        keywords = new HashSet<>();
        if (keyword != null)
        {
            keywords.addAll(keyword);
        }

        weight = 0;

        nodes.put(node.getLabel(), node);
        rootLabel = node.getLabel();

        id = count++;
    }


    public Node getNode(String label)
    {
        return nodes.get(label).copy();
    }

    public double getWeight()
    {
        return weight;
    }

    public int size()
    {
        return nodes.size();
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    public void addRootNode(Node node, Edge edge, ArrayList<String> kw) throws IllegalArgumentException
    {
        if (!((edge.getFrom().equals(getRoot().getLabel()) || edge.getTo().equals(getRoot().getLabel())) && (edge.getTo().equals(node.getLabel()) || edge.getFrom().equals(node.getLabel()))))
        {
            throw new IllegalArgumentException();
        }

        node.insertChildren(getRoot().getLabel());

        //It insert the new node as the root
        Node oldRoot = nodes.get(rootLabel);
        //It adds the link from the old root to the new one
        oldRoot.insertParent(node.getLabel());  

        //It insert the new node in the tree
        rootLabel = node.getLabel();
        nodes.put(rootLabel, node);

        //It updates the total weight
        weight = weight + edge.getWeight();

        //It updates the keywords
        for (String k : kw)
        {
            this.keywords.add(k);
        }

    }

    public void addRootNode(Node node, Edge edge) throws IllegalArgumentException
    {
        if (!((edge.getFrom().equals(getRoot().getLabel()) || edge.getTo().equals(getRoot().getLabel())) && (edge.getTo().equals(node.getLabel()) || edge.getFrom().equals(node.getLabel()))))
        {
            throw new IllegalArgumentException();
        }


        node.insertChildren(getRoot().getLabel());

        //It insert the new node as the root
        Node oldRoot = nodes.get(rootLabel); 
        //It adds the link from the old root to the new one
        oldRoot.insertParent(node.getLabel());  

        //It insert the new node in the tree
        rootLabel = node.getLabel();
        nodes.put(rootLabel, node);

        //It updates the total weight
        weight = weight + edge.getWeight();

    }

    public void concatenateTree(TreeInterface tree) throws IllegalArgumentException
    {
        if (!containsNode(tree.getRoot()))
        {
            throw new IllegalArgumentException();
        }

        // It finds the connection point in the tree
        Node connection = nodes.get(tree.getRoot().getLabel()); 
        // It fixed in a node the root of the second tree
        Node rootOfTree = tree.getNode(tree.getRoot().getLabel()); 

        // It inserts all the nodes of the second tree in the hash map of this tree
        nodes.putAll(tree.getTreeNode()); 
        // It extracts the children name of the second tree root
        HashSet<String> children = rootOfTree.getChildren(); 
        // It inserts the children straight out of the list
        connection.insertChildren(children); 

        nodes.replace(rootLabel,connection);

        weight += tree.getWeight();
        //It upgrades the keywords
        keywords.addAll(tree.getKeywords());
    }

    public boolean containsNode(Node node)
    {
        return nodes.containsKey(node.getLabel());
    }

    public boolean hasEmptyKeywordInterception(TreeInterface tree)
    {
        for (String kw : tree.getKeywords())
        {
            if (keywords.contains(kw))
                return false;
        }
        return true;
    }

    public boolean hasTheSameKeyword(TreeInterface tree)
    {
        if (tree.getKeywords().size() != keywords.size())
        {
            return false;
        }
        for (String kw : tree.getKeywords())
        {
            if (!keywords.contains(kw))
                return false;
        }
        return true;
    }

    public HashSet<String> getKeywords()
    {
        return keywords;
    }

    public Node getRoot()
    {

        return nodes.get(rootLabel).copy();

    }


    public boolean equals(Object other)
    {
        if (!(other instanceof Tree))
        {
            return false;
        }

        Tree t = (Tree) other;

        Double temp1 = new Double(t.getWeight() * 10000);
        Double temp2 = new Double(this.weight * 10000);

        int int1 = temp1.intValue();
        int int2 = temp2.intValue();

        // return comparing root, nodes, keywords and weights
        boolean first = this.getRoot().getLabel().equals(t.getRoot().getLabel());
        boolean second = this.keywords.equals(t.getKeywords());
        boolean third = this.nodes.equals(getTreeNode()) && int1 == int2;
        return (first && second && third);
    }

    public void addKeyword(String kw)
    {
        keywords.add(kw);
    }

    public void addKeyword(Collection<String> kw)
    {
        keywords.addAll(kw);
    }

    public HashMap<String, Node> getTreeNode()
    {
        return (HashMap<String, Node>) nodes.clone();
    }

    public void setTreeNode(HashMap<String, Node> nodeMap)
    {
        this.nodes.putAll(nodeMap);
    }

    /**
     * @return String A String representation of this Tree
     */
    public String toString()
    {
        String result = "Tree of size " + size() + ", with weight " + weight + ", keywords " + getKeywords() + ": \n";
        LinkedList<String> firstQueue = new LinkedList<>();
        LinkedList<String> secondQueue = new LinkedList<>();
        HashSet<String> children = getNode(rootLabel).getChildren();
        result += "Root node : " + getNode(rootLabel) + ";\n";
        firstQueue.addAll(children);
        while (firstQueue.size() > 0 || secondQueue.size() > 0)
        {

            while (firstQueue.size() > 0)
            {
                Node node = getNode(firstQueue.pop());
                result += node;
                if(firstQueue.size() > 0)
                {
                    result +=", ";
                }
                secondQueue.addAll(node.getChildren());
            }
            result += ";\n";
            while (secondQueue.size() > 0)
            {
                Node node = getNode(secondQueue.pop());
                result += node;
                if(secondQueue.size() > 0)
                {
                    result +=", ";
                }
                firstQueue.addAll(node.getChildren());
            }
            if(firstQueue.size()>0)
            {
                result += ";\n";
            }
        }
        return result + ".\n";
    }

    private void addNode(Node n,String key)
    {
        nodes.put(key,n.copy());
    }

    public Tree copy()
    {

        Tree tree = new Tree(this.getRoot().copy(), (HashSet<String>) this.keywords.clone());
        tree.setWeight(this.weight);

        HashMap<String, Node> selects = this.nodes;

        for(Map.Entry<String, Node> entry : selects.entrySet()) {
            String key = entry.getKey();
            Node value = entry.getValue();
            tree.addNode(value,key);
        }
        return tree;

    }
}