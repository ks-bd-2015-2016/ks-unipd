package graph;

import interfaces.PopulationInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TestSolution{
    private HashMap<Integer, Vertex> graph;

    public TestSolution(PopulationInterface database){
        HashMap<String, Vertex> dataset = database.getVertexMap();
        graph = new HashMap<>();

        for(String key : dataset.keySet()){
            Vertex v = dataset.get(key);
            graph.put( Integer.parseInt( v.getSearchId() ), v );

        }//for
    }//TestSolution

    public double compute(){
        Scanner in = new Scanner(System.in);
        String line;
        double treeWeigth = 0;

        System.out.println("Method to compute given solution graph weigth...\n");
        while( true ){
            System.out.println("Weight: " + treeWeigth);
            System.out.print("Insert search_id arch (exit to quit, q to change graph, n to continue): ");
            line = in.nextLine();

            if( line.equals("q") ) {
                return treeWeigth;

            }else if( line.equals("exit") ){
                return -1.0;

            }else if(line.equals("n")){
                Vertex from = null;
                try {
                    System.out.print("From: ");
                    from = graph.get(Integer.parseInt(in.nextLine()));
                    if (from == null) {
                        System.out.print(">> Null pointer!\n\n");
                        continue;
                    }//if
                }catch(Exception e){
                    System.out.print("Error adding search_id");
                    continue;
                }//try - catch

                String to;
                try {
                    System.out.print("To: ");
                    to = graph.get(Integer.parseInt(in.nextLine())).getLabel();
                    if( to.equals(from.getLabel()) ){
                        continue;
                    }//if
                }catch(Exception e){
                    System.out.print("Error adding search_id");
                    continue;
                }//if - else

                for( Edge n : from.getNeighbors() ){
                    if( n.getFrom().equals(to) || n.getTo().equals(to) ){
                        treeWeigth += n.getWeight();
                        break;
                    }//if
                }//for
            }//if-else
        }//while
    }//compute

}//class.testSolution
