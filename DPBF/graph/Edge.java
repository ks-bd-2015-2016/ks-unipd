package graph;

import java.io.Serializable;

public class Edge implements Comparable<Edge>,Serializable {
    // Leave this constant if u want to correctly import graph.ser
    private static final long serialVersionUID = -4581891385444130892L;

    private String from,to;
    private double weight;

    /**
     *
     * @param from The first vertex in the Edge
     * @param to The second vertex in the Edge
     */
    public Edge(Vertex from, Vertex to){
        this(from, to, 1);
    }

    /**
     *
     * @param from The first vertex in the Edge
     * @param to The second vertex of the Edge
     * @param weight The weight of this Edge
     */
    public Edge(Vertex from, Vertex to, double weight) {


        this.from = from.getLabel();
        this.to = to.getLabel();
        this.weight = weight;
    }

    /**
     *
     * @param to The second vertex of the Edge
     */
    public Edge(Vertex to){
        this.from = null;
        this.to = to.getLabel();
        this.weight = 0;
    }//constructor


    /**
     *
     * @param current the Vertex of which you want the neighbor
     * @return The label of the neighbor of current along this Edge
     */
    public String getNeighbor(Vertex current){
        if(!(current.getLabel().equals(from))){
            return null;
        }

        return to;
    }

    /**
     *
     * @return Vertex this.from
     */
    public String getFrom(){
        return this.from;
    }

    /**
     *
     * @return Vertex this.to
     */
    public String getTo(){
        return this.to;
    }

    /**
     *
     * @return double The weight of this Edge
     */
    public double getWeight(){
        return this.weight;
    }

    /**
     *
     * @param weight The new weight of this Edge
     */
    public void setWeight(double weight){
        this.weight = weight;
    }

    /**
     *
     * @param other The Edge to compare against this
     * @return double this.weight - other.weight
     */
    //public double compareTo(Edge other){
    public int compareTo(Edge other){
        return (int)(this.weight - other.weight);
    }

    /**
     *
     * @return String A String representation of this Edge
     */
    public String toString(){
        return "({" + from + ", " + to + "}, " + weight + ")";
    }

    /**
     *
     * @return int The hash code for this Edge
     */
    public int hashCode(){
        return (from + to).hashCode();
    }

    /**
     *
     * @param other The Object to compare against this
     * @return ture iff other is an Edge with the same Vertices as this
     */
    public boolean equals(Object other){
        if(!(other instanceof Edge)){
            return false;
        }

        Edge e = (Edge)other;

        return e.from.equals(this.from) && e.to.equals(this.to);
    }

}
