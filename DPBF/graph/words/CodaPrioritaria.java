package graph.words;

import java.util.ArrayList;

import interfaces.TreeInterface;

/**
 * Created by lamertex on 26/06/16.
 */
public class CodaPrioritaria
{
    TreeInterface[] coda;
    ArrayList<TreeInterface>[] otherSize;
    int ultimoEl, numberOfKeyword;

    public CodaPrioritaria()
    {
        coda = new TreeInterface[1000];
        ultimoEl = 0;
        this.numberOfKeyword = 1;
    }

    public CodaPrioritaria(int numberOfKeyword)
    {
        coda = new TreeInterface[1000];
        ultimoEl = 0;
        this.numberOfKeyword = numberOfKeyword;
    }


    public void remove(TreeInterface tree)
    {
        int i = 0;
        while(i<ultimoEl && !tree.equals(coda[i++]));
        while(i<ultimoEl-1)
        {
            coda[i]=coda[i+1];
            i++;
        }
        if(i<ultimoEl-1 && ultimoEl>0)
        {
            ultimoEl--;
        }
        checkSize();

    }

    public void add(TreeInterface tree)
    {
        checkSize();
        int lo = 0;
        int hi = ultimoEl - 1;
        int mid = lo + (hi - lo) / 2;
        boolean found = false;

        while (lo <= hi && !found) {
            // Key is in a[lo..hi] or not present.
            mid = lo + (hi - lo) / 2;
            if      (tree.getWeight() > coda[mid].getWeight()) hi = mid - 1;
            else if (tree.getWeight() < coda[mid].getWeight()) lo = ++mid;
            else found = true;
        }

        while(coda[mid] != null && (int)(tree.getWeight()*10000) == (int)(coda[mid].getWeight()*10000) && tree.getKeywords().size() > coda[mid].getKeywords().size())
        {
            mid++;
        }

        int i = ultimoEl++;
        while(i>mid)
        {
            coda[i]=coda[i-1];
            i--;
        }
        coda[mid]=tree;
    }

    public void addOnTop(TreeInterface tree)
    {
        checkSize();
        coda[ultimoEl++] = tree;
    }

    public void addFirstTime(ArrayList<TreeInterface> tree)
    {
        for(TreeInterface t : tree)
        {
            if(t.getKeywords().size() == 1)
            {
                addOnTop(t);
            }
            else
            {
                otherSize[t.getKeywords().size()-1].add(t);
            }
        }

    }

    public void finalizeAddFirstTime()
    {
        for(int i=1;i<otherSize.length;i++)
        {
            for(TreeInterface t : otherSize[i])
            {
                addOnTop(t);
            }
        }
        otherSize = null;
    }

    public TreeInterface poll()
    {
        return coda[--ultimoEl];
    }

    public TreeInterface peak()
    {
        return coda[ultimoEl-1];
    }

    public TreeInterface get(int i)
    {
        if(i<0 || i>=ultimoEl)
        {
            return null;
        }
        return coda[i];
    }

    public boolean isEmpty()
    {
        return ultimoEl==0;
    }

    public int size()
    {
        return ultimoEl;
    }

    public void setNumberOfKeyword(int n)
    {
        numberOfKeyword = n;
        otherSize = new ArrayList[numberOfKeyword];
        for(int i = 0; i < otherSize.length; i++)
        {
            otherSize[i] = new ArrayList<>();
        }
    }

    private void checkSize()
    {
        if(ultimoEl>=coda.length-2)
        {
            TreeInterface[] nuovaCoda = new TreeInterface[coda.length*2];
            for(int i=0;i<ultimoEl;nuovaCoda[i]=coda[i++]);
            coda=nuovaCoda;
        }
        else if(ultimoEl<coda.length/3 && coda.length>1000)
        {
            TreeInterface[] nuovaCoda = new TreeInterface[coda.length/2];
            for(int i=0;i<ultimoEl;nuovaCoda[i]=coda[i++]);
            coda=nuovaCoda;
        }
    }
}
