/**
 * PopulationIMDB.java
 */

package graph;

import interfaces.PopulationInterface;
import jdbc.DatabaseConnection;

import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;


public class PopulationIMDB implements PopulationInterface
{
    // Constants
    private static final String DEFAULT_SERIALIZE = "keyword.ser";
    private static final String DEFAULT_GRAPH = "graph.ser";
    private static final int DEFAULT_N_THREAD = 4;

    // Graph variables
    private Connection con;
    private HashMap<String, Vertex> vertexesMap;
    private HashMap<String, ArrayList<Vertex>> keywordMap;
    private String graphFile, serFile;
    private int nThread, edges;

    private RemoveString remover;
    private boolean isFirst;
    private String[] colsName;

    // Sherable variable to allow multithreading
    private String[][] castInfoResult;
    private String table_name;
    private int s_index, s_numThread;
    private boolean onlyKeywordThread;
    // ---------

    /**
     * Constructor
     */
    public PopulationIMDB(Connection con)
    {
        this(con, DEFAULT_GRAPH, DEFAULT_SERIALIZE, DEFAULT_N_THREAD);
    }//constructor

    /**
     * Constructor
     */
    public PopulationIMDB(Connection con, String graphFile, String serFile, int nThread)
    {
        this.con = con;
        this.vertexesMap = new HashMap<>();
        this.keywordMap = new HashMap<>();
        this.edges = 0;

        this.graphFile = graphFile;
        this.serFile = serFile;
        this.nThread = nThread;

        this.remover = new RemoveString();
    }//constructor

    /**
     * @see interfaces.PopulationInterface;
     */
    public boolean createGraph()
    {
        try
        {
            System.out.println("\nTrying to load serialized graph file ");
            loadGraph();
            System.out.println("Files loaded correctly!");
            System.out.println("Loaded " + vertexesMap.size() + " vertexes!");
            return true;

        } catch (IOException e)
        { // File does not exist - populate graph
            System.out.println("\nFile " + DEFAULT_GRAPH + " does not exist. Creating it...");
            String[][] tables = DatabaseConnection.getNumCols(con, "postgres");

            // Get DB structure and datas
            System.out.println("\n> [" + tables.length + "] tables found");

            System.out.println("Tables =" + tables.length + "x" + tables[0].length);
            for (int i = 0; i < tables.length; i++)
            {
                System.out.println("----");
                for (int j = 0; j < tables[i].length; j++)
                {
                    System.out.println(tables[i][j]);
                }//for
            }//for

            // Cycle to get datas from relations without foreign key
            addNoForeignKey(tables, false);
            // Cycle to get datas from relations with foreign key
            addForeignKey(tables, false);
            System.out.println("\n> Population done correctly");

            // Summary
            System.out.println("\nGraph info:");
            System.out.println(" - Number of vertexes:\t" + vertexesMap.size());
            System.out.println(" - Number of edges:\t\t" + edges);

            System.out.print("\nComputing weight... ");
            setEdgesWeight();
            System.out.println("Done");

            try
            {
                saveGraph();
                saveKeyword();
            } catch (IOException exc)
            {
            }//try-catch

            return true;
        } catch (ClassNotFoundException e)
        {
        } //try - catch - catch

        return false;
    }//createGraph

    public boolean addKeywords()
    {
        try
        {
            System.out.println("\nTrying to load serialized keyword file ");
            if(keywordMap.size()==0)
            {
                loadKeyword();
            }
            System.out.println("Files loaded correctly!");
            System.out.println("Loaded " + keywordMap.size() + " words!");
            return true;

        } catch (IOException e)
        { // File does not exist - populate graph
            System.out.println("\nFile " + DEFAULT_SERIALIZE + " does not exist. Creating it...");
            String[][] tables = DatabaseConnection.getNumCols(con, "postgres");
            // Create keyword map
            addNoForeignKey(tables, true);
            addForeignKey(tables, true);

            try
            {
                saveKeyword();
            } catch (IOException exc)
            {
            }//try-catch

            return true;

        } catch (ClassNotFoundException e)
        {
        } //try - catch - catch

        return false;
    }//createGraph

    /**
     * @see interfaces.PopulationInterface;
     */
    public ArrayList<Vertex> getKeyword(String keyword)
    {
        return keywordMap.get(keyword);
    }//getKeyword

    /**
     * @see interfaces.PopulationInterface;
     */
    public void putKeyword(String keyword, Vertex v)
    {
        ArrayList<Vertex> list = getKeyword(keyword);
        if (null == list)
        {
            list = new ArrayList<>();
            list.add(v);
            keywordMap.put(keyword, list);

        } else
        {
            list.add(v);
            keywordMap.put(keyword, list);

        }//if-else
    }//putKeyword

    public int size()
    {
        return vertexesMap.size();
    }//size

    public Vertex getVertex(String label)
    {
        return vertexesMap.get(label);
    }//getVertex

    public HashMap<String, Vertex> getVertexMap()
    {
        return vertexesMap;
    }//getVertexMap

    // --- PRIVATE METHODS --- //

    /**
     * Create graph for IMDB database. This methods analyze tables without foreign key first
     *
     * @param tables [String[][]]IMDB tables structure
     */
    private void addNoForeignKey(String[][] tables, boolean onlyKeyword)
    {
        System.out.println("\nList of vertexes without foreign key");

        String[][][] result = new String[tables.length][][];
        for (int i = 0; i < tables.length; i++)
        {
            result[i] = DatabaseConnection.getContentTable(con, tables[i][0], Integer.parseInt(tables[i][1]));
        }//for

        for (int i = 0; i < tables.length; i++)
        {
            long time = System.currentTimeMillis();
            if (!tables[i][0].equals("movie_info") && !tables[i][0].equals("cast_info"))
            {
                System.out.print(" - Table: '" + tables[i][0] + "' of size (" + tables[i][1] + ")");

                int search_id_index = result[i][0].length - 1;
                isFirst = true;
                for (int h = 0; h < result[i].length; h++)
                {

                    Vertex v;
                    if (!onlyKeyword)
                    {
                        v = new Vertex(tables[i][0] + ":" + result[i][h][0], result[i][h][search_id_index]);
                        vertexesMap.put(v.getLabel(), v);
                    } else
                    {
                        v = vertexesMap.get(tables[i][0] + ":" + result[i][h][0]);
                    }
                    computeKeywords(v, result[i][h], tables[i][0]);

                }//for_h
                time = (System.currentTimeMillis() - time) / 1000;
                System.out.println(" -- Time: " + time + " sec");
                System.out.println("\tAdded " + result[i].length + " vertexes.");

                result[i] = null;
            }//if [not foreign key]
        }//for_i


    }//addNoForeignKey

    /**
     * Create graph for IMDB database. This methods analyze tables with foreign key
     *
     * @param tables [String[][]] IMDB tables structure
     */
    private int addForeignKey(String[][] tables, boolean onlyKeyword)
    {
        //int edges = 0;
        System.out.println("\nList of vertexes with foreign key");

        try
        {
            for (int i = 0; i < tables.length; i++)
            {
                isFirst = true;
                //System.out.println("\n AddForeignKey Computing table "+i+" out of "+ tables.length);
                if (tables[i][0].equals("movie_info"))
                {
                    movie_info(tables, i, onlyKeyword);

                } else if (tables[i][0].equals("cast_info"))
                {
                    cast_info(tables, i, onlyKeyword);
                }// if - else if
            }//for_i

        } catch (UnsupportedEncodingException e)
        {
            System.err.println("[addNoForeignKey] Encoding error rises: " + e);

        } finally
        {
            return edges;
        }// try - catch - finally
    }//addForeignKey

    /**
     * Method to custom vertex constructions for movie_info relation
     *
     * @param tables [String[][]] query result with table data
     * @param i      [Integer]row $tables index
     * @return number of added edges
     */
    private void movie_info(String[][] tables, int i, boolean onlyKeyword) throws UnsupportedEncodingException
    {
        String[][] result = DatabaseConnection.getContentTable(con, tables[i][0], Integer.parseInt(tables[i][1]));
        System.out.print(" * Table: '" + tables[i][0] + "' of size (" + tables[i][1] + ") - " + result.length);

        long time = System.currentTimeMillis();
        for (int h = 0; h < result.length; h++)
        {
            if (h % 1000 == 0)
            {
                System.out.println("Movie_info Computing row " + h + " out of " + result.length);
            }
            Vertex v, u;
            int search_id_index = result[0].length - 1;
            if (!onlyKeyword)
            {
                v = new Vertex(tables[i][0] + ":" + result[h][0], result[h][search_id_index]);        // Vertex with foreign key
                u = vertexesMap.get(tables[0][0] + ":" + result[h][1]);   // Vertex with primary key

                Edge e = new Edge(v, u);  // movie_id => title.id
                v.addNeighbor(e);
                u.addNeighbor(e);

                vertexesMap.put(v.getLabel(), v);
                vertexesMap.replace(u.getLabel(), u); // Rewrite the old node
            } else
            {
                v = vertexesMap.get(tables[i][0] + ":" + result[h][0]);
            }

            computeKeywords(v, result[h], "movie_info");

            edges++;
        }//for_h
        time = (System.currentTimeMillis() - time) / 1000;
        System.out.println(" -- Time: " + time + " sec");
    }//movie_info

    /**
     * Method to custom vertex constructions for cast_info relation
     *
     * @param tables [String[][]] query result with table data
     * @param i      [Integer] row $tables index
     * @return number of added edges
     */
    private synchronized void cast_info(String[][] tables, int i, boolean onlyKeyword) throws UnsupportedEncodingException
    {
        String[][] result = DatabaseConnection.getContentTable(con, tables[i][0], Integer.parseInt(tables[i][1]));
        System.out.println(" * Table: '" + tables[i][0] + "' of size (" + tables[i][1] + ")" + result.length);
        castInfoResult = result;
        table_name = tables[i][0];
        s_index = 0;
        s_numThread = 0;
        onlyKeywordThread = onlyKeyword;
        for (int j = 0; j < nThread; j++)
        {
            (new PopulationThread()).start();
        }//for

        try
        {
            long time = System.currentTimeMillis();
            wait();
            time = (System.currentTimeMillis() - time) / 1000;
            System.out.println(" -- Time: " + time + " sec");
        } catch (InterruptedException e)
        {
        }
    }//cast_info


    private synchronized void monitor()
    {
        while (s_index < castInfoResult.length)
        {
            //while (s_index < 1000) {
            if (s_index % 1000 == 0)
                System.out.println("\n [cast_info] Computing row " + s_index + " out of " + castInfoResult.length);
            int i = s_index++;
            Vertex v;
            int search_id_index = castInfoResult[0].length - 1;
            if (!onlyKeywordThread)
            {
                v = new Vertex(table_name + ":" + castInfoResult[i][0], castInfoResult[i][search_id_index]);

                edges += addEdge(v, castInfoResult[i][1], "name"); // cast_info.person_id => name.id
                edges += addEdge(v, castInfoResult[i][2], "title"); // cast_info.movie_id => title.id
                edges += addEdge(v, castInfoResult[i][3], "char_name"); // cast_info.person_role_id => char_name.id
                edges += addEdge(v, castInfoResult[i][6], "role_type"); // cast_info.role_id => role_type.id

                vertexesMap.put(v.getLabel(), v); // Add new Vertex in the hashMap
            } else
            {
                v = vertexesMap.get(table_name + ":" + castInfoResult[i][0]);
            }
            computeKeywords(v, castInfoResult[i], "cast_info");
        }//while

        s_numThread++;

        if (s_numThread == nThread)
        {
            notifyAll();
        }//if
    }//cast_info

    private synchronized int addEdge(Vertex newVert, String key, String table)
    {
        if (key != null)
        {
            Vertex u = vertexesMap.get(table + ":" + key);
            Edge e = new Edge(newVert, u);

            newVert.addNeighbor(e);
            u.addNeighbor(e);
            vertexesMap.replace(table + ":" + key, u);
            return 1;

        } else
        {
            return 0;
        }//if-else
    }//addEdge

    private void setEdgesWeight()
    {
        int z = 0;
        for (String key : vertexesMap.keySet())
        {
            Vertex v = vertexesMap.get(key);
            ArrayList<Edge> neighbor = v.getNeighbors();
            System.out.println("SetEdgeWeight " + (z++) + " out of " + vertexesMap.keySet().size());
            for (int i = 0; i < neighbor.size(); i++)
            {
                if (vertexesMap.get(neighbor.get(i).getTo()) == v)
                { // Consider only inner edges
                    Vertex from = vertexesMap.get(neighbor.get(i).getFrom());
                    double w = Math.log(1 + Math.max(from.getNeighborCount(), v.getNeighborCount()));
                    neighbor.get(i).setWeight(w);
                }//if
            }//for
        }//for
    }//edgeWeight


    private void saveGraph() throws IOException
    {
        System.out.println("Saving graph, number of vertex = " + vertexesMap.size());

        // Save graph
        System.out.print("\n> Graph serialization... ");
        File file = new File(DEFAULT_GRAPH);
        FileOutputStream f = new FileOutputStream(file);
        ObjectOutputStream s = new ObjectOutputStream(f);
        s.writeObject(vertexesMap);
        s.close();
        System.out.println("Done");
    }//saveGraph


    private void saveKeyword() throws IOException
    {
        System.out.println("Saving keyword, number of keyword = " + keywordMap.size());

        // Save keywords
        System.out.print("> HashMap serialization... ");
        File file = new File(DEFAULT_SERIALIZE);
        FileOutputStream f = new FileOutputStream(file);
        ObjectOutputStream s = new ObjectOutputStream(f);
        s.writeObject(keywordMap);
        s.close();
        System.out.println("Done");
    }

    private void loadGraph() throws IOException, ClassNotFoundException
    {
        // Graph load
        System.out.print("> Graph loading... ");
        File file = new File(DEFAULT_GRAPH);
        FileInputStream f = new FileInputStream(file);
        ObjectInputStream s = new ObjectInputStream(f);
        vertexesMap = (HashMap<String, Vertex>) s.readObject();
        s.close();
    }//loadGraph

    private void loadKeyword() throws IOException, ClassNotFoundException
    {
        // Keyword load
        System.out.print("> Keyword loading... ");
        File file = new File(DEFAULT_SERIALIZE);
        FileInputStream f = new FileInputStream(file);
        ObjectInputStream s = new ObjectInputStream(f);
        keywordMap = (HashMap<String, ArrayList<Vertex>>) s.readObject();
        s.close();
    }


    private void computeKeywords(Vertex v, String[] result, String tableName)
    {
        String text = "";
        for (int i = 0; i < result.length; i++)
        {
            if (result[i] != null && !result[i].equals(null))
            {
                text += result[i] + " ";
            }//if
        }//for_i

        if (isFirst)
        {
            isFirst = false;

            colsName = DatabaseConnection.getColumnsNames(con, tableName);
        }//if
        text += tableName;
        for (int i = 0; i < colsName.length; i++)
        {
            text += " " + colsName[i];
        }//for_i
        String kw = remover.remove(text.toLowerCase());
        String[] kws = kw.split(" ");

        for (int i = 0; i < kws.length; i++)
        {
            ArrayList<Vertex> alv = keywordMap.get(kws[i]);
            if (alv == null)
            {
                alv = new ArrayList<>();
                alv.add(v);
                keywordMap.put(kws[i], alv);
            }//if
            else
            {
                alv.add(v);
            }

        }//for_z
    }//computeKeywords


    private class PopulationThread extends Thread
    {

        @Override
        public void run()
        {
            monitor();
        }//run
    }//class::PopulationIMDB::runningThread

}//class::PopulationIMDB

