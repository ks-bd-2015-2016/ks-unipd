package jdbc;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.sql.*;
import java.util.*;
import java.io.*;

public class DatabaseConnection
{
	private static final String SQL_FILE_NAME = "jdbc/query.properties";

	private static final String TABLES_NAME = "tables_name";
	private static final String TABLES_PARAM = "column_count";
	private static final String TABLES_COUNT = "table_size";
	private static final String COLUMNS_COUNT = "column_size";
	private static final String COLUMNS_NAME = "column_name";


	/**
	 * Start a connection to the database
	 * @return the connection to the database
     */
	public static Connection getConnection(String db, String user, String pwd)
	{
		Class cl = null;
		Connection con = null;
		try
		{
			//Start connection
			cl = Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://localhost/"+db, user, pwd);
			System.out.println("successfully established connection!");
		}

		catch (SQLException e)
		{
			System.err.println("Connection failed: " + e);
		}

		catch (ClassNotFoundException f)
		{
			System.err.println("Class not found: " + f);
		}
		finally
		{
			return con;
		}
	}

	/**
	 * Close a connection in a safe way (there is a controll that uses exceptions)
	 * @param con Is the connection to close
     */
	public static void closeConnection(Connection con)
	{
		try
		{
			con.close();
		}

		catch (SQLException e)
		{
			System.err.println("Error closing connection: " + e);
		}

		finally
		{
			con = null;
		}
	}

	/**
	 * It returns the string that represent the query in sql
	 * @param queryName Name of the query
	 * @return String that represent the query in sql
	 * @throws IOException
     */
	public static String getQuery(String queryName) throws IOException
	{
		Properties prop = new Properties();
		InputStream inputStream = DatabaseConnection.class.getClassLoader().getResourceAsStream(SQL_FILE_NAME);
		if(inputStream	!= null)
		{
			prop.load(inputStream);
		}
		else
		{
			throw new FileNotFoundException("Query file '"+SQL_FILE_NAME+"' not found!");
		}

		return prop.getProperty(queryName);
	}

	/**
	 * It extracts from the database the data that are requested
	 * @param set the resultSet that is returned from the execution of a query
	 * @param numCols number of columns of the resultSet
     * @return the table extracted in a two dimensioned array of string
     */
	public static String[][] getData(ResultSet set, int numRows, int numCols)
	{
		String[][] result = null;
		try
		{
			result = new String[numRows][numCols];
			int rowIndex = 0;
			while (set.next())
			{
				for (int i = 0; i < numCols; i++)
				{
					result[rowIndex][i] = set.getString(i+1);
				}
				rowIndex++;
			}
		}
		catch (SQLException d)
		{
			System.err.println(d);
		}
		finally
		{
			return result;
		}
	}

	/**
	 * It executes the query to find the names of all the tables in the database
	 * @param con Connection to the database
	 * @return the table extracted in a two dimensioned array of string
     */
	public static String[][] getTablesName(Connection con, String db)
	{
		String[][] result = null;
		Statement st = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			st = con.createStatement();
			rs = st.executeQuery(getQuery(TABLES_COUNT));
			rs.next();
			int rows = rs.getInt(1);

			ps = con.prepareStatement(getQuery(TABLES_NAME));
			ps.setString(1, db);
			rs = ps.executeQuery();
			result = getData(rs,rows,1);

			rs.close();
			st.close();
		}
		catch (SQLException e)
		{
			System.err.println(e);
		}
		catch (IOException e)
		{
			System.err.println(e);
		}
		finally
		{
			rs = null;
			ps = null;
			st = null;
			return result;
		}
	}

	/**
	 * It returns the number of columns of each table in the database
	 * @param con Connection to the database
	 * @return Table with in the first column the name of the table and in the second one the number of attributes. The format that is used is a two
	 * dimensioned array of string.
     */
	public static String[][] getNumCols(Connection con, String db)
	{
		String[][] result = null;
		Statement st = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = con.prepareStatement(getQuery(TABLES_COUNT));
			ps.setString(1, db);
			rs = ps.executeQuery();
			rs.next();
			int rows = rs.getInt(1);

			st = con.createStatement();
			rs = st.executeQuery(getQuery(TABLES_PARAM));
			result = getData(rs,rows,2);

			rs.close();
			st.close();
		}
		catch (SQLException e)
		{
			System.err.println(e);
		}
		catch (IOException e)
		{
			System.err.println(e);
		}
		finally
		{
			rs = null;
			st = null;
			return result;
		}
	}


	/**
	 * It returns the content of a table
	 * @param con Connection to the database
	 * @param table Name of the table
	 * @param numCols Number of columns of the table
     * @return Table with the content of the table specified. The format that is used is a two dimensioned array of string.
     */
	public static String[][] getContentTable(Connection con, String table, int numCols)
	{
		String[] colsName = null;
		String[][] result = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			// The query to extract the content of a specific table is write directly in the source code.
			// The from clause can't be passed as a parameter in a preparedStatement
			st = con.prepareStatement("SELECT COUNT(*) FROM "+ table + ";");
			rs = st.executeQuery();
			rs.next();
			int rows = rs.getInt(1);

            st = con.prepareStatement(getQuery(COLUMNS_NAME));
			st.setString(1, table);
			rs = st.executeQuery();

            colsName = getColumnsNames(con, table);

			st = con.prepareStatement("SELECT * FROM "+ table + ";");
			rs = st.executeQuery();
			result = getData(rs, rows, colsName.length);

			rs.close();
			st.close();
		}
		catch (SQLException e)
		{
			System.err.println(e);
		}
		
		finally
		{
			rs = null;
			st = null;
			return result;
		}
	}
    
    public static String[] getColumnsNames(Connection con, String table){
        ArrayList<String> colsName = new ArrayList<>();
        //String[][] result = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try{
            // The query to extract the content of a specific table is write directly in the source code.
            // The from clause can't be passed as a parameter in a preparedStatement
            st = con.prepareStatement(getQuery(COLUMNS_NAME));
            st.setString(1, table);
            rs = st.executeQuery();

            while( rs.next() ){
                colsName.add( rs.getString(1) );
            }//while
            
            rs.close();
            st.close();

        }catch (SQLException e){
            System.err.println(e);

        }finally{
            rs = null;
            st = null;
            return colsName.toArray(new String[colsName.size()]);

        }//try-catch-finally
    }//getColumnsNames


    /**
     * Methot to create table
     * @param con Connection to database
     * @param tabName Table name
     * @param params List of params
     * @param PK List of primary key
     */
    public static void createTable(Connection con, String tabName, String[] params, String[] PK){
        Statement st = null;
        try{
            st = con.createStatement();

            st.executeUpdate("DROP TABLE IF EXISTS " + tabName);

            String query = "CREATE TABLE " + tabName + " (";
            for (int i=0; i < params.length-1; i++){
                query += params[i] + " TEXT, ";
            }//for

            if(PK.length != 0) {
                query += params[params.length - 1] + " TEXT, PRIMARY KEY (";
                for (int i=0; i < PK.length-1; i++){
                    query += PK[i] + ", ";
                }//for
                query += PK[PK.length - 1] + ") );";

            }else{
                query += params[params.length - 1] + "TEXT);";
            }
            st.executeUpdate(query);


        }catch(SQLException e){
            System.err.println("Error creating " + tabName + " table:\n" + e);
        }finally {
            st = null;
        }//try - catch(SQLExc) - finally
    }//createTable

    /**
     * Method to execute COPY function from file, instead using INSERT
     * @param con Connection to DB
     * @param tabName Name of table to populate
     * @param filePath Path and name of file
     * @return CopyManager.copyIn result
     */
	public static long runDump(Connection con, String tabName, String filePath){
        long result = -1;
        try {
            CopyManager manager = new CopyManager( (BaseConnection)con );
            FileReader reader = new FileReader(filePath);

            result = manager.copyIn("COPY " + tabName + " FROM STDIN", reader);

        }catch (SQLException e){
            e.printStackTrace();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            return result;
        }//try - catch - finally
	}//runDump
}
