package jdbc;

import dbpf.Performer;
import dbpf.StandardPerformer;
import graph.PopulationIMDB;
import graph.PopulationMondial;
import graph.TestSolution;
import interfaces.PopulationInterface;
import interfaces.TreeInterface;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class ExampleExecution
{
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {

        boolean finish = false;
        Scanner in = new Scanner(System.in);

        Connection con;
        PopulationInterface pop = null;
        System.out.println("What database you want to load? If you want to close the program press Q.");
        System.out.println("1) Mondial");
        System.out.println("2) IMDB");
        System.out.println("3) Mondial solution graph weight computation");
        System.out.println("4) IMDB solution graph weight computation");
        String line;

        while (!finish)
        {
            System.out.print("Command: ");
            line = in.nextLine();
            while (line.equals(""))
            {
                line = in.nextLine();
            }
            switch (line.toLowerCase())
            {
                case "1":
                {
                    System.out.println("Loading Mondial database... It can take up to 2-3 minutes");
                    con = DatabaseConnection.getConnection("mondial", "postgres", "postgres");
                    pop = new PopulationMondial(con);
                    finish = true;
                    break;
                }
                case "2":
                {
                    System.out.println("Loading IMDB database... It can take up to 5-7 minutes");
                    con = DatabaseConnection.getConnection("postgres", "postgres", "postgres");
                    pop = new PopulationIMDB(con);
                    finish = true;
                    break;
                }
                case "3":
                {
                    System.out.println("Loading Mondial database... It can take up to 2-3 minutes");
                    con = DatabaseConnection.getConnection("mondial", "postgres", "postgres");
                    pop = new PopulationMondial(con);
                    pop.createGraph();
                    pop.addKeywords();

                    System.out.println("Now you can compute your graphs' weights");
                    TestSolution ts = new TestSolution( pop );

                    double res = 1;
                    while(res >= 0){
                        res = ts.compute();
                        System.out.println("Final weight ==> " + res );
                    }//while
                    System.exit(0);
                }
                case "4":
                {
                    System.out.println("Loading IMDB database... It can take up to 5-7 minutes");
                    con = DatabaseConnection.getConnection("postgres", "postgres", "postgres");
                    pop = new PopulationIMDB(con);
                    pop.createGraph();
                    pop.addKeywords();

                    System.out.println("Now you can compute your graphs' weights");
                    TestSolution ts = new TestSolution( pop );

                    double res = 1;
                    while(res >= 0){
                        res = ts.compute();
                        System.out.println("Final weight ==> " + res );
                    }//while
                    System.exit(0);
                }
                case "q":
                {
                    System.out.println("Closing program...");
                    in.close();
                    System.exit(0);
                    break;
                }
                default:
                    System.out.println("Invalid input, try again...");
                    break;
            }
        }

        if( pop == null){
            System.exit(-1);
        }//if
        pop.createGraph();
        pop.addKeywords();


        String[] query;
        ArrayList<String> rawQuery;
        System.out.println("Database loaded.");

        while (true)
        {
            System.out.println("Insert a query or press q to exit:");
            line = in.nextLine();
            while (line.equals(""))
            {
                line = in.nextLine();
            }
            if (line.toLowerCase().equals("q"))
            {
                System.out.println("Closing program...");
                in.close();
                System.exit(0);
            }
            StandardPerformer dbpf = new StandardPerformer(pop);
            rawQuery = new ArrayList<>();
            Scanner token = new Scanner(line);
            int index = 0;
            while (token.hasNext())
            {
                rawQuery.add(token.next());
                index++;
            }
            if (index > 0)
            {
                System.out.println("With how many result?");
                int count = in.nextInt();
                query = new String[index];
                index = 0;
                for (String word : rawQuery) {
                    query[index++] = word;
                }
                LinkedList<TreeInterface> ris = dbpf.dpbfk(query, count);
                if(ris != null)
                {
                    long time = dbpf.getLastQuerySec();

                   System.out.println("The results are: \n" + ris);
                    System.out.println("And they were calculated in: " + time + "ms");

                    System.out.println("Write the name of the file where you want to save the results:");
                    String n = in.nextLine();
                    while (n.equals(""))
                    {
                        n = in.nextLine();
                    }
                     PrintWriter writer = new PrintWriter("Results/Articolo/" + n +".txt", "UTF-8");
                    writer.println("The results are: \n" + ris);
                    writer.println("And they were calculated in: " + time + "ms");
                    writer.close();


                    System.out.println("Do you want also to use the alternative algorithm? Y/N");
                    line = in.nextLine();
                    while (line.equals(""))
                    {
                        line = in.nextLine();
                    }
                    if (line.toLowerCase().equals("y"))
                    {
                        Performer per = new Performer(pop);
                        ris = per.dpbfk(query, count);
                        time = per.getLastQuerySec();

                        System.out.println("The results are: \n" + ris);
                        System.out.println("And they were calculated in: " + time + "ms");

                        PrintWriter writer1 = new PrintWriter("Results/Matteo/" + n +".txt");
                        writer1.println("The results are: \n" + ris);
                        writer1.println("And they were calculated in: " + time + "ms");
                        writer1.close();
                    }
                }
                token.close();
            } else
            {
                System.out.println("You need to write at least one word for the query to be valid...");
            }
        }

    }


}//class::ExampleExecution
