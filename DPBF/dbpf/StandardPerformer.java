package dbpf;

import graph.*;
import graph.words.CodaPrioritaria;
import interfaces.PopulationInterface;
import interfaces.TreeInterface;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


public class StandardPerformer
{
    private PopulationInterface graph;

    //private PriorityQueue<TreeInterface> subGraphQueue;
    private CodaPrioritaria subGraphQueue;
    private HashMap<String, ArrayList<TreeInterface>> subGraphHash; // root.getLabel(), List of Tree

    private HashMap<String, ArrayList<String>> foundKws; // v.getLabel(), List of keywords
    private int numFoundKws; // Count how many keyword from query was found
    private LinkedList<TreeInterface> result;

    private long lastQuerySec;


    /**
     * Constructor
     */
    public StandardPerformer(PopulationInterface graph)
    {
        this.graph = graph;
        subGraphQueue = new CodaPrioritaria();
        subGraphHash = new HashMap<>();

        result = new LinkedList<>();
        foundKws = new HashMap<>();
        numFoundKws = 0;
    }//Constructor

    /**
     * Compute dpbf-k algorithm
     *
     * @param iKeywordQuery [String[]] query of keywords
     * @return a list of the best DPBF-k trees
     */
    public LinkedList<TreeInterface> dpbfk(String[] iKeywordQuery, int k)
    {
        subGraphQueue = new CodaPrioritaria();
        subGraphHash = new HashMap<>();

        result = new LinkedList<>();
        foundKws = new HashMap<>();
        numFoundKws = 0;
        System.out.println("\nStarting algorithm... ");
        lastQuerySec = System.currentTimeMillis();

        // Get starting vertex from graph
        getStartingVertexes(iKeywordQuery);
        if (0 == subGraphQueue.size())
        {
            System.out.println("> There aren't any keywords into IMDB database!");
            return null;
        }//if

        while (!subGraphQueue.isEmpty())
        {
            if(System.currentTimeMillis() - lastQuerySec >= 1800000)
            {
                lastQuerySec = System.currentTimeMillis() - lastQuerySec;
                return result;
            }
            System.out.println("> Execution time : " + (System.currentTimeMillis() - lastQuerySec)/1000 );

            TreeInterface tree = subGraphQueue.poll();
            Node root = tree.getRoot();
            ArrayList<TreeInterface> trees = subGraphHash.get(root.getLabel());
            if (trees==null || (trees!=null && !trees.contains(tree)))
            {
                // This subTree has been removed from previous operations
                continue;
            }//if

            // I've to remove the tree also from the hashMap
            ArrayList treeList = subGraphHash.get(root.getLabel());
            treeList.remove(tree);
            if(treeList.size() == 0)
            {
                subGraphHash.remove(root.getLabel());
            }

            // Compute a DPBF-k instead of a DPBF-1
            if (tree.getKeywords().size() == numFoundKws)
            {
                result.add(tree);

                System.out.println("> Found iKeword-query tree rooted in " + root.getLabel());
                System.out.println("> Partial result : " + tree);
                if (result.size() >= k) {
                    lastQuerySec = System.currentTimeMillis() - lastQuerySec;
                    return result;
                }//if
                continue;
            }//if


            ArrayList<Edge> neighbors = graph.getVertex(root.getLabel()).getNeighbors();
            System.out.println("> Number of tree in the queue= " + subGraphQueue.size() + " number of found results: " + result.size());

            // For each u neighbour of v:
            //   if T(v,p)+(v,u) has weight less than T(u,p) then
            //     T(u) = T(v)+(v,u);
            //     subGraphQueue <- T(u);

            Node rootCopy = root.copy();
            upToLevel(tree, rootCopy, neighbors);

            // Per ogni p1 tc (p intersecato p1)=0
            //   se T(v, p)+T(v, p1) < T(v, (p unito p1) ) allora
            //     T(v, (p unito p1) ) <- T(v, p)+T(v, p1)
            //     aggiorna subGraphQueue con T(v, (p unito p1) )
            merge(tree, rootCopy);

        }//while

        lastQuerySec = System.currentTimeMillis() - lastQuerySec;
        return result;
    }//dpbfk

    /**
     * Compute dpbf-1 algorithm
     *
     * @param iKeywordQuery [String[]] query of keywords
     * @return the best DPBF tree
     */
    public LinkedList<TreeInterface> dpbf1(String[] iKeywordQuery)
    {
        return dpbfk(iKeywordQuery,1);
    }//dpbf1

    /**
     * Extract the first subTrees from the graph
     *
     * @param iKeywordQuery [String[]] query of keywords
     */
    private void getStartingVertexes(String[] iKeywordQuery)
    {
        numFoundKws = 0;
        int i = 0, j = 0;

        for (String keyword : iKeywordQuery)
        {
            HashSet<Vertex> vertexList = new HashSet<>();
            ArrayList<Vertex> keywordVertexes = graph.getKeyword(keyword);


            if (keywordVertexes == null)
            {
                System.out.println("> Keyword '" + keyword + "' not found! Searching without it.");

            } else
            {
                vertexList.addAll(keywordVertexes);
                numFoundKws++;
                i = 0;
                j++;

                for (Vertex v : vertexList)
                {
                    System.out.println("-- Vertex " + (i++) + " out of " + vertexList.size() + " of the "
                            + j + " word out of " + iKeywordQuery.length);
                    HashSet<String> keywords = new HashSet<>();
                    keywords.add(keyword);

                    Tree t = new Tree(new Node(v.getLabel(), v.getSearchId()), keywords);
                    String label = v.getLabel();

                    // Update set of subTrees
                    ArrayList<TreeInterface> treeList;
                    if ((1 == j) || !subGraphHash.containsKey(label))
                    {
                        treeList = new ArrayList<>();
                        treeList.add(t);
                        subGraphHash.put(label, treeList);
                    } else
                    {
                        treeList = subGraphHash.get(label);
                        t.addKeyword(treeList.get(0).getKeywords());
                        treeList.remove(0);
                        treeList.add(t);
                        subGraphHash.replace(label, treeList);
                    }//if - else

                    // Update set of keywords
                    ArrayList<String> kws;
                    if (foundKws.containsKey(label))
                    {
                        kws = foundKws.get(label);
                    } else
                    {
                        kws = new ArrayList<>();
                    }//else
                    kws.add(keyword);
                    foundKws.put(v.getLabel(), kws);

                }//for


            }// if - else
        }//for
        System.out.println("-- Adding vertexes to the queue...");

        subGraphQueue.setNumberOfKeyword(j);
        //use this for a efficient shallow copy between the hash and the queue
        Collection<ArrayList<TreeInterface>> collection = subGraphHash.values();
        for (ArrayList<TreeInterface> al : collection)
        {
            subGraphQueue.addFirstTime(al);
        }//for*/
        subGraphQueue.finalizeAddFirstTime();

        System.out.println("> Found vertexes: " + subGraphQueue.size());
    }// getStartingVertexes

    /**
     * This method goes up one level on the tree
     *
     * @param tree      [Tree] tree to be analyzed
     * @param root      [Vertex] Tree root vertex
     * @param neighbors [ArrayList<Edge>] List of root's neighbors vertex
     */
    private void upToLevel(TreeInterface tree, Node root, ArrayList<Edge> neighbors)
    {
        for (Edge edge : neighbors)
        {
            //We must not change the original tree (we need it for the merge), so we use a deep copy of it
            TreeInterface newTree = tree.copy();

            // Consider bidirectional arcs
            Vertex v = graph.getVertex(edge.getTo());

            if (v.getLabel().equals(root.getLabel()))
            {
                v = graph.getVertex(edge.getFrom());
            }//if

            Node u = new Node(v.getLabel(), v.getSearchId());

            //if the new node is already in the tree we can't use it anymore or the tree will become a graph
            // and it will cicle on itself forever so we just pass to the next neighbor
            if (newTree.containsNode(u))
            {
                continue;
            }
            //if the new node is already present in the hash we need to check all the tree with this node
            // as the root, and keep only the one with less weight
            if (subGraphHash.containsKey(u.getLabel()))
            {
                neighborsExploit(edge, u, newTree);

            } else
            {
                //there are no tree with u as root, so we create the new tree and we put it in both the queue and the hash
                if (foundKws.containsKey(u.getLabel()))
                {
                    newTree.addRootNode(u, edge, foundKws.get(u.getLabel())); // Up to one level with the keywords update

                } else
                {
                    newTree.addRootNode(u, edge); // Up to one level without the keywords update
                }//if - else


                subGraphQueue.add(newTree);
                ArrayList<TreeInterface> alt = new ArrayList<>();
                alt.add(newTree);
                subGraphHash.put(u.getLabel(), alt);

            }//if-else
        }//for
    }// upToLevel


    private void neighborsExploit(Edge edge, Node u, TreeInterface tree)
    {
        ArrayList<TreeInterface> subGraphs = subGraphHash.get(u.getLabel());
        //We search in all the trees with the node u as root
        for (TreeInterface to : subGraphs)
        {
            if (to.hasTheSameKeyword(tree))
            {
                if (tree.getWeight() + edge.getWeight() < to.getWeight())
                {
                    //We have found a tree with the same keyword but greater weight than the one we will create

                    //we create the new tree and insert it in both the queue and the hash
                    if (foundKws.containsKey(u.getLabel()))
                    {
                        tree.addRootNode(u, edge, foundKws.get(u.getLabel())); // Up to one level with the keywords update
                    } else
                    {
                        tree.addRootNode(u, edge); // Up to one level without the keywords update
                    }//if - else

                    //we remove it from the queue and the hash and we put the new one in
                    subGraphQueue.remove(to);
                    subGraphQueue.add(tree);

                    ArrayList<TreeInterface> alt = subGraphHash.get(u.getLabel());
                    alt.remove(to);
                    alt.add(tree);
                }
                //There can be only 1 tree with the same keywords, so if we have found it, it was already swapped
                //with the new tree or the new tree doesn't need to be created
                return;
            }//if
        }//for

        //if there is no tree with the same keywords we need to create the new one and put it in both the queue and the hash
        if (foundKws.containsKey(u.getLabel()))
        {
            tree.addRootNode(u, edge, foundKws.get(u.getLabel())); // Up to one level with the keywords update

        } else
        {
            tree.addRootNode(u, edge); // Up to one level without the keywords update
        }//if - else


        subGraphQueue.add(tree);

        ArrayList<TreeInterface> alt = subGraphHash.get(u.getLabel());
        alt.add(tree);
    }//neighborsExploit()

    private void merge(TreeInterface tree, Node root)
    {
        CopyOnWriteArrayList<TreeInterface> subTrees = new CopyOnWriteArrayList<>();
        ArrayList<TreeInterface> trees = subGraphHash.get(root.getLabel());

        if (trees == null)
        {
            return;
        }//if
        subTrees.addAll(trees);
        //We search in all the trees with the node root as root
        for (TreeInterface to : subTrees)
        {
            if (tree.hasEmptyKeywordInterception(to))
            {
                //We have found a tree with all the keywords different so we proceed to merge it with our tree,
                //we need to remove this tree from the queue (and also from subTrees so we have less element
                //to cicle in the internal loop)
                subTrees.remove(to);
                subGraphQueue.remove(to);

                TreeInterface newTree = to.copy();
                newTree.concatenateTree(tree);

                boolean allDifferent = true;
                //We search in all the trees with the node root as root
                for (TreeInterface same : subTrees)
                {
                    if (newTree.hasTheSameKeyword(same))
                    {
                        allDifferent = false;
                        if (newTree.getWeight() < same.getWeight())
                        {
                            //We have found a tree with the same keyword but greater weight than the one we have created
                            //with the merge, so we remove it from the queue and the subTrees and we add the new tree instead
                            subTrees.remove(same);
                            subTrees.add(newTree);
                            subGraphQueue.remove(same);
                            subGraphQueue.add(newTree);
                        }
                        break;
                    }
                }
                //only if there are no other tree with the same keyword
                if(allDifferent)
                {
                    subTrees.add(newTree);
                    subGraphQueue.add(newTree);
                }
            }//if
        }//for


        ArrayList<TreeInterface> temp = new ArrayList<>();
        temp.addAll(subTrees);
        subGraphHash.put(root.getLabel(), temp);
    }//merge

    public long getLastQuerySec(){
        return lastQuerySec;
    }//getLastQuerySec

}//class::Performer
